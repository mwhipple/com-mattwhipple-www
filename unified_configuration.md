---
title: Unified Configuration
---

- [Home] (./index.html)

A project I've kicked around a couple times over the years and am
doing so again is to use a system where I can define my preferred
configuration (most notably for text editing) in some canonical source
and have files generated/updated for assorted tools.

For my personal use I'll likely be sticking with fairly low level,
well-understood tools. However for work I'm likely to use higher level
tools which can facilitate online collaboration/mentoring. I also have
many thoughts which will be covered on this site regarding the evolution
of building software as the line between art and science shifts and
much of what is currently within the purview of programmers can itself
become automated. From a business perspective I am therefore highly
interested in no-code/low-code solutions and making use of tools
such as IntelliCode to provide me with wheels[@10.1145/3398392].

I'll be starting that here with some target outputs to come. I'll also
be tying this in with my technology reboot so the source format and
transformation engine used will evolve accordingly.

To give myself a bit of a head start I'll begin with basic key events
that are unlikely to need specific binding. The initial format will be
a space separated list of theoretical commands followed by their arguments.
Ultimately this will likely be in some language or another.

~~~
bind a self_insert
bind b self_insert
bind c self_insert
bind d self_insert
~~~
