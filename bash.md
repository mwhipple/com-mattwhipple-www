---
title: Bash
---

- [Software](./software.html)

---

I tend to make heavy use of bash for scripting. It is fairly
ubiquitous, fits very neatly with a software tools methodology and
(unsurprisingly) POSIX in particular, and provides what amounts to a
standard and readily usable REPL which lends itself to rapid
composition and experimentation of functionality (there's a term for
this in the GNU world that I should track down).

Bash certainly has its quirks - many are due to baggage (which some
newer shells don't have) but many others are arguably side effects of
its primary use as a shell. In addition to behaviors that may make
more precise control awkward for the sake of more flexible command
invocation it is also oriented towards fitting more naturally with
other constructs provided by the environment rather than affording the
developer freer rein. It could certainly be considered to be more
specific to the domain of command line automation than it is general
purpose and its suitability is accordingly bounded by that
domain. As with other technologies that are biased or specific towards
domains bash can be leveraged very effectively when the role is
appropriate, and such roles include execution and composition of
commands which is a sizeable part of how I use computers across
looking to do things, support things, and automate things.

- [Bash Style](./bash_style.html)