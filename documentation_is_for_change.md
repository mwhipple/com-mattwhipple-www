---
title: Documentation is For Change
---

- [Assorted Thoughts](./assorted_thoughts.html)

---

One area which I tend to focus on for software projects is
documentation. In terms of implementation this typically manifests as
liberally commented code on my part (in the spirit of Literate
Programming[@knuth1992literate]) and more interesting new decisions
are captured by my team in some form of architecture decision record
(ADR) with a goal of primarily discussing the perceived problem and
how the adopted solution maps on to that problem (rather than
primarily focusing on the solution which is likely to bury the
underlying intent and proposed value). For inherited decisions and
solutions such documentation may primarily serve to attempt to
reconstruct past thinking and the assorted implications introduced by
existing solutions.

Even though such documentation may seem to further codify current
state, the underlying motivation is the exact opposite. A system
cannot be safely changed if it is not able to be well understood and
without awareness of the reasoning behind past decisions there is an
increased risk of newer decisions overlooking key forces that fed into
the old. I ideally prefer to use such documentation to continuously
track how such decisions are playing out, all of which is particularly
important given that typically decisions are based on a sizeable
collection of upfront obvious information but after a decision is made
subsequent information while such a decision is active is more likely
to be a subtle trickle. If left untracked this can manifest as plan
continuation bias[@the-field-guide-to-understanding-human-error].

There is certainly a justified desire to combat stale documentation
and the suggestion of adding _additional_ documentation may invite
that problem, but such arguments seem simplistic; any system _is_ the
source of truth and so dogmatic adherence to avoiding documentation
for the sake of it being misleading seems to suggest that some
non-empircal source of information would be trusted blindly and that
actions would be made absent of critical thought - which isn't to say
that there isn't the risk of this happening but that seems to suggest
far more fundamental issues and corresponding efforts are likely in
severe jeopardy to begin with. Having spent my fair share of time on
projects for which any type of documentation is lacking, they are
typically shrouded in mystery and depending on the system and
organization are ruled by FUD (whereas even stale documentation can
typically be used to elucidate why some things have coalesced the way
they did).

Decisions may be driven by a lot of thought...or maybe not but in
either case such information is highly valuable when change comes
knocking. When that time comes the thoughts in play for those
decisions may be unavailable or may have morphed; the likelihood of
any person having retaining an accurate perspective is low and is
likely to be inversely proportional to both the age of the decision
and how consequential that decision has become (due to things like
number of integrations and iterations). While certainly not a surefire
solution, documentation provides a means to corral such concerns in a
way that can significantly ease the risks of inevitable change.

