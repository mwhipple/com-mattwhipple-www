---
title: Domain Driven Design Quickly
---

- [Books](./books.html)

---

Eric Evans's Domain Driven Design book is certainly one of the
software books I most frequently recommend and in addition to
regularly utilizing many of the ideas in that book I also regularly
proselytize many of the ideas throughout organizations as a means to
guarantee that engineering efforts remain aligned with business value
and to promote assorted requirements engineering practices. Recently
one of my teammates found and asked about
Domain Driven Design Quickly[@domain-driven-design-quickly] as a
lighter weight alternative to the much longer and less accessible Big
Blue Book and so I quickly went through it to see how much it aligns
with the messaging I tend to promote.

I was wary at the outset as many of the shorter domain driven design(DDD)
resources seem to be more of technical cookbooks: offering software
design prescriptions that feel simplistic and diametrically opposed to
the foundation that the model provides an abstract bridge between the
solution and problem spaces (or the engineers and the subject matter
experts respectively) to enable ongoing communication and evolution
(many such books seem to espouse what amounts to a lean analytical
model which is typically placed in contrast to domain driven design).
I was plesaed to find that this book did _not_ fall into that
trap (it was also likely written prior to DDD being coopted by
consultants promising canned solutions).

I'd recommend the book to get a high level overview of what DDD is
about and many of the constructs it leverages. It does a solid job of
conveying most of the core principles while eliding much of the design
advice in The Big Blue Book that, while very valuable for both
software design and modeling, is not specific or essential in terms of
DDD.

I think my one concern with Domain Driven Design Quickly compared to
the original book is that the former seems to underplay the challenge
of distilling an appropriate model. This may be a natural consequence
of the treatment being done _quickly_ which doesn't naturally lend
itself to conveying work which is likely to be ongoing and unclear.

Overall this book is a valuable resource to help foster a shared
understanding of concepts throughout a team which can enable wider
contribution and collaboration but seems to be a little too light to
inform the driving of any associated effort. If you are participating
in an effort based on DDD or are just casually interested in the topic
then this book is a great resource but if you want to lead a DDD
effort then this book by itself doesn't seem likely to cut it (though
generally any such leadership should be driven by a breadth of
knowledge and resources).