---
title: Make
---

- [Software](./software.html)

---

I tend to use Make for all of my projects and have adopted
practices which extend it well beyond common uses.

Even when there may be preferred tooling to manage the building or
software or the invoking of build specific processes, make offers a
standard and consistent interface to orchestrate tasks which cut
across ecosystems (which is particularly common in containerized
environments) or can be more simply handled as enclosing rather than
part of the project itself (such as managing git commands).

## General Usage

- [Make Style](./make_style.html)
- [Standard Targets](./make_standard_targets.html)

## Concepts

- [Generating Targets](./make_generating_targets.html)

## Recipes

- [Check Command Versions](./make_check_command_versions.html)
- [Collect Source Files](./make_collect_source_files.html)
- [Required Commands](./make_required_command.html)
