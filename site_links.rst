#####
Links
#####

******************
refs and Citations
******************

One of the goals of this site in particular is to create a highly
networked set of information (kinda what HTML is made for).
To facilitate this `sphinx` will be used to manage the links appropriately.

There are two mechanisms which are used to produce links on this site:

internal
  Internal links will be managed through the use of ``ref`` roles :cite:`SphinxHomeRef`.
  The ``default_role`` :cite:`SphinxHomeDefaultRole` will be configured to allow references
  to omit the role name.
external
  External links will be managed through the use of
  sphinxcontrib-bibtex :cite:`SphinxBibTeX`.

All external sources of information will be captured in bibliographies as described
in `bibtex`. Most content should have corresponding sources, some of which may
be added retroactively. The bibliography files themselves also act as internal content;
they provide a superset of reference entries and information to that which is used
on the site. Many of the entries are likely to be used as citations in the future if
relevant content is written, or they'll contain notes indicating why they are not
considered likely to provide such future value.

Ideally I'd like to consider leaning on something like the ``any`` role to
be able to reference all targets using the same (terse) syntax. This is
impracticable at the moment, however, as while sphinx-contrib-bibtex entries
can be resolved that way it does not properly track that such entires have
been cited and such tracking is integral to the current reference population
strategy. Thie behavior may be adjusted if the cause is stumbled upon
(it feels like a lifecycle issue), but otherwise it will just be left alone
due to low prioritization and being a bit more EIBTI.

.. todo::

   * Reference and use APA compliant citations.
   * Taming toctree
   * cite HTML design info
   * Iterative navigation
   * Reference any role
