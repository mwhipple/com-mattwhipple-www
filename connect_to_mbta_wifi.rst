################################
Recipe: Connecting to MBTA Wi-Fi
################################

.. todo:: Add iw detection

**********
Background
**********

I sometimes take the `MBTA Commuter Rail <http://www.mbta.com>`_
when I go to the office to work. The Commuter Rail offers free
Wi-Fi with the caveat that the service leaves a bit to be
desired. The connection itself can be slow and unreliable, and
they also don't offer any type of mesh network so each access
point uses a different station id. The end result is that
establishing a usable connection may require occasionally
hopping from one network to another (in cases when a more
reliable connection is really needed using a mobile phone
hotspot is likely a safer bet).

Configuring a high-level network management tool like
NetworkManager :cite:`NetworkManagerHome`
usually ends up pretty far down my TODO list, so I'm
often left handling networking concerns more directly with
low level tools.

The need here is to identify a candidate MBTA station id
and try to connect to it. Due to the reliability issues
the selected network should be a random choice so that if
the connection to one network seems to have issues the command
can be reissued until a usable connection is established.

Some Background
===============

The MBTA network is not encrypted. This means that
appropriate care should be taken when connecting
(moreso than encrypted wireless connections which are
still relatively insecure).

The name for the wireless card interface should also be
known and it should be up.
`ifconfig <http://net-tools.sourceforge.net/man/ifconfig.8.html>`_
acts as one of the more standard means to get network interface
information (`ip <https://linux.die.net/man/8/ip>`_ is a more
powerful newer alternative). ``ifconfig -a`` could be used
to list all devices and :samp:`ifconfig {ifname} up` should
bring an otherwise down interface up (which would then
be listed by ``ifconfig`` without any arguments.

Accurately automagially detecting such a device would involve
an awareness of variations which I have no desire to acquire,
but in my case the single wireless interface (wlp1s0)
can be determined using:

.. code-block:: bash

   export WLAN_IF=${WLAN_IF:-$(basename $(dirname $(find /sys -name wireless)))}

A cleaner option would likely be to configure appropriate udev rules
to assign a reliably known name to the desired device.

*******
Recipes
*******

There are two recipe variations provided here, one which uses 
`wpa_supplicant <https://w1.fi/wpa_supplicant/>`_) and one which
does not. ``wpa_supplicant`` is likely to be the better option
as it also handles connecting to WPA protected networks,
and if you're using Wi-Fi extensively then you're likely to
need this functionality and therefore the software.

The other recipe uses the simpler ``iw`` directly which may be
more appealing if ``wpa_supplicant`` is not normally used.
If ``wpa_supplicant`` is used but the ``iw`` recipe is
preferred, then ``wpa_supplicant`` should be prevented
from changing the network settings (such as stopping the process).
I started with the ``iw`` recipe as the relevant workflow
was more obvious and the relevant information more easily
discovered. Most of the primary documentation for ``wpa_supplicant``
is security oriented and connecting to a relatively unknown network
presents an inherent security risk so that may explain the
lack of forthcoming documentation (or I may have simply missed it).

Finding Networks
================

iw
--

While ``ifconfig`` is useful for standard network interface
calls, most Wi-Fi concerns fall into the realm of wireless
extensions for which a standard low level tool is
`iw <https://wireless.wiki.kernel.org/en/users/documentation/iw>`_.
Scanning available networks can be done with
:samp:`iw dev {ifname} scan`. The output of the scan
will include located station ids in the form
:samp:`SSID: {name}` (with some leading whitespace)
and the MBTA networks primarily have
names matching the patten :samp:`MBTA_WiFi_`.
To get a list of available MBTA networks we can pipe the scan
results through awk:

.. code-block:: bash

   iw dev ${WLAN_IF} scan | awk '/SSID: MBTA_WiFi/ { print $2; }'

The above works to find MBTA networks, but the awk syntax would
only output the first word if the network name contains spaces.
The actual desired behavior is to return everything after
'SSID: ' (with a trailing space). Treating the input as delimited
can cause additional wrinkles (especially given that space) and
therefore it is more easily dealt with by treating the value as
having a prefix that should be removed. A standard and available way
to process such a value is to use regular expressions for which
support is built into bash (the potential parameterization makes
``sed`` a bit more unwieldly). Some bash to return an SSID
(with possible spaces) matching a given pattern is:

.. code-block:: bash

   match_ssids() {
     while read line; do
       if [[ "$line" =~ ^[[:blank:]]*SSID:\ (.*$1.*)$ ]]; then
         echo ${BASH_REMATCH[1]}
       fi
     done < <(iw dev ${WLAN_IF} scan)
   }

wpa_supplicant
--------------

=============
Randomization
=============

If there are multiple networks we want to select a random
network out of those, this can be done with
`shuf <https://www.gnu.org/software/coreutils/manual/html_node/shuf-invocation.html>`_.
The resulting pipeline to get a random MBTA therefore resembles:

.. code-block:: bash

   match_ssids 'MBTA_WiFi' | shuf -n1

==========
Connecting
==========

Now that we have a network name we can pass that to
:samp:`iw dev {ifname} connect`. Collecting the name from a subshell
yields the command:

.. code-block:: bash

   iw dev ${WAN_IF} connect "$(match_ssids 'MBTA_WiFi' | shuf -n1)"

==========
Tidying Up
==========

Leaving the above as more free form bash is likely perfectly sufficient as it may
just be thrown into a function or script and not need much messing with,
but since I'm documenting it it would be nice
to make the invocation a little more expressive. First the repeated interface selection
is a little noisy and is unlikely to change for most computers. Wrapping this up in a
function which leans on the established variable would be neater:

.. code-block:: bash

   myiw() {
     iw dev "${WLAN_IF}" "$@"
   }

The quoting of ``"$@"`` is significant in case any arguments (such as SSIDs)
contain spaces.

Now a scan for example can be more simply entered as ``myiw scan``.

The behavior of connecting to a (random)
network based on a pattern seems worth wrapping up also. It also seems
worth just plugging right into the ``myiw`` function as an extension
subcommand, so that can be done with something like:

.. code-block:: bash

   myiw::iw() {
     iw dev "${WLAN_IF}" "$@"
   }

   myiw::match_ssids() {
     while read line; do
       if [[ "$line" =~ ^[[:blank:]]*SSID:\ (.*$1.*)$ ]]; then
         echo ${BASH_REMATCH[1]}
       fi
     done < <(myiw::iw scan)
   }

   myiw() {
     case "$1" in
       scanconn)
         myiw::iw connect "$(myiw::match_ssids "$2" | shuf -n1)"
         ;;
       *)
         myiw::iw $@
         ;;
     esac
   }

The previous function has been moved to a helper function so it can be used
by the new ``scanconn`` command, and since bash doesn't seem to like
recursion. The ``match_ssids`` command is also given the ``myiw::``
namespace prefix and is adjusted to use ``myiw:iw scan``.

Loading the above and shortening the pattern a bit,
the final call to connect to MBTA Wi-Fi can become a nice simple:

.. code-block:: bash

   myiw scanconn MBTA

My version of the above also include relevant documentation and
some support for optional chattier output, but the core logic
remains the same. That file is availble at:
https://gitlab.com/mwhipple/mw-config/blob/master/bash.d/myiw.bash

cited and topic == 'mbta'

.. rubric:: References

.. bibliography:: /core.bib
   :filter: {docname} == docnames
