A Build Tool for Your Build Tool(s)
===================================

A significant additional value that it generally provides is to verify and/or
configure that underlying tooling. This avoids a self-referential mess where software
attempts to introspectively validate its own installation (or lack thereof).
In many software projects this is addressed by having (hopefully documented)
steps that must be run prior to building the tooling and hoping that such
configuration doesn't introduce subtle conflicts with such steps for another
projects. As make is standard and ubiquitous it can perform automation and validation
of such configuration so getting any project set up can be as simple as jumping
right to calling into make and having it take care of things or provide you with
targeted guidance.

Another more obvious value in the same vein is when multiple build tools are used
in the same project. Often a project may contain multiple relatively independent
modules for which tooling may evolve divergently or may provide distinct enough
behavior (such as in the case of end-to-end tests) that the technologies involved
may make consistent build tooling impractical. In cases where such modules are peers
rather than aligning with a parent-model relationship it is unlikely to make sense
for one technology specific build tool to drive another.

A continuation of the previous module concept would be that it may also be helpful
at times to compose a build not only of multiple modules within a single repository
but out of multiple repositories. make offers a neutral and natural solution for such
composition.

A Build Tool for Portable Tasks
===============================

Build tools which are oriented toward specific technologies deliver knowledge of
the details and conventions for those technologies, but a significant amount of
development activities are neutral to any technology. A basic example of this is
interactions with the used VCS system (normally git). This specific concern is
particularly prevelant when working on a project that makes use of containerization.
The shipping container metaphor embraced by tools like Docker promise portable
tooling and packaging, so the smoothest way to realize that value is to use such
constructs directly rather than hiding them behind obfuscatory implementations.

A large advantage to using make for many of these tasks is the practically
non-existent gap between the make recipe and the resulting shell command.
Indeed, switching between having make invoke such a command for you and performing
and tuning it manually is intuitive and trivial.
