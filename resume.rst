.. _resume:

#########
My Resume
#########

My current resume is available :download:`as a PDF </resume.pdf>`.

*******
Content
*******

My resume content is shaped by tastes I have acquired while working
and being involved in various stages of recruiting. Its target
audience is technical interviewers rather than recruiters, and it is
intended to also target a culture within which I would want to work.
It attempts to provide talking points which can provide fodder during
an interview, and it strives to avoid noise or bloat.

In particular it takes a project based approach where each item is a
description of a project and some of the key tools used while working
on that project; there is no dumping of skills or keywords without a
surrounding context. This hopes to allow quicker discrimination of
items that may be of interest, and to provide inroads to conversations
about those items (having spent a fair amount of time when conducting
interviews trying to establish such connections).
This also helps present a more problem-oriented
perspective: the problems were addressed and tools were adopted to aid
in that solution rather than advertising what may read as a fixed
set of tools looking for a problem at which they can be thrown.

I confine my resume to two pages: anything more than that seems likely
to be a burden to a potential interviewer (who is likely to be sifting
through many candidates)  and may suggest an inability to filter or
prioritize. My experience has also been that most candidates seem to
need to stretch their memories when discuss any items past the first
page of their resume, let alone any deeper.

=================
Template Commands
=================

To support the project oriented approach outlined above, some commands are used to
represent some of the elements while adhering to the tenets of *Logical Design*
:cite:`LaTeXBook`.

.. literalinclude:: /resume.tex
   :language: latex
   :start-after: %% @defgroup Local structural commands { %%
   :end-before: %% } %%

============
Head Spacing
============

There is a fair amount of vertical space after the name/title head
content when using the ``casual`` style. Whitespace can be aesthetically
pleasing, but reducing this *may* be desirable to accommodate more
content. Blindly correcting such spacing can often be a blueprint for
a house of cards, so configuring it out would be preferred.

After poking around a bit, the cause of the additional whitespace
is the additional 2.5em of vertical space while breaking the line
at `moderncvheadii.sty L161 <https://bazaar.launchpad.net/~xdanaux/moderncv/trunk/view/head:/moderncvheadii.sty#L161>`_
:cite:`LatexModernCVSource`. This does not appear to be configurable
(and making it so does not seem worthwhile) but now that the cause
is understood it can be reduced or removed with a line such as:

.. code-block:: latex

   \vspace*{-1.25em} % Correct for 2.5em space in makecvhead.


.. rubric:: References

.. bibliography:: /core.bib
   :filter: {docname} == docnames
