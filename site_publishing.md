---
title: Site Publishing - Matt Whipple
---

- [Home](./index.html)
- [Site Generation](./site_generation.html)

This site is published using GitLab Pages[@gitlab-pages-docs].  It
makes use of my custom domain with TLS provided by the GitLab support
for Let's Encrypt[@gitlab-pages-lets-encrypt].  The Let's Encrypt
integration can apparently suffer from an occasional hiccup (which I
just experienced) that can be resolved through a retry as documented
in the troubleshooting section of the relevant GitLab
docs[@gitlab-pages-lets-encrypt, #troubleshooting].

The documentation for GitLab pages[@gitlab-pages-docs] is overall
solid but it seems fairly example heavy which leaves the need for a
fair amount of inference based on those examples, though the reference
documentation seems fairly compehensive.  The publishing itself is
handled by a `.gitlab-ci.yml` file and is tied to a "specific `job`
called `pages`" within that file[@gitlab-yaml-pages].

I've fortunately also recently spent a significant amount of time
using GitLab for CI at work and have therefore become more comfortable
(and hopefully have landed on some appropriate practices).

In previous incarnations I seem to have been overly biased towards
particular ways to use containers that don't map onto CI servers and
was therefore making some of the concerns here needlessly
complicated. This is something I'll likely revisit later.

# .gitlab-ci.yml

The GitLab provided reference documentation[@gitlab-yaml-reference]
serves a fairly complete guide for options that can be used within this file.

## Rules

Publishing should only be done for the primary branch so a workflow will be configured
to target branches [@gitlab-pages-docs-scratch].

~~~{.yaml file=.gitlab-ci.yml}
workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
~~~

## Pages

The special `pages` job is defined which should be run during the
`deploy` stage [@gitlab-pages-docs-scratch].

~~~{.yaml file=.gitlab-ci.yml}
pages:
  stage: deploy
~~~

Pandoc provides an image which acts as a solid starting point.
The image typically calls pandoc as the entrypoint but this workflow
is going to use make instead so the entrypoint needs to be overridden.

Additionally the GitLab will invoke things using its custom
gitlab-runner which can cause issues if a shell is explicitly provided
as an argument, and so an empty entrypoint is preferred here.

~~~{.yaml file=.gitlab-ci.yml}
  image:
    name: pandoc/core:2.17.1-alpine
    entrypoint: [""]
~~~

### Install make

`make` is not included by default in the `pandoc` image and so can be installed
in a `before_script` definition.

~~~{.yaml file=.gitlab-ci.yml}
  before_script:
    - apk add --no-cache make
~~~

### Build site

Invoke the make target to produce the generated site.

~~~{.yaml file=.gitlab-ci.yml}
  script:
    - make site
~~~

The (already existing) `public` directory will be published as an artifact:

~~~{.yaml file=.gitlab-ci.yml}
  artifacts:
    paths:
      - public
~~~
