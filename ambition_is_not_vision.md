---
title: Ambition is Not Vision
---

- [Immaterial Implications](./immaterial_implications.html)

While virtually anyone can set out ambitious goals, the ability to 
pursue a defined vision requires far more insight and discipline.
Much of this will draw on business jargon for which I'm not a
particularly established source, so this should be supplemented by
more authoritative supporting information.

A fairly clear concept to distinguish the concepts is that of a vision
statement. An effective vision statement is intended to provide the
focus which enables prioritization and the decision of not only what
to do but what _not_ to do. This in turn should help resolve core
competencies which provide defensible and differentiated proposed
values.

Ambition without vision is lacking that focus and seems to manifest as
goals masquerading as strategies. This surfaces as statements such as
"we need to dominate the market" or "we need increased engagement" and
can even extend to include plans that are contingent upon such things
happening but are lacking the essential element of _why_ such things
would happen.

Ambition without vision is not only rudderless but encourages
significant waste. Most prominently overly wide ambitions tend to
thinly stretch resources and efforts, but additionally much of that
breadth is likely to dip into some combination of commodities and
externally available value. The latter lends itself to fashioning
inferior doppelgangers of established solutions and the former
may invite excessive costs; any of which exposes susceptibility to
flat world competitors delivering something deemed better or less
expensive or both.

This notion seems to be well supported by the notion that most
successful business were built on top of doing one thing particularly
well, and none of this is likely to provide any additional insight
over any conventional business wisdom that I've heard, but
it seems to be repeated more often than it is applied (which is likely
true of most things).

While looking up the Steve Jobs quote on simplicity I also came across
these which are appropriate:

> [...] it comes from saying no to 1,000 things to make sure we don't
  get on the wrong track or try to do too much. We're always thinking
  about new markets we could enter, but it's only by saying no that
  you can concentrate on the things that are really important.
  [@bbc-steve-jobs-quotes]

> People think focus means saying yes to the thing you've got to focus
  on. But that's not what it means at all. It means saying no to the
  hundred other good ideas that there are. You have to pick
  carefully. I'm actually as proud of the things we haven't done as
  the things I have done. Innovation is saying no to 1,000 things.
  [@goodreads-quote-steve-jobs-focus]
