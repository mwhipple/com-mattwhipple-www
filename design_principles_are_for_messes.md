---
title: Design Principles are for Messes
---

- [Assorted Thoughts](./assorted_thoughts.html)

---

As people that have worked with me can attest I tend to stongly
advocate for adhering to design principles. While I don't think I've
been called a purist I have been described by one team as
"uncompromising" and I periodically encounter detractors who aver that
such pursuits are more ideological than practical but most of the time
these voices seem to be missing the most integral virtue of such
designs...well structured design is not about trying to do everything
right but rather to allow for things to go wrong. I typically phrase
this as "good design is knowing how to contain your messes".

Sooner or later something is going to slip - there may be any
combination of time pressure, inexperience, ignorance, or sometimes
even indifference (which may not be an immediate problem if the
functionality has an uncertain future) that results in work that is
less than desired, but good design provides a means to stop any
malfeasance from spreading. In addition to suffering from a possible
broken window effect if practices are not contained by design then
some future work may end up intentionally or accidentally building on
top of it; suddenly what may have been a short term compromise
lodges itself deeply into business logic and may become one of those
ever-widening house of cards that no one wants to touch until it's one
of the legacy monsters that torments the poor soul that is stuck
trying to maintain it.

If the intent _were_ to pursue doing everything right then many such
designs wouldn't be necessary to begin with. Such principles are
typically about making the systems easier and safer to change with the
acknowledgement that such change is likely and the frequent cause of
such change is not having done things quite right up front. In
addition to allowing for the system to evolve if everything were done
right then softer concerns such as comprehensibility could likely be
addressed through documentation. Such a model ultimately worked for
$\TeX$ so it's certainly a viable one in some cases.

For those of us that are dealing with trying to deliver nebulous
business needs to a constantly changing environment and ticking clocks
it seems reasonable to not only expect but to embrace the need to make
some sacrifices here and there and good design allows for any such
compromises to be applied tactically and managed over time.