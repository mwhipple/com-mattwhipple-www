---
title: Books
---

- [Home](./index.html)

---

I've regrettably been a relatively poor reader
for much of my life. I've probably consumed a large and
consistent volume of technical material, but have spent far
too little time reading a wider variety of topics. I am recently
starting to remedy that.

Here I'll catalog books I've read since starting this cataloguing
process with some thoughts about each book.

- [10% Happier](./book-10_percent_happier.html)
- [Applied Empathy](./book-applied_empathy.html)
- [Apprenticeship Patterns](./book-apprenticeship_patterns.html)
- [The C Programming Language](./book-the_c_programming_language.html)
- [Designing Data-intensive Applications](./book-designing_data-intensive_applications.html)
- [Domain Driven Design Quickly](./book-domain_driven_design_quickly.html)
- [Drawing School: Fundamentals for the Beginner](./book-drawing_school.html)
- [Effective C](./book-effective_c.html)
- [The Field Guide to Understanding 'Human Error'](./book-the_field_guide_to_understanding_human_error.html)
- [The First 90 Days](./book-the_first_90_days.html)
- [Fundamentals of Software Architecture](./book-fundamentals_of_software_architecture.html)
- [The God Delusion](./book-the_god_delusion.html)
- [The Healthy Programmer](./book-the_healthy_programmer.html)
- [The Highly Sensitive Child](./book-the_highly_sensitive_child.html)
- [Kafka: The Definitive Guide](./book-kafka_the_definitive_guide.html)
- [Literate Programming](./book-literate_programming.html)
- [Managing Projects with GNU Make](./book-managing_projects_with_gnu_make.html)
- [Music Theory](./book-music_theory.html)
- [SysML Distilled](./book-sysml_distilled.html)
- [You Can't Be Neutral on a Moving Train](./book-you_cant_be_neutral_on_a_moving_train.html)
- [Your Brain at Work](./book-your-brain-at-work.html)