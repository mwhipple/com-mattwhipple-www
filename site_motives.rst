.. _AboutMotives:

**********************
Motives, Privacy, Etc.
**********************

On today's Internet it is important to be aware of the motives
of the services you may use, which can often be harder to
ascertain in the case of free sites.
This site is initially for my own use and is not expected to receive
significant  traffic. I have not
included anything on this site to track usage (such as analytics)
nor is there any means to monetize access (such as advertising).
If this site happens to become popular enough to drive the
hosting costs past the point of being negligble then some form
of monetization may be added at that time to offset those
costs; any additional funds will likely be donated to charity and
the specifics related to such a change will be reflected on this site.


