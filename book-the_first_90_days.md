---
title: The First 90 Days
---

- [Books](./books.html)

---

I acquired a copy of The First 90 Days[@the-first-90-days] many years
ago while working for Havard Business School Publishing(HBSP). I started to
read it a few years ago when changing jobs and it didn't resonate with
me at all, but I recently read it while starting a new job (and it was
coincidentlaly recommended to me during a coaching session) and I
found it dramatically more insightful. Such a change in perspective is
likely due to having originally read it in a place where I was _very_
much in an independent contributor headspace with idealistic notions
of meritocratic engineering cultures and have since caromed through
some more politically charged managerial positions. Originally the
book did not seem personally relevant and many of the underlying
concepts likely felt stale after having been somewhat immersed in
their ilk while working at HBSP, so I ended up returning the book to
collecting dust after a couple chapters.

This time around, however, I found the book very valuable to the point
where I've noticed there's a newer edition that I'd be strongly
tempted to pick up in the future. When entering a company in a
leadership role it provides a good mix of both ideological frameworks
and practical approaches to impactfully enter an organization while
gauging how well such entry is progressing. I'll be looking to
draw on such information periodically and systematically, checking in
with the book itself and metrics derived from it to guide my
initial navigation in my new role.