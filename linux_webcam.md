---
title: Linux Webcam
---

- [Software](./software.html)

A webcam is something I've typically never bothered to configure on my
personal laptop, but the prevelance of post-COVID teleconferencing has
led me to want to make sure I have a usable device of my own for that
purpose.

I had figured I was missing required drivers, but after walking
through the relevant steps provided on the Gentoo wiki webcam
page[@gentoo-webcam], everything seems to have worked without issue
(and my kernel alredy had the required configuration settings).