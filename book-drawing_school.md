---
title: "Drawing School: Fundamentals for the Beginner"
---

- [Books](./books.html)

---

As part of looking to expand my interests I've decided to invest in
improving my mark making abilities. I skimmed through Drawing
School[@drawing-school] which seems to provide a fairly wide survey of
information in terms of practices and resources along with plenty of
aspirational drawing excercises.

As I've currently read through the book but not acted upon it and my
drawing skills likely peaked around middle school, this book will
provide a well for me to draw from for quite a while as I gradually
look to improve this skill but so far it has largely resulted in a
pending task for me to get a decent set of pencils.
