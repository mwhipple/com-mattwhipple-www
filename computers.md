---
title: Computers
---

- [Home](./index.html)

---

This area will house assorted information about computers, and at some
point I should define what exactly qualifies as a computer given their
modern ubiquity. This will include a mix of hardware and software -
though primarily the latter since that aligns with my background (and
is likely a wider field overall...which is kind of the whole point of
computers). 

- [Software](./software.html)
- [XPS 13](./xps_13.html)
