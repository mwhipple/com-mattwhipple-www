---
title: Node Event Lag Timing
---

# Motivation

A major concern when using NodeJS is blocking the event loop.  This is
particularly a problem when writing backend code since while in some
cases it may not adversely impact throughput it can lead to
significant unfair quality of experience and hamper observability.

Keeping a low maximum time spent per event loop tick can avoid many
such pitfalls (means to reliably accomplish this will be covered
elsewhere) and can ultimately provide similar behavior to that
provided in a more typical preemptive multitasking environment (where
that max tick time is analogous to a per-jiffy timer interrupt).

# Probe vs. Instrumentation

Tick time can be tracked by attaching a handler to the libuv event
loop but that requires wiring in C++ code. A less invasive approach is
to submit an event for the next tick and time how long until it is
handled. This is less likely to be reliable as it is
implemented within the event loop and therefore is subject to being
impacted by blocking: for example if the event loop is blocked for one
second during which time such a probe _should_ have been sent it will
be latent until the event loop is freed and therefore that second lag
will avoid detection. Such probes are only effective if the probe
happens to be sent just prior to any code of concern being executed.

On a production system such probes should likely be used for periodic
sampling and are therefore not to be trusted for any fine grained
data, but over time the numbers should reflect typical system
behavior. Produced metrics may therefore be useful for trends but
are of questionable utility for diagnosing any specific incident;
the configuration of a libuv handler should be preferred for such
insight (more information may follow regarding this).

# Scope

If such probes are used on every tick then they should provide a
complete picture of any latency within the event loop. While this may
be prohibitively expensive in a production environment it seems very
reasonable as a profiling