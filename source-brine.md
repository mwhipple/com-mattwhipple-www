---
title: Source - Brine
---

- [Source Reading](./source_reading.html)

A bit of a cheat for reading source is one project that
I primarily authored: a Cucumber based DSL for writing
functional tests of REST APIs called Brine[@brine-github].
It's not foreseeably likely that I'd use the project enough
to integrate the source into this site but it's likely enough
to be used again to keep around.

Left to my own devices I'd be more likely to test a REST API
using a combination of small tools such as bash, curl,
and jq, but presented with a need to manage functional tests
for the semantics of a REST API where readability or easily
collaboration seem valuable then Brine would remain a
contendor (combined with tools such as Prism for schema
validation and Pact for consumer driven contracts).

My heavily biased opinion on the code is that it reads well
and is very economical: the project itself largely glues
together several other libraries and so delivers a wide
range of functionality with few lines of code. The single
existing runtime is ruby and I'm not a rubyist and so it
may not be overly idiomatc but does not _seem_ to be off-the-mark.

## Brief History

The origin of Brine fairly directly explains the functionality
and the current state.

Several years ago I joined a team that owned a REST API which
included a very effective QA engineer who oversaw a substantial
catalog of RSpec tests across multiple projects. She later departed
the organization leaving those tests to be maintained by the engineers.
I dove in and quickly grew to appreciate RSpec but became concerned
about the range of practices within the tests. The tests themselves
performed a small number of operations but did so using different
libraries and different approaches: often in ways that obfuscated their
intent, challenged testing best practices, or required knowledge of
decisions or assumptions within the test code. All of these were likely
to be pragmatically unimportant for someone primarily working with those
tests, but bred confusion and wasted time when those tests
were one of many contexts being switched between. I set about establishing
more common structure and patterns for the tests and looked at
leveraging more standard RSpec functionality.

After some time the QA engineer position was backfilled and so those
tests acquired a new de facto owner. My experience with the tests
along with trying to preseve some level of consistency while the new
engineer onboarded led me to create Brine. The existing test
technology seemed too flexible for what was needed and that
flexiblity bore entropy, particularly as it passed through the hands
of many chefs. As new tests were created I quickly threw Brine
together to rein in further divergence.

I generally had (and have) mixed feelings about Cucumber, but it
seemed a compelling foundation for the envisioned work. It was likely
also a crucial piece in allowing me to quickly spin up a fairly
full-featured means to execute tests (which acquired utility surpassing
the RSpec tests). The team involved (including the new QA engineer)
had a total of zero ruby afficionados at that time, but the existing
suites were in ruby and ruby was the tool of choice for the waning QA
department so it seemed the least controversial language selection.
One of the further motivations of Cucumber was that it paved the way
to migrate away from ruby without disrupting the specifications, but
an alternative runtime has still yet to be implemented.

The initial implementation was done within a day or so and then
was largely refined as needed with some initial course corrections.
The number of tests written using the library grew steadily and
so after the library was extracted the changes were made while coordinating
compatibility with those existing tests. In a fairly short amount of time
the library landed on a place that was _good enough_ such that new
tests could typically be created by any of a group of developers then
contributing to the end-to-end test suite. The rate at which a new
feature was _necessary_ declined: some workarounds were established any
any of those that were particularly ungainly would also elicit an
enhancement such that the workaround could be replaced.
I also accumulated a list of tasks that seemed worth
doing for reasons like hygiene, user-expressed preferences, or
reconsidering past decisions (and things like the aforementioned
alternative runtime(s)), but as the project rarely needed attention and
I was not a common user of it most of those ideas largely languished.
I may have been able to establish slightly more of a community if I
ever attempted to bring attention to the project, but it seems unlikely
that would have made a substantial difference. Likely one of the
trickier hurdles was simply establishing the language for the DSL
which seems to have stabilized in a consistent albeit verbose
package (supporting a terser alternative is one of the outstanding
bits of work). Ultimately many of the trickier pieces seem to have
been addressed and so it seems as though there's just a lot of
somewhat straightforward gaps to fill in.

I've also made use of Brine in other projects: the published Docker image
signficantly reduces configuration overhead such that the specs can simply
be written and executed in a container which is one of the major reasons
why this remains a viable option in my estimation.
