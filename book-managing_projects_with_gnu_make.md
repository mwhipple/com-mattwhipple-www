---
title: Managing Projects with GNU Make
---

- [Books](./books.html)

---

`make` is a technology I often end up introducing or optimizing within
projects. Often builds have little to no orchestration and
in heavily containerized builds particularly `make` can help coordinate
a lot of functionality (handling containerization with more
ecosystem-oriented build tools can introduce varying levels of
unfortunate complexity). Having spent a fair amount of time recently
working with NodeJS projects and given the fact that neither `npm` nor
`yarn` are designed to model a build graph, I found myself introducing
a significant amount of `make` (particularly due to how unfortunately
rich some of the previously implicit build graphs were).

After introducing it I was frequently asked about good `make` resources
and I unfortunately could not readily recommend decent introductory
material (other than the manual) and so I took some time to go through
Managing Projects with GNU Make[@managing-projects-with-gnu-make] to
assess how well it could fit that role (I read the GNU Make Book a few
years ago and it's not exactly introductory). I'm not particularly
optimistic that a _book_ recommendation is likely to be read (as
opposed to something shorter), but at least it can be given.

The book is decent but is unforuntately a bit dated which likely leads
to a few drawbacks. `make` may start off at a disadvantage of being
perceived as an out-of-date tool and books that discuss "new"
technologies like CVS (when everyone knows SVN is the new hotness :|)
may not help its case. This may be more symptomatic of short term
collective memory issues that pervade software engineering but
dusting off some of the older material (perhaps using cutting edge
tools like `sed s/CVS/git/g`) and sprinkling in references to invoking
newer tools like Go and Docker could quickly confer a sense of obvious
relevance and the specific role that it can continue to provide fairly
optimally as a software tool. More modern developments such as the
proliferation of POSIX compatibility and containers also have major
implications in terms of portability which is given significant
but outdated attention.

Another possible concern with the age is some of the overly specific
guidance that is provided. When the book was published (2003) it was
more typical for a book to strive to be fairly self-contained, but now
I'd strongly argue that books should look to provide net value on top
of online resources (with the quintessential example being Stack
Overflow). Specific scenarios are covered at length where it would be
far more efficient to provide the complementary insight which can
guide optimal consumption and extension of such information which can
primarily be referenced elsewhere. Rather than providing and analyzing
fairly complete examples the book could focus more specifically on
some combination of details for organizational approaches and recipes
and offload much of the contextualization to linked examples.

I personally picked up some practices I'll be looking to
incorporate. I'll likely make use of a couple of the tricks covered
and getting up to speed on and making use of the built-in rules has
been on my to-do list for a while but this book has pushed that
along. More interestingly an awareness of `call` being more macro
expansion rather than runtime expression evaluation is a welcome
conceptual correction, and most practically the use of multi-line
`define` statements is certainly an adjustment I'll pursue over my
current tendency to simply use the less legible recursive assignment
with line continuation.

I read this book with the idea of adding more `make` resources to this
site (and as I read through the `make` source code), but I'm unsure when
and if I'll get back to that as I've emptied and refilled my plate
with other ideas. I do have some nascent ideas floating around in
terms of checklists and `make` but I may have minimal expected use of
it in the near term (other than this site) unless I get asked about
it. Especially given that this book is freely available I'd likely
recommend that anyone read the first part for a primer on the basics
and then track down other resources to explain some of the more
sophisticated uses.