==========
File Model
==========

The first concept worth calling out (which needs some supporting sources)
is that make is oriented more towards providing more declarative rules (recipes)
used to create files rather than executing more general imperative tasks.
The latter often manifests as a Makefile which is dominated by PHONY targets
even for cases where files are being produced, and awkward command invocations
of verb sequences such as ``make build`` rather than far more idiomatic indications
of what is being made such as ``make app``. Makefiles should therefore focus on
modeling the build in terms of produced files and the relationships between those files.
Imperative target names should indicate that the primary purpose of a target is not
to produce an output file.

=============================
Minimize Exposed Surface Area
=============================

A Makefile for any non-trivial project is likely to contain a lot of targets,
but generally most of them can and should be left as internal details. Only a
subset of those targets should be selected for direct use. This can ease use,
keep noise out of the Makefile, reduce maintenance costs, and enable more flexible
evolution of the build model.

==============================
Prefer the Simplest Assignment
==============================

Simple assignments (``:=``) should be preferred whereever it makes sense for a value
to be expanded at assignment time, and in most other cases ``=`` should be used.
``?=`` should only be used for those values which are *normally* provided through
the environment. There is a tendency to overuse ``?=`` to support overriding of
values through environment variables, but such overrides can be done regardless of
the assignment type by specifying the values as arguments to make (i.e. ``make all FOO=bar``).

=============================
Use of Braces and Parentheses
=============================

make supports use of either curly braces (``{}``) or parentheses (``()``) after a
``$`` for expansion. Although the two are interchangeable, I try to adopt usage which
somewhat mirrors bash's use of braces for simpler variable expansion and parentheses
for more complex invocations (subshells in bash). These are interchangeable in make
because there is no difference: any variable expansion amounts to the evaluation of
some expression. The complexity of the expression being evaluated and when that
evaluation is done may vary, but for the most part the "simpler variable expansion"
amounts to a small expression which doesn't accept parameters.

A clearer distinction from a technical standpoint would likely be to use braces for
values which have been assigned using simple expansion (and therefore do not involve
further evaluation), but that seems more fragile (as it would hinder changing the expansion
strategy) and less likely to be useful. Instead the distinction will be that
nullary expressions primarily use braces while any parameterized calls will use parentheses,
but there may be exceptions as the intent is to distinguish values from operations which
are likely to act upon those values.

From a practical standpoint this practice can assist in visually parsing expressions 
like ``$(addprefix ${PRE},$(addsuffx ${POST}))``; matching the braces is (hopefully)
easier and there's a strong indication of what evaluation is expected to be performed
within the expression and what should be simple values or those provided elsewhere.

======================================
Include Trailing Slash for Directories
======================================

.. todo: Which function?

Directories will be represented using a trailing ``/`` similar to output such
as returned by ``ls -F``. This practice was originally instigated because one of the
``make`` functions (I forget which...this will be clarified later) returns results
in this format and its far easier to append the slash than it is to remove it.
The practice was fully embraced as it is more expressive and preserves a cleaner
division of responsibilities where such defined values provide a container/namespace
and the fact that they're directories becomes a more encapsulated implementation
detail.

============================
Carve Out Localized Sections
============================

Definitions should be kept as close to their use as possible.
Global definitions should be placed at the top of the file but otherwise
as much as possible should be defined in group sections within this file.
This can help provide a level of modularity or hierarchy which can help
in understanding the build structure on both a macro and micro level.
The locality can also convey scope which can aid in maintaining the project.

This may also be addressed through child Makefiles if a build grows large enough,
but generally in cases where make is primarily used to orchestrate other tools this
is likely to be overkill.

====================
Template of Sections
====================

Each section defined will follow a general bottom-up pattern where lower level
pieces are defined first, followed by more specialized higher level constructs.
This will generally match a sequence along the lines of:

* local variables
* pattern rules
* real targets
* PHONY targets composed from the above which likely act as the primary interface

The specific ordering is likely to prefer cohesive grouping which is likely to
deviate from any prescribed ordering, but sections as a whole should conform to
that pattern.

While my general style prefers a top down definition style, bottom up is adopted for
make to facilitate use of simple assignment (which requires any dependencies to be
resolved before they are referenced).

===================================
Lazy Expansion with Local Variables
===================================

It is common for a project to have tasks with build recipes that only slightly vary
in well defined ways. In the simplest cases the difference may simply be environment
variables that could be set within the recipe (or use target local variables). To modify
the command itself the command can be composed out of variables with internal variables
acting as named formal parameters/slots which can be populated by binding target local
variable values. Provided is a contrived example of running a more comprehensive set of
integration tests by expanding on a basic test target.

.. code-block:: make

   ##
   # Run tests using a commonly defined invocation.
   #
   # @param TESTS_DIR Specify the directory in which the tests are contained.
   ##
   RUN_TESTS = ${CHECK} ${TESTS_DIR}

   unit-tests: TESTS_DIR = tests/unit
   unit-tests: ; ${RUN_TESTS}

   integ-tests: TESTS_DIR = tests/integ
   integ-tests: export REAL_ENV = true
   integ-tests: unit-tests ; ${RUN_TESTS}

The above could result in integ-tests running unit-tests with ``REAL_ENV`` set to
presumably replace some test doubles with something more realistic and then applying
the same command pattern to a different test directoy. Judicious use of variations of
this approach can allow for the definition of powerful building blocks which can help
tame build recipes with minimal logic being introduced into the build itself.
