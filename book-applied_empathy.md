---
title: Applied Empathy
---

I've recently been receiving management/leadership coaching for work
which has been what will hopefully be a transformative experience.
The timing has been particularly appropriate as I look to finally
unplug from a technical focus and approach work (and life) more
holistically.

In the course of that coaching I was provided a copy of
Applied Empathy [@applied-empathy] which was consumed fairly quickly.
The book has a lot of useful information, though I honestly needed to
quickly skim through it to remember what that was as some other recent
books on similar subjects have taken far firmer root in my memory.

What is likely the core value that this book delivers is a practical
framework that could be applied to encourage empathic thinking. The
definition of assorted archetypes and models provides a clear path in
both reappraising individual scenarios along with fostering more
expansive exploration of how things are likely to be perceived.

The proferred value of empathy itself did not feel relevatory but was
well supported. This book does suffer from what seems to be a common
affliction (which likely has a name I should track down)
where a fairly narrow ideology is stretched past obvious applicability
and is treated in isolation rather than being contextualized with
alternative relevant ideas. This likely stems from some of the
underlying topics being fairly massive and this book being fairly
short, but offering empathy as an independent solution to larger
organizational concerns feels simplistic both in terms of an
organization itself and the dynamic environment in which it
operates.

Overall this books provides some useful practices to incorporate into
a toolkit but the hawking of empathy as a tonic for a range of
business issues warrants a grain of salt.