Consistency is one of my main drivers for using make, and key piece is that is
also adopting some standard targets so common tasks can be performed
seamlessly across projects. This will extend the standard GNU targets.

make check
	Perform any validation and linting of the software.

make help
	Display information about working with this project. See `make_recipe_help`.

make todo
	Display pending tasks for this project or a link to such tasks.
