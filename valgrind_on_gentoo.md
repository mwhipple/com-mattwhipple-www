---
title: Valgrind on Gentoo
---

- [Home](./index.html)

Valgrind is a fairly widely used analysis tool suite which can
help smoke out a range of issues for native code. It's been on
my list of things to pull into my build process for such projects
but some suspect recent code of mine gave it a friendly bump up
to the top of the queue.

Unfortunately when trying to execute it on one of my Gentoo
systems I ran into an error at the time invocation. Thankfully
this is the same error that is reported on a relevent
Gentoo Wiki page[@gentoo-debugging], so the guidance on that
page _should_ help resolve the issue for me...and it did.
