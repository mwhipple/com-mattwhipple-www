---
title: The C Programming Language
---

- [Books](./books.html)

I've picked through "The C Programming Language"[@DBLP:books/ph/KernighanR88]
(K&R) a couple times over the years, but have only recently read it cover to
cover. It is understandably a classic text; it is well written
with compelling exercises and covers a lot of fundamentals.
I'd posit that any serious software engineer should read through it,
especially given how entrenched C is within modern systems
although few developers may directly interact with it on a regular basis.

The primary drawbacks are likely obvious given the age and limitations of
C; there is a chasm between the content in the book and what most
current developers are likely to be working on. When considering this
book next to what is potentially its more modern doppelganger
"The Go Programming Langugae" (also written by Kernighan and having
a similar style) there is a glaring difference between how readily
the knowledge can be applied to modern problems. It seems very important
to note, however, that such utility is not free and is delivered by
layers of enabling functionality while this book stays far closer to
the metal.

This book also clearly defines the boundaries between the
(small) language itself which provides the logic which is unlikely to be of
practical value and the system libraries which provide those side
effects that put the rubber on the road. This is very likely due to
the integral role C played in enabling program portability but is an
often overlooked distinction as treatments of languages are often lumped
together with standard libraries and package managers such that some
of those pieces become more difficult to tease apart (sometimes particularly
so if the notion of management of external packages is integrated tightly
into the language packages). This is of particular note given aforementioned
drawbacks in that the chasm to wider practical benefit is filled by such
functionality that is largely outside of the language itself and is
therefore not precluded but simply omitted due to focus on the language
itself and core interactions with the operating system. This is not to
say that other languages themselves don't provide mechanisms which
may dramatically simplify some of these needs or amplify key aspects
nor to advocate general use of C across wider domains, but rather that
C logic is a more direct analog
for typical machine behavior and is foundational in most modern
systems; understanding of the language itself therefore can foster
deeper understanding of the facilities more readily available elsewhere.
