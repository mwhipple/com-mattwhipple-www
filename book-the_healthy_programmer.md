---
title: The Healthy Programmer
---

- [Books](./books.html)

---

As part of expanding the topics I'm reading on O'Reilly Learning I
picked my way through [@the-healthy-programmer]. I've been
very fortunate to generally feel healthy; while I don't have notable
_unhealthy_ habits I certainly am very lax about having any healthy
ones (or healthful though that sounds more gimmicky). As I'm getting
older and have settled into a pandemic-induced but now indefinite
state of working from home I certainly want to be vigilant against
settling into being overly sedentary which seems to be an odd skill if
not tendenency of mine (I don't have any issue spending exorbidant
amounts of time barely budging).

The book largely aligns with some incremental health initiatives I've
already started for myself and will return to mine some of the
exercises that it catalogs. Likely the single biggest takeaway for me
was the Pomodoro technique that I'd uncannily started to independently
adopt in the month or so prior to reading the book, but the book
substantiated, named, and refined the practice (I'd landed on
a mix of 50 and 25 minute intervals whereas the info in this book led
me to drop the longer variation).