---
title: Ekstra - Matt Whipple
---

- [Home](./index.html)

# Overview

The most ambitious and self-indulgent project as part of my latest
tool reboot is the creation of my old text editor called *Ekstra*.
This was started by following the "How To Build Your Own Text Editor"
[@snaptoken-kilo] tutorial.

I'll be rapidly diverging from the initial code as I modularize the
pieces and work to add increased robustness and flexibility.
I'll also be working on porting everything to Rust so the effort
will likely shift by the summer of 2021 to be a mix of porting
and extension per my [road to rust](./road_to_rust.html).

# Naming

The origin of the name "Ekstra" is that several years ago I started
down this path with the original idea of building on top of/extending
GNU Zile. The combination of wanting a name that conveyed extensibility
along with a contemporary interest in rhyming slang led me to
Zile -> Mile -> Extra Mile -> Extra, and then the realization that
replacing the "x" with "ks" yielded a prefix of "eks" which seemed
relatively unique made the name stick (although basing things on Zile
was abandoned shortly thereafter).

In this latest incarnation I've decided to ekstend that general pattern
thoughout the code to name key objects using words that have
"x"s which are then replaced with "ks"s. This is somewhat a manifestation
of the aforementioned self-indulgence but is also driven by the desire
to adopt names that avoid collision through some form of namespacing while
also not using overloaded terms that may breed confusion: overall providing
a mechanism to help in working through one of the hard things[@fowler-two-hard-things].
A helpful side effect is that this is also likely to help boost my Scrabble scores.

# Files

- [ekson](./ekson.html)
