###########################
Resetting Your AWS Password
###########################

**********
Background
**********

Accessing the AWS Console is one of those activities that I
end up having to perform just infrequently enough that it as
often as not leaves me struggling to remember what my password
is. While using AWS itself is fairly constant, most of those
interactions are through tooling that either has some alternate
saved credentials or uses cached credentials and therefore I
need only ro provide an MFA token; this leaves console access
for those atypical uses that intermittently pop up and for which
for whatever reason the console seems more attractive than
alternatives. Combined with a policy enforced password rotation,
this leads to often not even using a password before it expires,
and rarely using one often enough to remember it.

Using a password manager is certainly a good option to avoid this
type of problem and can offer other security benefits, but there
are times when this may fall short. With something like an AWS
password it may need to be retrieved because something seems to
be going wrong and access to the managed password may not be
immediately accessible: and if the password has also expired it
may result in that managed password becoming invalidated. In such
situations the password may effectively be lost and need to be reset.

******
Recipe
******

An AWS console password can be reset through the use of the AWS CLI
provided the user has proper permissions and the appropriate credentials
have been configured for the CLI. This is a readily documented
feature and by no means a secret; when I was last given an OPS managed
AWS account I was not given a console password at all and had to enable
one for myself. This seems worth capturing given the frequency I see other
developers request that their passwords be reset combined with my needing
to rediscover the specific incantation (especially provided the additional
security concern outlined below).

An AWS console password can be reset using a command such as :cite:`AWSCLIIAMUpdateLoginProfile`:

.. code-block:: bash

   aws iam update-login-profile --user-name ${user_name} --password ${new_password}

Easy peasy. The above provides an additional minor concern in that
the resulting password should be provided in a way which isn't going to linger
outside of volatile memory which a command normally will if it is saved
to your history file. This could be avoided through the use of setting
``HISTIGNORE`` :cite:`BashReferenceHISTIGNORE` (or ``HISTCONTROL``)
and starting the command with a space or other ignored pattern,
however having the password echoed on the
screen is best avoided entirely to curb risks such as shoulder surfing
:cite:`ShoulderSurfingWikipedia`.

The command itself provides a viable solution by forcing a reset upon first use 
through the ``--password-reset-required`` option (where that login/reset should be done
immediately). The enforced single use not only provides safety against forgetting to
reset, but also against concurrent use of the interim credentials.

The shell itself could also be used to prevent echoing the password or leaving it anywhere
durable. A sequence such as:

.. code-block:: bash

   read -s new_password
   aws iam update-login-profile --user-name ${user_name} --password ${new_password}
   unset new_password

will provide a non-echoing prompt using read :cite:`BashReferenceread` to retrieve the value
which will then be passed to the CLI command and then cleared out so that the value is
not otherwise retrievable.
The invoking shell could also just be closed upon completion to clear the value out of
memory. This prevents the password from being displayed on the screen and the history will
only reflect the variable name rather than its value. This could be packaged more nicely
(and offer features such as password confirmation) but seems simple enough to not bother.

.. rubric:: References

.. bibliography:: /core.bib
   :filter: {docname} == docnames
