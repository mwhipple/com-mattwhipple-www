#################
Make Recipe: Help
#################

**********
Background
**********

When working with any project I immediately want to know
what my development-time options are for working with the
project are. This should include any typical build tasks
along with any expected variables or variations that may
be used. Many of these options could be made to be fairly
conventional between projects, but documentation is still
preferred in case there is any deviation, to minimize the
overhead for anyone not familiar with the conventions, and
to prevent having to remember details like the exact
invocation needed to perform that task that comes up once
every few months.

As the canonical way to perform such tasks themselves is
normally from the command line, such information should
normally also be available through the command line.
If using ``make`` consistently, such information can be
made consistently available through a target such as
``make help``: this provides an environment where the
setup for any project can be driven by the steps of
checking out the project and running ``make help``,
without needing to locate and consult where such
setup information is or to determine the relevant
information for any other build tool in use. As will be
mentioned elsewhere, make can be used to automate some
of the other development environment concerns which are
often documented as additional steps which must be performed.

******
Recipe
******

The recipe for this is about as simple as a recipe can be.
The most straightforward way to provide such information
while ensuring that it conveys the most useful information
in a digestible format is to just write the desired output
into a file and spit the contents of that file back out with
a make target.

Create a File
=============

The file could be placed anywhere; in smaller projects I may
reuse the main README file, in larger projects I normally place
the file in a directory designated for build tool support,
conventionally either ``.make/help`` or ``buildsrc/help``.

The contents I create typically look like:

.. code-block:: rst

   ############
   Project Help
   ############

   ************
   Common Tasks
   ************

   ...
   make check
   	Run tests and other checks for the project.
   ...

   *******
   Recipes
   *******

   Selecting certain tests to run
   ==============================

   Some descriptions of scenarios with example invocations.

The above uses a slimmed down version of reStructuredText
:cite:`reStructuredText` but any formatting or lack thereof would
work as well: preferably something that remains readable from
a console. As a quirk to keep other pieces simple, I normally
leave some leading and trailing newlines in the file so it
displays with proper spacing without needing additional help.


Add a Target
============

Outputting the created help file is then simply a matter of
``cat``\ing the file:

.. code-block:: make

   make help: ; cat buildsrc/help

Most likely the directory containing the file should be captured
in a variable, you wouldn't want the ``cat`` command to actually
be output, and you'd want help to be a PHONY target, so the more
complete pieces of the Makefile would look like:

.. code-block:: make

   BUILD_SRC := buildsrc/

   .PHONY: help

   make help: ; @cat ${BUILD_SRC}help

Possible Adjustment
===================

In case you have a lot of output you may want to allow paging through it.
The simplest solution (and therefore likely the best one to use) would
be to leave this to the user to handle such as by running ``make help | less``,
but another option would be to add that directly into the Makefile.
In such a case it could make sense to use the standard ``PAGER`` to view the
contents. To support this the target above could be changed to something like:

.. code-block:: make

   help: ; @$${PAGER:-less} ${BUILD_SRC}help

This would leave the expansion to Bash (which is a bit shorter than handling it
with make) and use ``less`` if the ``PAGER`` environment variable has not been set.
``less`` is very common but is not standard POSIX so something like ``more`` may
be a better option for maximum portability.
