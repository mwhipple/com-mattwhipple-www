---
title: sway
---

- [Software](./software.html)

Sway[@sway-man] is my preferred window manager. I've always gravitated
towards the more minimal window managers (having initially gravitating
towards the blackbox family), subsequently drifted towards tiling
managers (to help reduce expected input devices) and have also adopted
wayland. sway delivers a pretty clear synthesis of those preferences.