---
title: C Programming
---

- [Home](./index.html)

# Style

- [Naming](./c_style_naming.html)

# Recipes

- [Closures](./c_closures.html)
- [Error Callbacks](./c_error_callbacks.html)
