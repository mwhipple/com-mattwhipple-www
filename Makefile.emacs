.SILENT:

EMACS_HOME := ~/.emacs.d/

OUTPUTS := Makefile.emacs init.el

all: ${OUTPUTS}
.PHONY: all

${OUTPUTS}: emacs_configuration.md
	./tangleFile ${<} ${@} > ${@}

${EMACS_HOME}init.el: init.el
	cp ${<} ${@}
	echo 'Update with (load-file "${@}")'

install: ${EMACS_HOME}init.el
.PHONY: install
