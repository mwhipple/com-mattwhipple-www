---
title: Linux Audio
---

Configuring audio for my Linux workstations is not something
I've dedciated much attention to in the recent past (as most of
what I use a computer for does not require audio), but having
acquired the first decently powerful system in a while (and adopting a
more documentation-heavy outlook) I'm digging into this a bit more.

Most of the core audio concerns seem to be handled by the kernel
nowadays, so the primary challenge for basic needs is to locate the
kernel settings relevant for the system. This can be somewhat tricky
as systems-on-chip become fairly ubiquitous which can lead to
non-obvious relationships between required settings and drivers.
I should be pulling key kernel configuration parameters into this site
at some point which will capture the relevant settings for sound on
this system. The Arch wiki[@arch-alsa] also had some guidance around
installing `sof-firmware` which seems likely to have helped.

I also installed PulseAudio since it seemed to be the most clear-cut
mechanism to make sure audio was supported in Firefox.

The Gentoo wiki[@gentoo-alsa] has some more specific information about
configuring and optimizing sound which I may revisit a bit, especially
if my interest in music gets revitalized and the new system plays a role.