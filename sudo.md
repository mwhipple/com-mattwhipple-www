---
title: sudo
---

- [Software](./software.html)

`sudo` is certainly a fairly standard means to control access to Linux systems,
and seems to be considered best practice even for personal systems with only
one expected user (or at least it's one I adopt).

While many of my configuration files will be captured in source control
(and most will be on this site), the sudo configuration is _typically_
managed by `visudo` which makes this less immediately practicable.
The invocation of `visudo` could likely be tweaked to support this
practice, but more likely the use of include directories will allow
for desired configuration to be pushed into place.

My sudo configuration typically stays fairly simple. At the moment
I've configured enough to unblock myself based on the comments
included in the standard file and the sudo page of the Gentoo
wiki[@gentoo-sudo].