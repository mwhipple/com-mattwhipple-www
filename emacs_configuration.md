---
title: Emacs Configuration
---

There is currently some additional escaping to work around the
current tangling solution; this should be removed later.

# Makefile

~~~{.make file=Makefile.emacs}
.SILENT:

EMACS_HOME := ~/.emacs.d/

OUTPUTS := Makefile.emacs init.el

all: ${OUTPUTS}
.PHONY: all

${OUTPUTS}: emacs_configuration.md
	./tangleFile ${<} ${@} > ${@}

${EMACS_HOME}init.el: init.el
	cp ${<} ${@}
	echo 'Update with (load-file "${@}")'

install: ${EMACS_HOME}init.el
.PHONY: install
~~~

# File Contents

## Helpers

~~~{.el file=init.el}
(defun define-kbd (keymap keys command)
  (define-key keymap (kbd keys) command))
~~~

## Low level essentials

~~~{.el file=init.el}
(let ((bind (-partial 'define-kbd global-map)))
  (funcall bind "C-g" 'keyboard-quit))
(apply 'define-key (list global-map (kbd "M-x") 'execute-extended-command))
(define-key global-map (kbd "C-x C-c") 'save-buffers-kill-terminal)
(define-key global-map (kbd "M--") 'negative-argument)
(define-key global-map (kbd "C-z") 'suspend-frame)
(define-key global-map (kbd "C-x z") 'repeat)
~~~

## Primitive Editing

~~~{.el file=init.el}
(define-key global-map (kbd "a") 'self-insert-command)
(define-key global-map (kbd "b") 'self-insert-command)
(define-key global-map (kbd "c") 'self-insert-command)
(define-key global-map (kbd "d") 'self-insert-command)
(define-key global-map (kbd "e") 'self-insert-command)
(define-key global-map (kbd "f") 'self-insert-command)
(define-key global-map (kbd "g") 'self-insert-command)
(define-key global-map (kbd "h") 'self-insert-command)
(define-key global-map (kbd "i") 'self-insert-command)
(define-key global-map (kbd "j") 'self-insert-command)
(define-key global-map (kbd "k") 'self-insert-command)
(define-key global-map (kbd "l") 'self-insert-command)
(define-key global-map (kbd "m") 'self-insert-command)
(define-key global-map (kbd "n") 'self-insert-command)
(define-key global-map (kbd "o") 'self-insert-command)
(define-key global-map (kbd "p") 'self-insert-command)
(define-key global-map (kbd "q") 'self-insert-command)
(define-key global-map (kbd "r") 'self-insert-command)
(define-key global-map (kbd "s") 'self-insert-command)
(define-key global-map (kbd "t") 'self-insert-command)
(define-key global-map (kbd "u") 'self-insert-command)
(define-key global-map (kbd "v") 'self-insert-command)
(define-key global-map (kbd "w") 'self-insert-command)
(define-key global-map (kbd "x") 'self-insert-command)
(define-key global-map (kbd "y") 'self-insert-command)
(define-key global-map (kbd "z") 'self-insert-command)
(define-key global-map (kbd "A") 'self-insert-command)
(define-key global-map (kbd "B") 'self-insert-command)
(define-key global-map (kbd "C") 'self-insert-command)
(define-key global-map (kbd "D") 'self-insert-command)
(define-key global-map (kbd "E") 'self-insert-command)
(define-key global-map (kbd "F") 'self-insert-command)
(define-key global-map (kbd "G") 'self-insert-command)
(define-key global-map (kbd "H") 'self-insert-command)
(define-key global-map (kbd "I") 'self-insert-command)
(define-key global-map (kbd "J") 'self-insert-command)
(define-key global-map (kbd "K") 'self-insert-command)
(define-key global-map (kbd "L") 'self-insert-command)
(define-key global-map (kbd "M") 'self-insert-command)
(define-key global-map (kbd "N") 'self-insert-command)
(define-key global-map (kbd "O") 'self-insert-command)
(define-key global-map (kbd "P") 'self-insert-command)
(define-key global-map (kbd "Q") 'self-insert-command)
(define-key global-map (kbd "R") 'self-insert-command)
(define-key global-map (kbd "S") 'self-insert-command)
(define-key global-map (kbd "T") 'self-insert-command)
(define-key global-map (kbd "U") 'self-insert-command)
(define-key global-map (kbd "V") 'self-insert-command)
(define-key global-map (kbd "W") 'self-insert-command)
(define-key global-map (kbd "X") 'self-insert-command)
(define-key global-map (kbd "Y") 'self-insert-command)
(define-key global-map (kbd "Z") 'self-insert-command)
(define-key global-map (kbd "1") 'self-insert-command)
(define-key global-map (kbd "2") 'self-insert-command)
(define-key global-map (kbd "3") 'self-insert-command)
(define-key global-map (kbd "4") 'self-insert-command)
(define-key global-map (kbd "5") 'self-insert-command)
(define-key global-map (kbd "6") 'self-insert-command)
(define-key global-map (kbd "7") 'self-insert-command)
(define-key global-map (kbd "8") 'self-insert-command)
(define-key global-map (kbd "9") 'self-insert-command)
(define-key global-map (kbd "0") 'self-insert-command)
(define-key global-map (kbd ".") 'self-insert-command)
(define-key global-map (kbd "(") 'self-insert-command)
(define-key global-map (kbd ")") 'self-insert-command)
(define-key global-map (kbd "-") 'self-insert-command)
(define-key global-map (kbd "_") 'self-insert-command)
(define-key global-map (kbd "\\\"") 'self-insert-command)
(define-key global-map (kbd "'") 'self-insert-command)
(define-key global-map (kbd "~") 'self-insert-command)
(define-key global-map (kbd "{") 'self-insert-command)
(define-key global-map (kbd "}") 'self-insert-command)
(define-key global-map (kbd ".") 'self-insert-command)
(define-key global-map (kbd ",") 'self-insert-command)
(define-key global-map (kbd "?") 'self-insert-command)
(define-key global-map (kbd "#") 'self-insert-command)
(define-key global-map (kbd "/") 'self-insert-command)
(define-key global-map (kbd "\\\\") 'self-insert-command)
(define-key global-map (kbd "+") 'self-insert-command)
(define-key global-map (kbd "=") 'self-insert-command)
(define-key global-map (kbd ":") 'self-insert-command)
(define-key global-map (kbd ";") 'self-insert-command)
(define-key global-map (kbd ">") 'self-insert-command)
(define-key global-map (kbd "<") 'self-insert-command)
(define-key global-map (kbd "@") 'self-insert-command)
(define-key global-map (kbd "!") 'self-insert-command)
(define-key global-map (kbd "[") 'self-insert-command)
(define-key global-map (kbd "]") 'self-insert-command)
(define-key global-map (kbd "$") 'self-insert-command)
(define-key global-map (kbd "^") 'self-insert-command)
(define-key global-map (kbd "`") 'self-insert-command)
(define-key global-map (kbd "*") 'self-insert-command)
(define-key global-map (kbd "|") 'self-insert-command)
(define-key global-map (kbd "&") 'self-insert-command)
(define-key global-map (kbd "%") 'self-insert-command)
(define-key global-map (kbd "<space>") 'self-insert-command)
(define-key global-map (kbd "<ret>") 'newline)
(define-key global-map (kbd "<del>") 'delete-backward-char)
(define-key global-map (kbd "C-d") 'delete-char)
(define-key global-map (kbd "C-q") 'quoted-insert)
(define-key global-map (kbd "<tab>") 'indent-for-tab-command)
~~~

## Extended Editing

~~~{.el file=init.el}
(define-key global-map (kbd "C-k") 'kill-line)
(define-key global-map (kbd "C-y") 'yank)
(define-key global-map (kbd "M-y") 'yank-pop)
(define-key global-map (kbd "C-o") 'open-line)
(define-key global-map (kbd "M-/") 'dabbrev-expand)
(define-key global-map (kbd "M-<del>") 'backward-kill-word)
(define-key global-map (kbd "M-d") 'kill-word)
(define-key global-map (kbd "M-u") 'upcase-word)
(define-key global-map (kbd "M-l") 'downcase-word)
(define-key global-map (kbd "M-c") 'capitalize-word)
(define-key global-map (kbd "C-t") 'transpose-chars)
(define-key global-map (kbd "C-j") 'electric-newline-and-maybe-indent)
(define-key global-map (kbd "C-_") 'undo)
(define-key global-map (kbd "M-q") 'fill-paragraph)
(define-key global-map (kbd "C-M-q") 'align-regexp)
(define-key global-map (kbd "M-\\\\") 'delete-horizontal-space)
(define-key global-map (kbd "M-;") 'comment-dwim)
(define-key global-map (kbd "M-%") 'query-replace)
~~~

## Basic Motion

~~~{.el file=init.el}
(define-key global-map (kbd "C-n") 'next-line)
(define-key global-map (kbd "C-p") 'previous-line)
(define-key global-map (kbd "C-f") 'forward-char)
(define-key global-map (kbd "C-b") 'backward-char)
~~~

## Extended Motion

~~~{.el file=init.el}
(define-key global-map (kbd "C-e") 'move-end-of-line)
(define-key global-map (kbd "C-a") 'move-beginning-of-line)
(define-key global-map (kbd "C-v") 'scroll-up-command)
(define-key global-map (kbd "M-v") 'scroll-down-command)
(define-key global-map (kbd "C-l") 'recenter-top-bottom)
(define-key global-map (kbd "M->") 'end-of-buffer)
(define-key global-map (kbd "M-<") 'beginning-of-buffer)
(define-key global-map (kbd "M-f") 'forward-word)
(define-key global-map (kbd "M-b") 'backward-word)
(define-key global-map (kbd "C-s") 'isearch-forward)
(define-key global-map (kbd "C-r") 'isearch-backward)
(define-key global-map (kbd "C-M-n") 'forward-list)
(define-key global-map (kbd "C-M-p") 'backward-list)
(define-key global-map (kbd "C-M-f") 'forward-sexp)
(define-key global-map (kbd "C-M-b") 'backward-sexp)
(define-key global-map (kbd "M-}") 'forward-paragraph)
(define-key global-map (kbd "M-{") 'backward-paragraph)
(define-key global-map (kbd "M-g g") 'goto-line)
~~~

## Mark and Region

~~~{.el file=init.el}
(define-key global-map (kbd "C-@") 'set-mark-command)
(define-key global-map (kbd "C-w") 'kill-region)
(define-key global-map (kbd "M-w") 'kill-ring-save)
(define-key global-map (kbd "C-x h") 'mark-whole-buffer)
(define-key global-map (kbd "C-M-@") 'mark-sexp)
~~~

## Inter-File Navigation

~~~{.el file=init.el}
(define-key global-map (kbd "M-.") 'xref-find-definitions)
(define-key global-map (kbd "C-x `") 'next-error)
~~~

## Help

~~~{.el file=init.el}
(define-key global-map (kbd "C-h b") 'describe-bindings)
(define-key global-map (kbd "C-h c") 'describe-key-briefly)
(define-key global-map (kbd "C-h f") 'describe-function)
(define-key global-map (kbd "C-h i") 'info)
(define-key global-map (kbd "C-h k") 'describe-key)
(define-key global-map (kbd "C-h l") 'view-lossage)
(define-key global-map (kbd "C-h m") 'describe-mode)
(define-key global-map (kbd "C-h P") 'describe-package)
(define-key global-map (kbd "C-h v") 'describe-variable)
(define-key global-map (kbd "C-h w") 'where-is)
~~~

## Buffer Management

~~~{.el file=init.el}
(define-key global-map (kbd "C-x C-f") 'ffap)

(let ((buffer-map (make-keymap)))
  (define-key global-map (kbd "C-x C-b") buffer-map)

  (define-key buffer-map (kbd "k") 'kill-buffer)
  (define-key buffer-map (kbd "l") 'buffer-list)
  (define-key buffer-map (kbd "m") 'rename-buffer)
  (define-key buffer-map (kbd "o") 'switch-to-buffer)
  (define-key buffer-map (kbd "_") 'revert-buffer)
  (define-key buffer-map (kbd "n") 'next-buffer)
  (define-key buffer-map (kbd "p") 'previous-buffer)
  (define-key buffer-map (kbd "s") 'save-buffer)
  (define-key buffer-map (kbd "w") 'write-file))
~~~

## Window Management

~~~{.el file=init.el}
(define-key global-map (kbd "C-x o") 'other-window)
(define-key global-map (kbd "C-x 0") 'delete-window)
(define-key global-map (kbd "C-x 1") 'delete-other-windows)
(define-key global-map (kbd "C-x 2") 'split-window-below)
(define-key global-map (kbd "C-x 3") 'split-window-right)
(define-key global-map (kbd "C-x ^") 'enlarge-window)
(define-key global-map (kbd "C-x }") 'enlarge-window-horizontally)
(define-key global-map (kbd "C-x {") 'shrink-window-horizontally)
(define-key global-map (kbd "C-x +") 'balance-windows)
~~~

### IPC

~~~{.el file=init.el}
(define-key global-map (kbd "M-!") 'shell-command)
(define-key global-map (kbd "M-&") 'async-shell-command)
(define-key global-map (kbd "M-|") 'shell-command-on-region)
~~~

### REPL

~~~{.el file=init.el}
(define-key global-map (kbd "C-x C-e") 'eval-last-sexp)
(define-key global-map (kbd "C-x M-e") 'eval-region)
(define-key global-map (kbd "M-:") 'eval-expression)
~~~

### Global Mode Invocation

~~~{.el file=init.el}
(define-key global-map (kbd "C-x C-m c") 'compile)
(define-key global-map (kbd "C-x C-m C") 'recompile)
(define-key global-map (kbd "C-x C-m s") 'shell)
(define-key global-map (kbd "C-x C-m v") 'view-mode)
(define-key global-map (kbd "C-x C-m t") 'auto-revert-tail-mode)
(define-key global-map (kbd "C-x C-m w") 'eww)
(setq auto-revert-verbose nil)
~~~

### Display Tweaking

~~~{.el file=init.el}
(tool-bar-mode 0)
(display-battery-mode)
(display-time-mode)
(global-font-lock-mode t)
(line-number-mode)
(define-key global-map (kbd "C-x C-+") 'text-scale-adjust)
(define-key global-map (kbd "C-x C--") 'text-scale-adjust)
(define-key global-map (kbd "C-x C-=") 'text-scale-adjust)
~~~

### Minibuffer

~~~{.el file=init.el}
(define-key minibuffer-local-map (kbd "<tab>") 'minibuffer-complete)
(define-key minibuffer-local-map (kbd "<ret>") 'minibuffer-complete-and-exit)
~~~

### View Mode

~~~{.el file=init.el}
(define-key view-mode-map (kbd "<space>") 'view-scroll-page-forward)
~~~

### Info Mode

~~~{.el file=init.el}
(define-key Info-mode-map (kbd "<space>") 'Info-scroll-up)
(define-key Info-mode-map (kbd "<del>") 'Info-scroll-down)
~~~

### Org Mode

~~~{.el file=init.el}
(define-key org-mode-map (kbd "M-<ret>") 'org-meta-return)
(define-key org-mode-map (kbd "M-<right>") 'org-metaright)
(define-key org-mode-map (kbd "M-<left>") 'org-metaleft)
(define-key org-mode-map (kbd "<tab>") 'org-cycle)
(define-key org-mode-map (kbd "C-c C-c") 'org-ctrl-c-ctrl-c)
(define-key org-mode-map (kbd "C-c C-s") 'org-schedule)
(define-key org-mode-map (kbd "C-c C-t") 'org-todo)
(define-key org-mode-map (kbd "C-c C-w") 'org-refile)
(define-key org-mode-map (kbd "C-a") 'org-beginning-of-line)

(let ((org-global-map (make-keymap)))
  (define-key global-map (kbd "C-x C-o") org-global-map)  

  (define-key org-global-map (kbd "a") 'org-agenda)
  (define-key org-global-map (kbd "c") 'org-capture)
  (define-key org-global-map (kbd "l") 'org-store-link)
  (define-key org-global-map (kbd "o") 'org-open-at-point))

(define-key org-agenda-mode-map (kbd "C-c t") 'org-agenda-todo)
(define-key org-agenda-mode-map (kbd "C-c TAB") 'org-agenda-goto)
(define-key org-agenda-mode-map (kbd "C-c <ret>") 'org-agenda-switch-to)
(define-key org-agenda-mode-map (kbd "C-c g") 'org-agenda-redo-all)
(define-key org-agenda-mode-map (kbd "C-c d") 'org-agenda-day-view)
(define-key org-agenda-mode-map (kbd "C-c ,") 'org-agenda-priority)

(setq org-cycle-global-at-bob t)
(setq org-agenda-span 'day)
(setq org-log-repeat nil)
~~~

### Shell Mode

~~~{.el file=init.el}
(define-key shell-mode-map (kbd "<tab>") 'completion-at-point)
~~~

### Dired

~~~{.el file=init.el}
(define-key dired-mode-map (kbd "C-c <ret>") 'dired-find-file)
(define-key dired-mode-map (kbd "C-c d") 'dired-flag-file-deletion)
(define-key dired-mode-map (kbd "C-c x") 'dired-do-flagged-delete)
~~~