---
title: Music Theory
---

- [Books](./books.html)

One of the books kicking around my house that I decided to
read was "Music Theory"[@jones1974music] that my wife had
from college. I have some musical background and used to
perform fairly regularly but have no formal training in music
so figured this may be a good introduction.

The book itself is very much a college book: it is somewhat
of a reference manual along with exercises and advice that
the information therein should be accompanied by sufficient
drilling to make the important information practicable.
As I did not do the exercises very little of the information
actually stuck, particularly as the book moved on.

Some of the higher level (especially those earlier) concepts
were of interest and the read through provided my a sparse
framework that I can work on populating over time and can
use this book to consult. I have plans to dig more into
music again including capturing a fair amount of music
using LilyPond and so this book will likely help frame going
deeper into analysis of the inscribed pieces.

I did enjoy that the content reflected the notion expressed
in the preface that "theory follows practice" since that
seems to often be inverted in some minds "in music,
as in many other fields"; the abstract overtakes the
concrete and too much attention can be diverted away
from reality.
