---
title: Overengineering
---

- [Assorted Thoughts](./assorted_thoughts.html)

---

This may be an isolated personal opinion, but I've come to think that
"overengineering" is a terribly inappropriate and pernicious term.
Overengineering typically refers to creating a system that seems in
some way over complicated with a range of what form that complication
may be. The resulting solution is overly/prematurely optimized for one
of the many forces that inform how to design the solution such as
performance or generalization.

The underlying issue with that stance is that cherry picks which
forces qualify but good engineering should strive to accommodate
all such forces. Those that are omitted are often the most
visible but least sexy such as cost and maintainability. Perhaps the
strongest signal of the underlying issue is that over optimizing for a
force such as cost (which has some of the clearest business
implications) may be complementarily described as "underengineering"
which creates a terribly unfortunate message that engineering is
something which is independent of immediate reality rather than
the pursuit of delivering business value. If engineering is considered
a virtuous process then there is the message that there has been too
much or too little of a good thing even though the practice exists
to solve problems but is being treated independently of the problems
at hand.

A far healthier perspective is one in which engineering is considered
the development of solutions which appropriately weigh the full
context against which they will be applied and in that light
neglecting one for the sake of the other is not "overengineering" or
"underengineering" but simply _poor_ engineering. This deflates the
potentially self-indulgent notion that going too far in any direction
is "engineering" too much and also presents a built-in framework to
guide the calibration of solutions to problems; essentially the
prospect of being able to apply solutions in a vaccuum is kept out of
the realm of engineering and instead replaced with properly accounting
for effectively delivering value.