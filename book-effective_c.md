---
title: Effective C
---

- [Books](./books.html)

---

Effective C[@effective-c] is a solid introductory text to C
programming which covers many pitfalls and information for utilizing
newer facilities and for satisfying current requirements.

Perhaps the drawback of the book in hindsight is that it potentially
falls between the cracks of being a primer and elucidating some of the
darker corners; it doesn't seem to provide some of the reinforcement
of other introductory texts such as K&R while also staying fairly high
level and non-exhaustive in terms of details. As this is one of many
resources published by the author some of the concerns may be due to
this book serving a particular role within the aggregate works. This
perspective may also be unfairly skewed by my prior knowledge. Another
looming question given the mixed survey style of this book is whether
it offers notable combined value over sources which are available on
the Internet; as I'm reading this book on the Internet and through an
online service I not only don't know the answer but am not
particularly affected by it but it always seems worth asking given the
higher overhead of books.

I'll certainly periodically refer to this book as a lightweight
reference manual for C when seeking appropriate approaches to somewhat
basic challenges.