---
title: Apprenticeship Patterns
---

- [Books](./books.html)

---

A book that I read upon recommendation from a former teammate is
Apprenticeship Patterns[@apprenticeship-patterns].
My take on the book overall is a positive one and I'd suggest it a
range of developers that are either starting out, feeling adrift, or
potentially stuck in an unfortunate culture. The latter aspect is
significant in that many engineering cultures seem to not invest
appropriately in developing of people and projects and this book can
help at least provide a backdrop for thinking about such issues.

I've certainly tended to adopt many of the patterns covered in the
book, and this site itself is an example of one of the threads
covered.

One particular theme in parts of the book that didn't sit
overly well with me is around the nature and dynamics of software
craftsmanship. While I certainly do not want to undermine the pursuit
of improved software development I think a far healthier perspective
remains grounded in the pragmatic value delivered via the execution of
such software. This is likely a difference of perspective rather than
of practice where I'd place well-crafted software is the service of
efficiently delivering such value over time rather than a means to an
end in and of itself, but it seems worth remaining vigilant against
the latter as it feels fairly common but can lead to distance between
engineering efforts and substantiated business objectives. The section
about sweeping the floor certainly seemed to encourage carrying some
of these ideas into territory that could quickly become unhealthy.

I think an undertone which I think could have been elevated far more
(and has been given attention elsewhere) is the notion that
engineering is an endeavour of constant learning and that working
software is a distillation of such learning. Likely one of the most
compelling concepts in the book is defining mastery as being
contingent on the ability to nurture others. This position aligns very
neatly with the orientation toward learning while extending it to a
more generalized perspective of knowledge mobility taking precedence
over crafting products. There may be a more expressive mapping from
apprentice, journeymen, master to terms such as learner, mentor, coach
respectively where learners consume knowledge, mentors distribute
knowledge, and coaches foster the skills in others to allow them to
more efficiently acquire and share relevant information.

The book may have suffered a bit from trying to fit too closely into
the craftsman paradigm which has a tenuous mapping to both
organizational realities and the blackbox nature of software
deliverables. Similarly the presentation of the ideas as a pattern
catalog seemed as likely to be driven by a desire to ride the wave of
pattern popularity from the time and subculture rather than being
inherently suitable. While defining terms and concepts seems sound the
catalog presentation often seemed contrived and ill-fitting for many
of the tightly-knit "patterns".

Overall the book is worth a read and full of some good advice but
should be accompanied by some grains of salt.