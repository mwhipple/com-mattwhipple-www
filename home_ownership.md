---
title: Home Ownership
---

- [Home](./index.html)

Home ownership brings a fairly constant stream of voluntary
and unfortunate projects.

- [Washer/Dryer Replacement 2021](./washer_dryer_replacement_2021.html)

# DIYness

I'd had plans to become increasingly
more of a DIYer; I was raised with some of that background and
have spent sporadic time doing handiwork for myself and others
but not regularly enough to be immediately equipped to dive
into a project. Growing up there was also work that _was_
regularly taken on but it always seemed eclipsed by that work
which was talked about but left undone. I've therefore tried
to identify that work which I am able and likely to do
well and factor that in to whether to do something, pay to have
it done, or defer it. With young children the first category
was very narrow and so most projects have been outsourced.
As my children are growing and the pandemic is not abating
(and therefore there is a desire to minimize the time any
contractors spend in my house) I
may be starting to ramp up some projects.
