---
title: Immaterial Implications
---

- [Assorted Thoughts](./assorted_thoughts.html)

---

Over the natural course of short-circuiting conclusions, there is a
tendency to fall into traps of false equivalences and immaterial
implications. Recognizing such fallacies can prevent blindly following
paths that may not be leading to intended destinations.

- [Abstraction is Not Decoupling](./abstraction_is_not_decoupling.html)
- [Ambition is Not Vision](./ambition_is_not_vision.html)
- [DRY is Not Maintainable](./dry_is_not_maintainable.html)
- [Easiest is Not Simplest](./easiest_is_not_simplest.html)
- [Potential is Not Value](./potential_is_not_value.html)