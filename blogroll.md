---
title: Blogroll - Matt Whipple
---

- [Home](./index.html)

Here's a list of some blogs and similar resources with which I stay up to date.
I'd imagine the concept of a blogroll has largely been subsumed by more social
aspects within the zeitgeist of the modern Web (I had to look up the term to make
sure I wasn't imagining it [@blogroll-wiktionary]), but it fits within the
intent of this site.

## antirez[@antirez]

I stumbled into awareness of Salvatore Sanfilippo/antirez through the
kilo editor and the associated tutorial[@snaptoken-kilo] and after
realizing that he's also the creator of Redis decided to read through
his blog which I found full of interesting ideas. In particular his
openness and the breadth of some of his material builds a good narrative
of how a highly effective coder digs into problems and some of the
practical values that are prioritized.

## Linux Kernel Monkey Log[@linux-kernel-monkey-log]

This is Greg Kroah-Hartman's blog. I don't recall what initially led
me to this blog (likely picking through some kernel issues or reading
_Linux Kernel in a Nuthsell_ at some point). The blog itself is
updated fairly infrequently. There's some info on the blog I'll likely
pick from and it seems generally helpful as a Linux user to have some
insight into the thoughts of one of the key maintainers.
