#####
Style
#####

I'll be incrementally tinkering with the design of this site.
UX and design are areas I've generally steered clear of, though
maybe this undertaking will change that. I am aware of enough
concerns in those areas to have informed though likely largely
unfounded opinions, so those will somewhat drive this and potentially
be supported or defeated by future work and research.

***************
Design Concepts
***************

A specific aspect of computers that interests me (and which I should
devote more time to consuming relevant established information)
is the ways in which we interact with the data that computers can provide.
At this point in our bridging that gap most of that interaction is
driven through digital doppelgangers of things experienced outside of
and prior to computers. Such an approach provides an accessible entrance
to the technology but also yields a lacking simulacrum of the original
experience while also (more importantly) not fully harnessing the digital
medium. This is a larger topic which I'll likely delve
into further later replace these brief comments with a link to an
appropriate page as it is created.

These thoughts influence the design of this site in terms of pursuing a
minimal amount of distance between a viewer and the contents of the
site. Structual visual elements on Web sites very often amount to some
combination of noise or non-portable assumptions. They generally don't
translate smoothly to other channels of delivery which may have limited or
lacking graphical support; such channels are likely to become
increasingly commonplace as technology pervades our lives in ways
which involve more continuous but less focused engagement. They
are a good example of something that is familar, may be pleasant,
and can also be used to nudge users into particular directions
(such as for reasons of advertising), but something which should be
used very judiciously to attempt to create an efficient means of
interaction which is attuned to digital content.

The above vision is fairly ambitious and, as previously mentioned,
I'm not equipped to tackle it, so the most practical consequence
is minimalism and consideration. I try to consider multi-channel
delivery in the design: thinking how easily the content could also
be consumed through something like an audio-only interface. Most
importantly I strive to avoid the assumptions that often accompany
the visual design of a Web site since the investment in any such
practice is less likely to be portable and result in either a more
significant effort required to pivot or a more degraded experience
for ny user to whom those assumptions do not hold.

************
Custom Theme
************

The site styling is provided by a custom `sphinx` theme
which is defined in the ``mw_theme`` directory. A custom theme
is used to support ample flexibility in light of the aforementioned
conceptual challenges: where extending an existing (non-basic)
theme seems likely to result in a fair amount of unused code
and may require additionl work to "undo" features rather than
just avoiding them. The preference for a theme over local
overrides to basic is driven by the prospect of using the
established interace between themes and their usage which can
curb the tangling of the two.

**********
Typography
**********

Typography is an art that intrigues me but is not a
field where I'm likely to have any insight for the foreseeable
future. I do, however,  have a modicum of taste that drives me to want
a font that looks better than the fonts generally provided 
by default in most browsers and operating systems.
As such I'm effectively deferring to more refined tastes, and
I've adopted Computer Modern [FontComputerModernWikipedia]_
as the font for this site. That seems an appropriate gateway font
as Donald Knuth is someone whose other works I will be exploring
and someone who has a fairly rich background in typography (not
primarily a typographer but he created TeX and has collaborated with
Hermann Zapf).

***********
Logo Design
***********

My logo is something which I designed several years ago for
independent work and which I still occasionally use to
provide a hint of personalization.

The logo itself is my name typed in Courier with the
"W" replaced by an upside-down "M", and the first and
last name arranged so that the "M" and "W" interlock.
The staggered, interlocking "M" and "W" serve as a
smaller logo variation such as used by the favicon on
this site.

The logo was created using Sodipodi (which has since
evolved into `inkscape <https://inkscape.org>`_). The
letters were positioned and the outline of the resulting
shape was retrieved to ensure that the resulting graphic
was entirely vector-based. The resulting outline was
filled which incidentally produced the effect where there
is a thin, variable width space between the inner fill and
the outline. The accidental variability of the space seems
to me to provide a more natural, less sterile feel to the
logo and is likely the main reason I continue to use it.

.. rubric:: References
.. bibliography:: /core.bib
   :filter: topic %"SiteStyle"
