---
title: The God Delusion
---

- [Books](./books.html)

After reading Richard Dawkin's "The God Delusion" [@dawkins2006god]
I've officially crossed the threshold to atheism. The book seems
to present a lot of arguments which would be difficult to resonably
counter, but my personal beliefs did not need so much alteration as
they did clarity. Among the other variations along the spectrum of
agnosticism outlined early in the book, I've held on to one that was
largely borne out an ignorance of athiestic thought. I was not raised
to be religious and live in a region which (contrary to its puritanical
history) is predominantly liberal and secular so the adoption of atheism
presents no particular challenges for me. However I've also been
aware that people such as Dawkins have a mature, cultivated atheism
whereas I've primarily been shrouded in benighted indifference. For me
this book served to illumintate the supports for scientific and humansitic
perspectives such that there needs not be room for my lingering and
typically neglected Einsteinian god.

Beyond religion itself the more general thread of dogma and unconditional
belief in opposition to reason and critical thought was something that
significantly resonated with me. This conflict is something that I was
seemingly late to recognize fully but have been subjected to a series of
rude awakenings over the past several years (likely along with many other
Americans who have been privileged enough to be spared extensive
previous exposure). This book helps contextualize some of the recent
need for confrontation and reconciliation within the history of humanity
through the lens of one of what are perhaps the clearest poles of similar 
tensions.

# As a Parent

As a parent likely the most important topic for me while reading this
book is how religion applies to (or more specifically is forcibly applied
to) children. In this particular regard I feel as though my wife and I
are doing right by our children. My children are exposed to religious
(primarily Christian) material and often express (unsolicited and neither confirmed
nor denied) religious beliefs that seem appropriate for what they have been
exposed to and what is apparently the dualism appropriate for their ages.
Similar to how Dawkins describes his own upbrining I certainly feel as though we try to tech
"how to think" rather than "what to think". We strive to instill the values
of curiosity, critical thought, and skepticism. A major theme I try to
push which is contrary to my own upbringing (and is likely to be a major
recurring theme of mine) is a retained bias towards questions rather than
answers, all of which should hopefully empower them to find their own
belief system without a prerequisite liberation from indoctrination.

# As an Engineer

There are seemingly pretty clear parallels between Darwinian evolution and
agile/incremental software development which has likely been spelled out
elsewhere. The notion that it is far more feasible to build a
sufficiently complex system (where complexity gets a helpful definition
based on heterogeneity) is through a crane of iterative feedback based refinement
(natural selection applied to reducible complexity)
rather than presuming enough intelligence and forethought
to initially produce a viable top down skyhook seems to fit very naturally
with established thinking around software development (and relevant human limitations).
This is certainly on a _far_ more modest level than systems such as the entirety
of creation, but it seems as though relative scale may be preserved. This
likely is supported by arguments such as Architcture Without Architects
which is a talk I'll need to rewatch and link to at some point.

The notion of adopting simplified physical, design, and intentional stances
is also likely to help frame not only the desire for a potentially naive
desire for big design up front
but is also likely to offer a helpful framework for organizing knowledge in
general, particularly in the light of abstractions which typically
form the foundations of engineering as a discpline. Acknolwdgement of such
shortcuts and the potential utility and tradeoffs thereof can help drive
discovery and knowledge sharing. The stories surrounding cargo cults was also
welcome to elucidate some of the associated idioms and practices as I've
somewhat avoided such terms due to uncertainty regarding particulars.
Cargo cultuing could be treated as an extension of some of the stance
derived cognitive simplifications.
