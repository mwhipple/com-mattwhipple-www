:orphan:

###########
My Web Site
###########

This site is hosted on GCP, the console for which is available at:
https://console.cloud.google.com. gustil is used for uploading which
should be installed and credentials can be acquired through running ``gsutil config``.

************
Common Tasks
************

help
	Show this help.

build:
	Generate a local copy of the site.

upload:
	Upload/publish a generated copy of this site.

watch:
	Automatically update a locally generated  copy of the site
        whenever a source file changes.

clean:
	Remove all generated files.

*********************
Environment Variables
*********************

INCLUDE_TODOS
	If 'True` (exactly) then render TODO items in the generated site.

