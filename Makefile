##
# ---
# title: Makefile
# ---
#
# * [Software](./software)
#
# This file provides the build configuration for this site itself.
#
#
# ## Weave
#
# Much of the content in this site needs to be pulled out of source
# code (such as this file) and will make use of [weave](./weave.html)
# which needs to be built.
#
# Giving local commands a relative path is generally a good idea, but
# in this case it was also driven by the fact that many systems may have
# a system installed `weave` command which is the namesake for this one.
#
# This can be produced by a fairly typical compilation recipe.
#
# This should be rewritten using standard make rules and also to use ASLR
##

##
# Target the hot new C standard.
##
CPPFLAGS += -std=c2x -O2

WEAVE := ./weave

${WEAVE}: weave.c

#${WEAVE}: weave.cpp
#	${CXX} -std=c++2a -Wextra -Wall -pedantic -D_FORTIFY_SOURCE=2 -O2 -o ${@} ${<}


##
#
# ## Woven Sources
#
# Those files that should be passed through weave are defined here.
# The output markdown file name can be derived by removing any
# file extension present (retrieving the `basename`) and then
# appending `.md`. To start this will be done manually.
#
# The different types of source files will be grouped into different
# variables to facilitate passing different arguments to weave.
# The resulting outputs will be combined with the rest of the site
# sources.
#
# An apparent challenge with this make configuration is tracking
# variations and granular prerequisities, specifically the notion
# that all of the markdown files are to be treated the same but
# follow different paths to generation. To address this I'm
# making use of secondary expansion combined with the input type
# specific variables so that the input files can be derived for the
# members of each of those groups. As types are added this is
# likely to acquire some additional indirections.
#
# There was something in the recently read make
# book[@managing-projects-with-gnu-make] that promised a simpler alternative to this
# that should be dug out (but I may be imagining that).
##

WEAVE_C_SRC     := weave.c words.c
WEAVE_C_OUTPUTS := ${WEAVE_C_SRC:.c=.md}
MD_SRC          += ${WEAVE_C_OUTPUTS}
RAW_SRC         += ${WEAVE_C_SRC}

WEAVE_C_OPTIONS    := '-i/**' '-i**/' -o{.c}
WEAVE_MAKE_OPTIONS := '-i\#\#' -o{.Makefile} '-c\# ' '-c\#'
WEAVE_BASH_OPTIONS := '-i\#\#' -o{.bash} '-c\# ' '-c\#'

.SECONDEXPANSION:
${WEAVE_C_OUTPUTS}: $$(patsubst %.md,%.c,$${@}) ${WEAVE}
	< ${<} ${WEAVE} ${WEAVE_C_OPTIONS} > ${@}

WEAVE_MAKE_SRC     := Makefile
WEAVE_MAKE_OUTPUTS := $(addsuffix .md,${WEAVE_MAKE_SRC})
MD_SRC             += ${WEAVE_MAKE_OUTPUTS}
RAW_SRC            += ${WEAVE_MAKE_SRC}

.SECONDEXPANSION:
${WEAVE_MAKE_OUTPUTS}: $$(patsubst %.md,%,$${@}) ${WEAVE}
	< ${<} ${WEAVE} ${WEAVE_MAKE_OPTIONS} > ${@}

WEAVE_BASH_SRC     := isbn_to_bibtex
WEAVE_BASH_OUTPUTS := $(addsuffix .md,${WEAVE_BASH_SRC})
MD_SRC             += ${WEAVE_BASH_OUTPUTS}
RAW_SRC            += ${WEAVE_BASH_SRC}

.SECONDEXPANSION:
${WEAVE_BASH_OUTPUTS}: $$(patsubst %.md,%,$${@}) ${WEAVE}
	< ${<} ${WEAVE} ${WEAVE_BASH_OPTIONS} > ${@}

##
#
# ## Standard Sources
#
# Much of the markdown is just authored directly and so doesn't
# need any special support and will just make use of the standard
# make wildcard function.
#
##
MD_SRC += $(wildcard *.md)

##
# ## Outputs
#
# The rendered outputs will be placed into the target directory,
# which is defined and created as needed.
#
##

OUT_DIR  := public/
${OUT_DIR}: ; mkdir -p ${@}

##
#
# ### HTML
#
# HTML files will be produced from the markdown, using pandoc.
#
# This will make use of a pattern target to facilitate grabbing
# of the stem and association with the appropriate prerequisite.
#
##

PANDOC       := pandoc
HTML_OUTPUTS := $(addprefix ${OUT_DIR},${MD_SRC:%.md=%.html})

${OUT_DIR}%.html: %.md | ${OUT_DIR}
	${PANDOC} --defaults pandoc.defaults -s -o ${@} ${<}

##
#
# ### Raw
#
# Raw outputs will just be copied from their source.
#
# Currently this will make use of a pattern target similar
# to the HTML logic and rely on precedence for shorter stems
# which I _think_ is provided by newer versions of make.
#
##

RAW_OUTPUTS = $(addprefix ${OUT_DIR},${RAW_SRC}) 
${OUT_DIR}%: % | ${OUT_DIR}
	cp ${<} ${@}

site: ${HTML_OUTPUTS} ${RAW_OUTPUTS}
.PHONY: site
