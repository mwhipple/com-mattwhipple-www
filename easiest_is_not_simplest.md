---
title: Easiest is Not Simplest
---

- [Immaterial Implications](./immaterial_implications.html)

One of the most often perpetuated abuses of language at least by
software engineers is equating _simplicity_ with _ease_. It perversely
is often invoked in response to indications that complexity is
lurking: identification of inconsistencies and problems down the line
are met with a rebuttal along the lines of "let's just keep it simple
for now", when what is actually meant is "let's do what is easiest".

Certainly there is nothing _wrong_ with ease and it is a vital force
to rein in issues like scope creep and excessive up front design, but
it is crucial to not conflate ease with the far more valueable
simplicity. Some of the most difficult to understand systems I've
worked with seemed to have evolved pretty clearly from a series of
steps driven by immediate ease and were chock-full of needless
accidental complexity.

Another danger lurking in this area is not distinguishing _simple_
from _simplistic_. A simple answer does not imply a simple solution.
Such a mismatch can be signaled by pursuit of panaceas which may
provide answers to some subset of concerns while neglecting or
complicating others. Tradeoffs are inevitable and do not imply
inferior solutions but they certainly challenge any imputed
simplicity.

Perhaps a useful spin is to make sure that the KISS guidance is parsed
with proper meaning. _Keep_ing a solution simple is certainly a
worthwhile endeavor and which naturally requires tracking any
accumulated complexity, which is a very different stance than using
the mantra as a shield against deeper thought.

In the words of Steve Jobs:

> Simple can be harder than complex: you have to work hard to get your
  thinking clean to make it simple. But it's worth it in the end
  because once you get there, you can move mountains. [@bbc-steve-jobs-quotes]

