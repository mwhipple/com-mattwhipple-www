---
title: Potential is Not Value
---

- [Immaterial Implications](./immaterial_implications.html)

While most of the immaterial implications outlined seem likely to be
logical errors, the overloading of _potential_ and _value_ is more
of a fallacious instrument.

Very often in discussions over proposed solutions there are arguments
made that approach Foo will enable additional value Bar. Sometimes
such arguments go further to suggest that Bar has long been desired
and that Foo will finally scratch that itch. While alternative
solutions offer different benefits, it is important to track that any
proposed potential benefits are in no way equal to delivered value and
should be weighed accordingly rather than being lumped together with
primary value.

The dangers of not distinguishing potential from value is that it can
drown out the ability to actuallly track the costs and returns from
the approaches taken. Often such discussions may involve upfront
costs, and incorporeal hopes for the future are unlikely to be a
prudent path to amortizing those costs. Generally such future plans
may not pan out for any one of a number of reasons, or the envisioned
state may not match how things are ultimately realized which may lead
to a poor fit or additional reworking.

Aspiration is a key ingredient for progress, and reconciling this
position with working towards a better future state _may_ seem
difficult but there are some very simple practices to make sure that
progress is not only _made_ but is not illusory. The first is to
simply to make sure there is a system to track return on investment.
Such a system in itself is a cost and so its weight should be
proportional to the costs and risks for the decision being discussed;
though _something_ should likely be in place to collect data over time
as possible issues accrete and the environment evolves.

A more direct and complementary action is simply to _lead_ with
the promised value. Going back to the terms in the initial paragraph:
start delivering Bar. Build out a working example that is ready for
wider use; if that can't be done _now_ then why would it be expected
to be able to be done _later_? While assessing solutions there is a
stark contrast between one option that _may_ enable some added value
down the road and one that is forfeiting value which is immediately
available; if the value is reified the decision is pushed from the
first fuzzy force into the second clear one. Through a wider lens any
larger efforts become supported by the prospect of magnifying proven
value rather than tilting windmills.