---
title: Source - Criterion
---

- [Source Reading](./souce-reading.html)

Several years ago when i decided to brush up on my C
I spent a bit of time looking around for a testing library.
I landed on Criterion[@Criterion-github] as it offered a testing
experience more similar to other languages where the library
itself effectively collects the tests. It seemed like a good
entrypoint for reading code that is likely to reflect modern
C practices and is relatively small.

The code itself stayed pretty readable with a lot of adapters
and some elaborate macros to provide a friendlier interface
to those. The key functions that would be used are well documented
and there is some internal implementations that seem worth
mining. One expectedly rich vein is some of the coordination to
support test running with the use of protobuf being a less
expected bonus.
