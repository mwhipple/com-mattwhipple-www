---
title: Book - The Highly Sensitive Child
---

- [Books](./books.html)

Upon recommendation from a friend my wife picked up a copy of
_The Highly Sensitive Child_[@aron2002highly] for guidance on
dealing with some of the perpetual challenges of parenthood.
As I've certainly lagged behind her in consuming parenting
resources this presented an opportunity to start to close that
gap.

There are some disclaimers around my thoughts; this is a topic
well outside of my wheelhouse and this book is currently just
under twenty years old. Those two are somewhat related in that
my perspective is based on limited exposure to recent materials
so I could certainly be missing insights within the book not only
against the backdrop of current thought but also over time.

The analysis of and attention to _temperment_ which seems to
be the foundation for the book's position seems like an
interesting vein of ideas; the relevant interactions between evolution
and cultural values alone seem worth reading a book about.
While there were personally plenty of very
relatable signs and consequences of being a "highly sensitive person"(HSP)
the specifics of that categorization seemed fuzzy. The diverse
attributes attributed to the trait along with vague definitions
of distribution and subjective assessments does not seem to
rigorously distinguish an HSP from other temperment variations that
presumably exist. Perhaps the argument has been simplified
(overly so for my taste) for the sake of this book or for a
wider audience, but it presents as though there is a binary
distinction between some kind of more common temperment around
which American society has ostensibly be oriented around and a
common minority of alternatives which are posited to all be
manifestations of the same underlying trait (high sensitivity).
Further, while the behavioral differences seem to be assigned to
variations in patterns of cognition, the consistent causal
relationship to sensory input seems unclear.
As the focus of this book is not to define HSPs it would be
reasonable that more comprehensive treatment would be within
earlier books and materials which set the stage for this book,
but the presentation of the material leaves plenty of room for
questions.

In terms of guidance for parenting the book also seems unconvincing.
There is very little mention of external sources of information such
as developmental psychology. While I also do not have a background
in that field, my wife does (as an EdD) and so I am periodically
informed and reminded of how stages of cognitive development apply
to our children. It seems as though this book attempts to usurp
the source for some associated behaviors. It seems regardless of
whether this is accurate the lack of contextualization and demarcation
leads back to a suspect one-note argument. The crux of the parenting advice
itself seems to largely be reducible to one of adopting empathy which
does not seem notably different than typical modern parenting advice.
The discussion of highly sensitive children seems to be a source of
anecdotes but the practical impacts on general advice seems tenuous.

One notable area for concern is that while HSPs are defined to be a
common minority of up to 20% of people much of the actual advice on
the topic seems to advocate for highly individualized treatment.
To me this seems to speak to the overly-narrow focus that HSPs introduce.
While general strategies for empathy and awareness of tempermental
differences seems globally beneficial the apparent argument that there
is a massive number of special cases seems contradictory and impractical.
Much of the messaging dripped of privilege and seems to promote approaches
that are unlikely to be able to be realized by many or most people and
the commensurate proportion that represent HSPs. Rather than offering
universal perspectives and systemic approaches this book seems to be driven
by cloistered personal experiences and permitted and proffered self-indulgence.

Ultimately this book largely fell flat. It had a good amount of
parenting advice and many useful reminders, but such material seemed
more readily and completely conusmable without being enshrouded by
the premise of high sensitvity. The notion of high sensitivity by itself
within the context of this book felt like a chery-picked, simplistic
concept which is likely devoid of defensible substance due to it seemingly
being presented as a black-and-white island. As initially mentioned I may
be missing key historical perspective but the seeming tunnel vision along
with a dearth of unique insight suggests that this book primarily reflects
a repackaging of largely general guidance under the brand of HSP to sell
books and build that brand without carrying an independently worthwhile
message.

