<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Node.js Traps</title>
  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      word-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    div.csl-bib-body { }
    div.csl-entry {
      clear: both;
    }
    .hanging div.csl-entry {
      margin-left:2em;
      text-indent:-2em;
    }
    div.csl-left-margin {
      min-width:2em;
      float:left;
    }
    div.csl-right-inline {
      margin-left:2em;
      padding-left:1em;
    }
    div.csl-indent {
      margin-left: 2em;
    }
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title">Node.js Traps</h1>
</header>
<p><a href="./javascript.html">JavaScript</a></p>
<h1 id="introduction">Introduction</h1>
<p>Node.js is a very widely embraced runtime, but unfortunately it seems to be far more widely used than it is understood. This is particularly dangerous due to the nature of the system which is sigificantly different than most other platforms; the combination of the technical details of Node.js along with some practices that may be widely common in JavaScript can force the responsibility for a range of considerations that are typically mitigated elsewhere on to often unwitting developers. It additionally seems that some of the features in newer versions of ES/JavaScript further mask some of the key plumbing from its users: leading to expectations of new types of magic.</p>
<p>In my experience it seems as though the JavaScript programming model obscures the more foundational inner workings and chunks of the community embrace tenuous hype while ignoring fundamental concerns. Like every other technology Node.js is not a panacea and there are some things for which it is more suitable than others. Unlike many other runtimes however, Node.js is not designed for <em>general</em> computing purposes.</p>
<h1 id="event-loop">Event Loop</h1>
<p>The most prominent concern for Node.js is that it is driven off of a single threaded event loop. This relies on a model of what is equivalent to cooperative multitasking where the sharing of resources is dependent on each unit of work yielding control of those resources rather than being preempted by some form of scheduler. Such a model has enormous benefits when each such unit of work is very small or of roughly equivalent size. This leads to the mandate of “don’t block the event loop”<span class="citation" data-cites="dont-block-the-event-loop">(<a href="#ref-dont-block-the-event-loop" role="doc-biblioref">1</a>)</span>.</p>
<p>The idea that one should not block the event loop seems to be somewhat common knowledge, but practical application less so. I think this often comes down to a lack of understanding of just how easily blocking of the event loop can be realized. Mentioning of concepts like CPU-bound code seems to create thoughts of elaborate, estoric logic rather than something humdrum like code that takes more than constant time (i.e. serializing JSON) and how such code may compose and react as the input varies.</p>
<p>There may be some ways to mitigate event loop blocking but the ways in which it can manifest and cause issues such as IO polling starvation are subtle and manifold and so the most reliable and likely worthwhile approach is to keep the amount of work being done tightly bound. This is therefore the basis for the earlier statement that general compute purposes do not fit naturally within Node.js and instead “the Event Loop should orchestrate client requests, not fulfill them itself”<span class="citation" data-cites="dont-block-the-event-loop">(<a href="#ref-dont-block-the-event-loop" role="doc-biblioref">1</a>)</span>.</p>
<p>For deployment of mid-tier systems such orchestration is likely to be a natural fit and at smaller scale an acceptable level of latency can likely be delivered through a combination of tolerating some blocking and offloading more of the work to upstream systems (such offloading is likely to present issues at large scale). Offloading to other threads with Node.js is certainly an option though seems as though it undermines the proposed value of Node.js). If using streaming and potentially some additional infrastructure Node.js may also offer a compelling filter solution. Also event loop blocking is only a practical issue if there’s something that is blocked and there are likely to be plenty of cases where that is not a concern.</p>
<p>As in most things the most important aspect is to maintain awareness of how this may be impacting your systems so that informed decisions can be planned and made. Keeping track of issues such as event loop lag and the time for the “tick” of an event loop cycle can help drive such awareness and make sure that information is on-hand before things catch fire. Some additional information around keeping track of this will likely be captured later.</p>
<p>A closely related concern that will get further attention shortly is quality of experience fairness. “The fair treatment of clients is thus the responsibility of your application”<span class="citation" data-cites="dont-block-the-event-loop">(<a href="#ref-dont-block-the-event-loop" role="doc-biblioref">1</a>)</span>.</p>
<div id="refs" class="references csl-bib-body" role="doc-bibliography">
<div id="ref-dont-block-the-event-loop" class="csl-entry" role="doc-biblioentry">
<div class="csl-left-margin">1. </div><div class="csl-right-inline"><em>Don’t block the event loop (or the worker pool) | node.js</em> [online]. May 2021. Available from: <a href="https://nodejs.org/en/docs/guides/dont-block-the-event-loop/">https://nodejs.org/en/docs/guides/dont-block-the-event-loop/</a></div>
</div>
</div>
</body>
</html>
