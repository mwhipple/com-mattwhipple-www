<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>GNU Hash Table Support in Mimick</title>
  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      word-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    div.csl-bib-body { }
    div.csl-entry {
      clear: both;
    }
    .hanging div.csl-entry {
      margin-left:2em;
      text-indent:-2em;
    }
    div.csl-left-margin {
      min-width:2em;
      float:left;
    }
    div.csl-right-inline {
      margin-left:2em;
      padding-left:1em;
    }
    div.csl-indent {
      margin-left: 2em;
    }
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title">GNU Hash Table Support in Mimick</h1>
</header>
<ul>
<li><a href="./index.html">Home</a></li>
</ul>
<h1 id="background">Background</h1>
<p>I’m a big proponent of software tests (which I’l likely write about a fair amount) and am most comfortable when programming consists of making automated tests pass and therefore trusting that things will work as they should. I’ve spent <em>many</em> months working on service code without ever running those services locally: simply creating and satisfying tests and then pushing the code through CI/CD.</p>
<p>Over time I’ve grown to tend to avoid mocking libraries. Sometimes they are required due to platform rigidity but often they are at best needlessly complex, and at worst may be hiding questionable design decisions. If you have well defined interactions with appropriately segregated interfaces then directly providing test implementations should be relatively trivial. Maintaining a distinction between pure logic and risky concerns such as IO is another attractive design principle that can further drive down the proposed value of a mocking library particularly given how easily mocked IO calls can provide a false sense of security.</p>
<p>In C, however, the prospect of packaging your code up in some referentially transparent bubble wrap is far more elusive; access to dynamic memory is not absorbed by a lower layer and is likely to be fairly ubiquitous in any relatively large application. While these calls may not be likely to fail it seems reckless to assume that they will not and so code and tests should at least be aware of that possibility.</p>
<p>This seems like a suitable case for mocking. One evident alternative was to introduce preprocessor directives to enable different functionality while testing, but I’m generally opposed to corrupting primary files <em>solely</em> for the sake of tests (very often the design may be tuned to support tests in ways that at least based on my rationalizations are design improvements). Beyond questions of pollution this also implies <em>some</em> degree of difference between the code that is tested and that which is ultimately used. Another option would be to introduce some form of injectable allocator which could provide an appropriate indirection over the system calls, but that seems very clunky and noisy, especially with an eye towards moving away from C.</p>
<p>Quickly scanning around I came across Mimick which seemed like a promising fairly lightweight option. I’d already started using Criterion by the same author (an option I landed on years ago and haven’t felt the need to reevaluate) so it certainly made a lot of sense (I ended up stumbling across a reference to it somewhere on the Web rather than finding any direct path from Criterion). That the example in the README was for stubbing <code>malloc</code> offerred strong support that my needs would be addressed.</p>
<h1 id="the-catch">The Catch</h1>
<p>Unfortunately when attempting to actually use Mimick to simulate allocation failure I received an error from the library about <code>vital functions</code> missing. A search led me to an existing but unresponded to issue<span class="citation" data-cites="mimick-18">(<a href="#ref-mimick-18" role="doc-biblioref">1</a>)</span> for the problem filed by someone else who was using the same distribution I use (Gentoo).</p>
<h1 id="diagnosis">Diagnosis</h1>
<p>My first steps were to check for anything obvious on my system. I made sure that those libraries were present and I checked and fiddled with some of the use flags for glibc that may be having an impact. My integration of Mimick involves checking out the source into a vendor directory so I also concurrently starting sprinkling some <code>printf</code>s throughout the code to see where things were blowing up. I tend to default to the mainstay of debugging through vomiting out debug lines since it’s a quick option to get info without remembering platform specific tools and configuration. I quickly zeroed in on the regions of code that were failing in the code for working with ELF procedure lookup tables (PLT). After fishing around to figure out what fields within what structs would be worth printing I dumped out the libraries that were accessed prior to the results for each and noticed no hash table was found for the <code>libc</code> library.</p>
<p>This confirmed suspicions I had earlier on and so I took a quick detour to see if I could quickly adjust my libc build but it didn’t come together immediately and didn’t seem like the ideal solution regardless. I added some additional information to identify the tags that were present within that file and confirm the suspicion that one of them matched that for the GNU hash table which it did. The standard ELF hash table can have unfortunate runtime along the lines of linear probing in cases where entries are missing which is unfortunately one of those worst case edge cases that is likely to be very common <span class="citation" data-cites="flapenguin-dt_hash">(<a href="#ref-flapenguin-dt_hash" role="doc-biblioref">2</a>)</span>.</p>
<p>Issue identified, now for the fun part of rolling up my sleeves to fix the problem.</p>
<h1 id="put-away-the-hand-soap">Put Away the Hand Soap</h1>
<p>As revealed on the ticket I’d left the firm diagnosis in the morning before the start of my workday and a PR<span class="citation" data-cites="mimick-26">(<a href="#ref-mimick-26" role="doc-biblioref">3</a>)</span> was already opened by the end of the workday without my having to/getting to do anything. I’m not sure if that work was coincidentally already in flight or was just turned around quickly, but it solves my problems so I’m back to other tasks.</p>
<p>The code in the PR appears is in line with that which is typically used to perform lookups on GNU hash tables, making use of constants Mimick provides to properly target the build system. The format itself makes use of a bloom filter to probabalistically short circuit for entries that are not present before looking up entries in the table itself which seems like a more typical modern hash table with more contrained practical worst case performance.</p>
<div id="refs" class="references csl-bib-body" role="doc-bibliography">
<div id="ref-mimick-18" class="csl-entry" role="doc-biblioentry">
<div class="csl-left-margin">1. </div><div class="csl-right-inline">May 2021. Available from: <a href="https://github.com/Snaipe/Mimick/issues/18">https://github.com/Snaipe/Mimick/issues/18</a></div>
</div>
<div id="ref-flapenguin-dt_hash" class="csl-entry" role="doc-biblioentry">
<div class="csl-left-margin">2. </div><div class="csl-right-inline"><em>ELF: Symbol lookup via DT_HASH | flapenguin.me</em> [online]. May 2021. Available from: <a href="https://flapenguin.me/elf-dt-hash">https://flapenguin.me/elf-dt-hash</a></div>
</div>
<div id="ref-mimick-26" class="csl-entry" role="doc-biblioentry">
<div class="csl-left-margin">3. </div><div class="csl-right-inline">May 2021. Available from: <a href="https://github.com/Snaipe/Mimick/pull/26">https://github.com/Snaipe/Mimick/pull/26</a></div>
</div>
</div>
</body>
</html>
