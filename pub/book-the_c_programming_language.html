<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>The C Programming Language</title>
  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      word-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    div.csl-bib-body { }
    div.csl-entry {
      clear: both;
    }
    .hanging div.csl-entry {
      margin-left:2em;
      text-indent:-2em;
    }
    div.csl-left-margin {
      min-width:2em;
      float:left;
    }
    div.csl-right-inline {
      margin-left:2em;
      padding-left:1em;
    }
    div.csl-indent {
      margin-left: 2em;
    }
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title">The C Programming Language</h1>
</header>
<ul>
<li><a href="./books.html">Books</a></li>
</ul>
<p>I’ve picked through “The C Programming Language”<span class="citation" data-cites="DBLP:books/ph/KernighanR88">(<a href="#ref-DBLP:books/ph/KernighanR88" role="doc-biblioref">1</a>)</span> (K&amp;R) a couple times over the years, but have only recently read it cover to cover. It is understandably a classic text; it is well written with compelling exercises and covers a lot of fundamentals. I’d posit that any serious software engineer should read through it, especially given how entrenched C is within modern systems although few developers may directly interact with it on a regular basis.</p>
<p>The primary drawbacks are likely obvious given the age and limitations of C; there is a chasm between the content in the book and what most current developers are likely to be working on. When considering this book next to what is potentially its more modern doppelganger “The Go Programming Langugae” (also written by Kernighan and having a similar style) there is a glaring difference between how readily the knowledge can be applied to modern problems. It seems very important to note, however, that such utility is not free and is delivered by layers of enabling functionality while this book stays far closer to the metal.</p>
<p>This book also clearly defines the boundaries between the (small) language itself which provides the logic which is unlikely to be of practical value and the system libraries which provide those side effects that put the rubber on the road. This is very likely due to the integral role C played in enabling program portability but is an often overlooked distinction as treatments of languages are often lumped together with standard libraries and package managers such that some of those pieces become more difficult to tease apart (sometimes particularly so if the notion of management of external packages is integrated tightly into the language packages). This is of particular note given aforementioned drawbacks in that the chasm to wider practical benefit is filled by such functionality that is largely outside of the language itself and is therefore not precluded but simply omitted due to focus on the language itself and core interactions with the operating system. This is not to say that other languages themselves don’t provide mechanisms which may dramatically simplify some of these needs or amplify key aspects nor to advocate general use of C across wider domains, but rather that C logic is a more direct analog for typical machine behavior and is foundational in most modern systems; understanding of the language itself therefore can foster deeper understanding of the facilities more readily available elsewhere.</p>
<div id="refs" class="references csl-bib-body" role="doc-bibliography">
<div id="ref-DBLP:books/ph/KernighanR88" class="csl-entry" role="doc-biblioentry">
<div class="csl-left-margin">1. </div><div class="csl-right-inline">KERNIGHAN, Brian W. and RITCHIE, Dennis. <em>The <span>C</span> programming language, second edition</em> [online]. Prentice-Hall, 1988. ISBN <a href="https://worldcat.org/isbn/0-13-110370-9">0-13-110370-9</a>. Available from: <a href="https://en.wikipedia.org/wiki/The\_C\_Programming\_Language">https://en.wikipedia.org/wiki/The\_C\_Programming\_Language</a></div>
</div>
</div>
</body>
</html>
