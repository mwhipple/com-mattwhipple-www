;;; emacs_configuration.el --- Configure my Emacs -*- lexical-binding: t -*-

;; Copyright (C) 2021 Matt Whipple <mattwhipple@acm.org>

;; Author: Matt Whipple <mattwhipple@acm.org>
;; Maintainer: Matt Whipple <mattwhipple@acm.org>
;; Keywords: init


;; This file is NOT part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configure Emacs to my liking.

;;; Code:

(progn
  (require 'package)
  (setq package-archives '(("gnu"   . "https://elpa.gnu.org/packages/")
                           ("melpa" . "https://melpa.org/packages/")))
  (package-initialize))
(defun require-ensured (pkg)
  "If `pkg' if not already installed, refresh packages and install `pkg'"
  (unless (package-installed-p pkg)
    (unless (assoc pkg package-archive-contents)
      (package-refresh-contents))
    (package-install pkg))
  (require pkg))
(progn					; dependencies
  (require 'cl-macs)
  (require-ensured 'dash-functional))

(defun define-key-kbd (keymap key def)
  "Call `define-key' after applying `kbd' to `key'"
  (define-key keymap (kbd key) def))

(progn					; low-level
  (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
    (bind "C-g" 'keyboard-quit)
    (bind "M-x" 'execute-extended-command)))
(progn					; help!
  (cl-flet ((bind (apply-partially 'define-key-kbd help-map)))
    (bind "a" 'apropos-command)
    (bind "b" 'describe-bindings)
    (bind "f" 'describe-function)
    (bind "k" 'describe-key)
    (bind "i" 'info)
    (bind "m" 'describe-mode)
    (bind "v" 'describe-variable)
    (bind "w" 'where-is)))
(progn					; help!
  (cl-flet ((bind (apply-partially 'define-key-kbd help-map)))
    (bind "a" 'apropos-command)
    (bind "b" 'describe-bindings)
    (bind "f" 'describe-function)
    (bind "k" 'describe-key)
    (bind "i" 'info)
    (bind "m" 'describe-mode)
    (bind "v" 'describe-variable)
    (bind "w" 'where-is)))
(progn					; theming
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (add-to-list 'default-frame-alist '(background-color . "black"))
  (add-to-list 'default-frame-alist '(foreground-color . "white")))

(progn					; mode-line
  (display-time-mode 1))

(progn					; primitive-motion
  (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
    (bind "C-f" 'forward-char)
    (bind "C-b" 'backward-char)
    (bind "C-e" 'move-end-of-line)
    (bind "C-a" 'move-beginning-of-line)

    (bind "C-n" 'next-line)
    (bind "C-p" 'previous-line)

    (bind "C-v" 'scroll-up-command)
    (bind "M-v" 'scroll-down-command)
    (bind "C-l" 'recenter-top-bottom)
    (bind "M-<" 'beginning-of-buffer)
    (bind "M->" 'end-of-buffer)))
(progn					; content-aware motion
  (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
    (bind "M-f" 'forward-word)
    (bind "M-b" 'backward-word)
    (bind "C-M-n" 'forward-list)
    (bind "C-M-p" 'backward-list)

    (bind "C-s" 'isearch-forward)
    (bind "C-r" 'isearch-backward)

    (bind "M-." 'xref-find-definitions)))
(progn					; primitive editing
  (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
    (bind "DEL" 'delete-backward-char)
    (bind "C-d" 'delete-char)
    (bind "C-k" 'kill-line)
    (bind "C-o" 'open-line))

   (define-key global-map (kbd "M-d") 'kill-word)
   (define-key global-map (kbd "C-j") 'electric-newline-and-maybe-indent)


   (define-key global-map (kbd "M-\\") 'delete-horizontal-space)
   (define-key global-map (kbd "C-q") 'quoted-insert)
   (define-key global-map (kbd "C-w") 'kill-region)
   (define-key global-map (kbd "M-w") 'kill-ring-save)
   (define-key global-map (kbd "M-l") 'downcase-word)
   (define-key global-map (kbd "M-u") 'upcase-word)
   (define-key global-map (kbd "RET") 'newline)

   (define-key global-map (kbd "M-/") 'dabbrev-expand)
   (define-key global-map (kbd "M-%") 'query-replace)

   (mapc
     (-compose 
       (-cut define-key-kbd global-map <> 'self-insert-command)
       'string)
     "abcdefghijklmnopqrstu"))
(progn					; file-management
  (define-key ctl-x-map (kbd "C-f") 'ffap)
  (define-key ctl-x-map (kbd "C-s") 'save-buffer))
(progn 					; window-management
  (define-key ctl-x-map (kbd "0") 'delete-window)
  (define-key ctl-x-map (kbd "1") 'delete-other-windows)
  (define-key ctl-x-map (kbd "2") 'split-window-below)
  (define-key ctl-x-map (kbd "o") 'other-window))
  (progn					; org
    (require 'org)
    (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
    (progn					; agenda bindings
      (define-key org-agenda-mode-map (kbd "C-c ,") 'org-agenda-priority)
      (define-key org-agenda-mode-map (kbd "C-c t") 'org-agenda-todo))
    (setq org-agenda-files 
      (directory-files-recursively org-directory org-agenda-file-regexp t))
    (setq org-agenda-span 'day)
    (setq org-agenda-time-grid
      '((daily today require-timed) () "" ""))
    (setq org-agenda-show-current-time-in-grid nil)
    (setq org-agenda-mode-hook 'hl-line-mode)
    (setq org-cycle-global-at-bob t)
    (define-key global-map (kbd "C-c l") 'org-store-link)
    (define-key global-map (kbd "C-c a") 'org-agenda)
    (define-key global-map (kbd "C-c c") 'org-capture)

    (progn					; primitive-motion
      (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
        (bind "C-f" 'forward-char)
        (bind "C-b" 'backward-char)
        (bind "C-e" 'move-end-of-line)
        (bind "C-a" 'move-beginning-of-line)
    
        (bind "C-n" 'next-line)
        (bind "C-p" 'previous-line)
    
        (bind "C-v" 'scroll-up-command)
        (bind "M-v" 'scroll-down-command)
        (bind "C-l" 'recenter-top-bottom)
        (bind "M-<" 'beginning-of-buffer)
        (bind "M->" 'end-of-buffer)))
    (progn					; content-aware motion
      (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
        (bind "M-f" 'forward-word)
        (bind "M-b" 'backward-word)
        (bind "C-M-n" 'forward-list)
        (bind "C-M-p" 'backward-list)
    
        (bind "C-s" 'isearch-forward)
        (bind "C-r" 'isearch-backward)
    
        (bind "M-." 'xref-find-definitions)))
    (define-key org-mode-map (kbd "C-M-f") 'org-next-visible-heading)
    (define-key org-mode-map (kbd "C-M-b") 'org-previous-visible-heading)
    (define-key org-mode-map (kbd "C-M-n") 'org-forward-heading-same-level)
    (define-key org-mode-map (kbd "C-M-p") 'org-previous-heading-same-level)
    (define-key org-mode-map (kbd "C-M-^") 'outline-up-heading)
    (define-key org-mode-map (kbd "C-c C-j") 'org-goto)

    (progn					; primitive editing
      (cl-flet ((bind (apply-partially 'define-key-kbd global-map)))
        (bind "DEL" 'delete-backward-char)
        (bind "C-d" 'delete-char)
        (bind "C-k" 'kill-line)
        (bind "C-o" 'open-line))
    
       (define-key global-map (kbd "M-d") 'kill-word)
       (define-key global-map (kbd "C-j") 'electric-newline-and-maybe-indent)
    
    
       (define-key global-map (kbd "M-\\") 'delete-horizontal-space)
       (define-key global-map (kbd "C-q") 'quoted-insert)
       (define-key global-map (kbd "C-w") 'kill-region)
       (define-key global-map (kbd "M-w") 'kill-ring-save)
       (define-key global-map (kbd "M-l") 'downcase-word)
       (define-key global-map (kbd "M-u") 'upcase-word)
       (define-key global-map (kbd "RET") 'newline)
    
       (define-key global-map (kbd "M-/") 'dabbrev-expand)
       (define-key global-map (kbd "M-%") 'query-replace)
    
       (mapc
         (-compose 
           (-cut define-key-kbd global-map <> 'self-insert-command)
           'string)
         "abcdefghijklmnopqrstu"))
    (define-key org-mode-map (kbd "M-<RET>") 'org-meta-return)
    (define-key org-mode-map (kbd "C-<RET>") 'org-insert-heading-respect-content)
    (define-key org-mode-map (kbd "M-S-<RET>") 'org-insert-todo-heading)
    (define-key org-mode-map (kbd "M-<LEFT>") 'org-metaleft)
    (define-key org-mode-map (kbd "M-S-<LEFT>") 'org-shiftmetaleft)
    (define-key org-mode-map (kbd "M-<RIGHT>") 'org-metaright)
    (define-key org-mode-map (kbd "M-S-<RIGHT>") 'org-shiftmetaright)
    (define-key org-mode-map (kbd "C-c *") 'org-toggle-heading)

    ;; ChromeOS bindings
    (define-key org-mode-map (kbd "<prior>") 'org-move-subtree-up)
    (define-key org-mode-map (kbd "<next>") 'org-move-subtree-down)
    (define-key org-mode-map (kbd "C-c C-w") 'org-refile)
    (define-key org-mode-map (kbd "C-c ^") 'org-sort)

    (define-key org-mode-map (kbd "C-c /") 'org-sparse-tree)

    
    (define-key org-mode-map (kbd "C-c @") 'org-mark-subtree)
    (define-key org-mode-map (kbd "C-c C-x C-w") 'org-cut-subtree)
    (define-key org-mode-map (kbd "C-c C-x M-w") 'org-copy-subtree)
    (define-key org-mode-map (kbd "C-c C-x C-y") 'org-paste-subtree)
    (define-key org-mode-map (kbd "C-c C-x c") 'org-clone-subtree-with-timeshift)
    (define-key org-mode-map (kbd "C-y") 'org-yank)
    (define-key org-mode-map (kbd "C-c C-x v") 'org-copy-visible))

(progn					; C
  (add-to-list 'auto-mode-alist '("\\.c\\'" . c-mode)))
(progn					; elisp
  (add-to-list 'auto-mode-alist '("\\.el\\'". emacs-lisp-mode))
  (define-key emacs-lisp-mode-map (kbd "C-x C-e") 'eval-last-sexp))
(progn					; javascript
  (add-to-list 'auto-mode-alist '("\\.js[mx]?\\'" . javascript-mode))
    (setq js-indent-level 2)
    (add-hook 'js-mode-hook (lambda () (setq indent-tabs-mode nil))))
(progn					; BibTeX
  (require 'bibtex)
  (add-to-list 'auto-mode-alist '("\\.bib\\'" . bibtex-mode)))
(progn
  (add-to-list
    'auto-mode-alist '("[cC]hange[lL]og[-.][0-9]+\\'" . change-log-mode)))
