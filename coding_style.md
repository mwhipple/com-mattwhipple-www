---
title: Coding Style
---

- [Software](./software)

---

I've certainly acquired some strong tastes for coding style,
probably primarily drawing from time spent _reading_ code either
through having to work with such code or through voluntarily reading
in pursuit of knowledge (and taste acquisition).

Many of the resulting ideas are likely to be touched on
elsewhere.

## Small Footprints

## Economy

## Avoid Hourglass Code

## Composition of Simplicity

A typical pattern is to consolidate behaviors using patterns that
amount to consolidation of complexity. Very often this seems to result
from combining multiple things together, for example creating a
function that does foo and bar may be bundled up into a function
`doFooBar`, but then there may further need to be the desire to only
foo and not bar which would result in a parameter and branching added
to that function to only do what is necessary or perhaps some
additional logic to determine what should be done.

Very often it is far simpler and more flexible (and often dramatically
easier to understand) to just define the smaller pieces and compose
them as necessary. There is no difference in expression between
`doFooBar` and separate invocations to `foo` and `bar` but the latter
avoids consolidation of complexity. This general pattern can then be
extended in assorted ways, making use of higher order constructs and
patterns with the end result being that simple pieces can be composed
together into simple structures rather rather than trying to force
everything into increasingly amorphous super-buckets. Likely the
clearest specific pursuit of this approach is that flexibility can be
increased without a commensurate increase in complexity. When combined
with practices such as polymorphism a fantastic range of functionality
can be delivered with very little branching.

This seems to be a fairly common practice in mature APIs and aligns
very naturally with common design principles such as SOLID but I very
often see the opposite approach in practice where logic is
increasingly and needlessly consolidated in functions seemingly driven
by the idea that a single increasingly ungainly signature is a better
interface for callers than multiple focused operations.

## Chekhov's Gun

Likely the most distinguishing factor is that I strive for code that I
can most easily _fully_ understand which is contrary to practices
which may provide superficial understanding. A closely related notion
is the dramatic principle of Chekhov's gun: everything in code should
be bound as closely as possible to the reasons for its
existence. While this may sound fairly standard the additional
refinement is that much code is written in ways that express how it
_may_ behave but is not sufficiently constrained to convey the precise
which ways it _does_ behave and therefore also clearly expressing what
it does not do (which is left to inference from absence).

An example of this would be tightly scoped binding environments and
avoidance of otherwise redundant lvalues. Some of these concerns may
seem fairly trivial but when trying to _understand_ the code it is far
less of a lift on the prefrontal cortex to be able to read something
and move on rather than have to keep some symbol floating around in
the wings in case it is referenced further along.

## EIBTI