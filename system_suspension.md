---
title: System Suspension
---

- [Software](./software)

A concern that gets pushed to the foreground when running Linux on a
laptop is managing suspending to memory or disk. While there may be
more robust ways to preserve state across shutdowns, suspension
provides a straightforward way to quickly pause and resume work
while reducing consumed energy in the interim.

systemd provides built-in commands for this but as part of my most
recent installation I've opted for lighter weight distinct tools.
While I remember having to jump over additional hurdles years ago to
get hibernation working (I think at the time there was less kernel
support and external solutions such as tux-on-ice were required), on
this install most things seemed to be in place (though my initial
kernel config is admittedly a bit bloated) and I simply needed to
install the packages suggested on the Gentoo wiki
page[@gentoo-suspend-and-hibernate]. I started with `hibernate-script`
package for the lightweight reasons mentioned above and having used it
in the past. `suspend` seems promising but I didn't have the
information necessary to quickly assess it, though I may revisit it
later as I'm speculating that it's a more modern alternative; neither
need to be much more than thin wrappers around messages to the kernel
though some legacy cruft could certainly be lurking.

At the moment I have suspension wired up to a key binding, though I'll
shortly replace that with the lid close event.

My laptop at the moment consumes a fair amount of battery while
suspended to RAM and therefore I'll be looking to either prefer
and optimize hibernation to disk or adopt the hybrid route.
The resume option was configured as a kernel option which can likely
move me a small step closer to the optimization route (if I remove grub).