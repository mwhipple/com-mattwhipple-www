---
title: Home - Matt Whipple
---

Welcome to my Web site.

Currently the navigation on this site is structured as a tree without
any additional assistance in terms of a full map of that tree or
indication such as recent updates. As a result navigating the site may
not be useful unless you know what you're looking for until such
alternative paths are provided.

- [Assorted Thoughts](./assorted_thoughts.html)
- [Blogroll](./blogroll.html)
- [Books](./books.html)
- [Career](./career.html)
- [Computers](./computers.html)
- [Camping](./camping.html)
- [Cat Bladder Stones](./cat_bladder_stones.html)
- [Conscious Consumerism](./conscious_consumerism.html)
- [Home Ownership](./home_ownership.html)
- [My Resume](./my_resume.html)
- [Personal Setup](./personal_setup.html)
- [This Site](./this_site.html)

