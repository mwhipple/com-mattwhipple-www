---
title: Cat Bladder Stones
---

- [Home](./index.html)

My family has a cat who is now getting to be fairly old
(currently 14 years old). He's basically been an indoor
cat for all his life and has rarely had health issues and
has until now typically own gone to the vet when precipitated
by such an issue.

When my son was a newborn there was such an issue where he
seemed unable to urinate so I brought him fairly late in
the night (not that time matters much with a newborn) during
a snowstorm. That time seemed to be primarily due to stress
partly caused by the large amount of accumulated snow that
winter (he was unable to see outside) and a follow-up visit
to the vet prompted me to introduce wet food into his diet
(which had been previously absent) and to prefer grain-free
food.

There was then no issues for over 6 years, but a couple months
ago he once again had issues urinating. My _theory_ is that
the pet store did not have the food I'd been getting him and
so I bought a bag of another grain-free alternative and some
combination of the change of food in general and a possibly
less appropriate formulation caused the issue, but that is a
theory that will remain untested since our cat is a pet and
not a specimen. Once again the fear of severe consequences led
to an emergency visit (this time scheduled at a local vet
rather than a dash to a 24-hour facility).

During this visit I was told that the issue was likely tied
to issues around crystals, and that his diet should instead
include products which are focused on urinary health and that
any pursuit of grain-free should be driven primarily by
specific dietary concerns (whereas previously I was told that
it was an inherently better fit for cats in general).
After getting some medicine for immediate relief and some
prescription food he was once again back to himself after
a brief period where things righted themselves.

Unfortunately he's now having some issues again, likely due to
my getting too relaxed and not adhering to the recommended
diet strictly enough. After a day of him seeming uncomfortable I
ran to the pet store to get some supplies that had run down
(wet food and cat litter) and unfortunately went to a
larger pet chain as they had the desired food (Hills c/d)
on their Web site but didn't have it in-store (this is where
I discovered the foot itself is apparently prescription). I
grabbed some Urinary Hairball Control in the hopes that it's
roughly equivalent especially when combined with the c/d
dry food and will plan on just going to the vet office next
time I need food. He now seems to be relatively comfortable
but continues to have some urination problems likely due to
some remaining stones[@vca-cat-struvite-bladder-stones]
that need to dissolve or be passed.

Neither of the times he needed to go the vet was he blocked
which would the source for major concern.
I'll be monitoring his consumption and expulsion of food
and water and will plan on running him to the vet again
if anything seems amiss; though for now I'll be planning
on learning a bit more about the condition and giving his
tract some time to get back to a healthy state and pH.
