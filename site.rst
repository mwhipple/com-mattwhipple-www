.. _AboutSiteContents:

#########
This Site
#########

.. toctree::
   :maxdepth: 1

   site_links
   site_motives
   site_style
   site_tech

The content of this site is going to continuously evolve
in the style of a wiki [WabiSabi]_, so any pages are subject to change
after initial publication. The history of such changes will
be available in version control. Information provided will be
supported through linked sources. Initially most of the information
is likely to be related to computers
but this may change as I acquire more time for more general
research.

**************
Site Structure
**************

Navigation through this site is presented as a tree. Any immediate
children of a page will be presented at the top of that page
(which can be used to descend the tree) and the ancestry/path of
the current page will be captured in the breadcrumb at the top
of each page (which can be used to ascend the tree).

The URL structure itself is flat, with the navigation acting as
an organizational overlay. This is in keeping with the recommendations
outlined by W3C Cool URIs [CoolURIs]_; allowing the overlaid structure
to evolve while the URLs themselves remain stable.

.. rubric:: References

.. bibliography:: /core.bib
   :filter: topic %"SiteGeneral"

