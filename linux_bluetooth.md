---
title: Linux Bluetooth
---

- [Linux Configuration](./linux_configuration.html)

In pursuit of configuring my personal laptop, I've begun to work
on getting bluetooth straightened out. I've tended to not use
bluetooth too heavily, but the post-pandemic increase in video
conferencing in a house with young children make the use of a headset
very valuable and while BT may not be a requirement it is a standard
convenience.

The Gentoo provided wiki page provides basic configuration guidance
which worked largely without issue[@gentoo-bluetooth]. I've used
`bluez` intermittently over the years and now apparently I'll make
enough use of it to acquire decent familiarity. I'll likely also look
at adding some conveniences for configuration.

To actually get my BT headset working consistently I also needed to
borrow ideas from an ArchWiki page[@arch-bluetooth-headset] though I
need to capture the specifics of the resolved solution once I actually
get past the point of periodically needing to jiggle the handle.