Matt Whipple
============

----

>  I am a driven autodidact with a track record of leading \&
>  contributing to solutions across a variety of domains throughout the
>  software development lifecycle.

----

[Autonomic](https://autonomic.com)
-----
2022--present
: Architect

Shepharded technical information to drive
best practices and convergence towards long term goals.

- Empowered teams to assess technologies by improving an internal tech radar

[Pear Therapeutics](https://peartherapeutics.com/)
-----

2020--2022
: Senior Engineering Manager

Managed a team charged with enabling a new microservice architecture,
with a focus on coordinating knowledge and empowering informed decisions

* Enabled acquisition of crucial new revenue streams by extending
  algorithm configurations within core business logic
  *- Node.js - Sequelize - MySQL*
* Increased attention to deployed systems by introducing operator manuals
  and regular review and improvement of runtime metrics
  *- Datadog - OpenTelemetry*
* Significantly reduced deployment costs by streamlining images & pipelines.
  *- Docker - GitLab*
* Bolstered data consistency through hands-on workshops and reference materials
  explaining the pitfalls of distributed systems
* Participated in the definition of new process runbooks intended to reduce
  friction while encouraging traceability and measurability
* Fostered shared understanding and unpacked tribal knowledge through domain
  modeling sessions and requirements engineering practices
* Worked closely with the operations team to initially vet and adopt
  new infrastructure solutions
  *- GitLab - Kubernetes - Helm - Rancher - Harness - Vault*
* Collaborated with the data science and clinical teams to model and
  optimize the parameters for a treatment schedule
  *- Python - pandas - Jupyter Notebook*
* Defined and initiated transparent extraction and verification of a
  microservice from a monolith
  *- NestJS - TypeScript - TypeORM - PostgreSQL - AWS DMS - Stoplight Prism - OpenAPI*
* Improved build tooling more with increased reproducibility and targeted feedback
  *- Make - Docker*
  
[Robin](https://www.robinpowered.com)
-----

2019--2020
: Principal Software Engineer

Improved internal analytics tooling and practices to enable
reliability, flexibility, and transparency

* Designed and implemented end-to-end tests for an ETL process
  *- Jest - Docker - Jenkins*
* Established practices and tooling to design and implement a
  dimensionally modeled data warehouse
  *- dbt - Terraform - AWS Redshift*
* Introduced practices to explore, document, and validate data processing algorithms
  *- Jupyter - AWS SageMaker - Pandas - Hypothesis*
* Initiated technical design for new analytics front-end
  *- React - CubeJS - TypeScript*
* Contributed to initial rollout of federated GraphQL gateway
  *- Apollo - TypeORM - Kubernetes*
* Extended SCIM API to support enterprise user attributes
  *- PHP - MySQL*

[Brightcove](https://www.brightcove.com)
----

2015-2019
: Principal Software Engineer

Contributed to and led several teams which delivered and supported
services providing high availability to high traffic volume

* Led the team owning the video metadata REST API used to coordinate system interactions,
  requiring highly available handling of $\approx 2500$ requests/second
  *- Kotlin - Groovy - MongoDB*
* Designed integration of acquired external data
  *- Go - Kafka - Kubernetes - MySQL - MongoDB*
* Enabled new search functionality while reducing development costs through
  improvements to the syntax, codebase and result relevancy
  *- Elasticsearch - Solr - Lucene - ANTLR*
* Diagnosed an ELB scaling issue which threatened the broadcast of an event to millions of users
* Provided smoother and more controlled resource managment by
  refining operations practices and infrastructure as code tooling.
  *- Terraform - Datadog - Sumo Logic*
* Designed and led development of an API gateway used for customer
  facing APIs which empowered each development team to configure and expose
  their service while maintaining a baseline of consistent functionality
  *- nginx - OpenAPI - Kubernetes - C - Kotlin*
* Led a team to update authorization services
  *- OAuth2 - Clojure - Kotlin - Cassandra - Docker*
* Developed inter-team solutions for a variety of use cases regarding building,
  releasing and deploying software
  *- Make - Docker - Bash - TeamCity - Gradle - Kubernetes - Git*
* Designed and developed a service to manage inter-customer relationships
  and coordinate related changes
  *- Kotlin - DynamoDB - ReactiveX - AWS*
* Configured a deployment environment and tooling for containerized applications
  *- ECS - Terraform - TeamCity - AWS Lambda - Datadog - SumoLogic - CoreOS*
* Removed a cause of spikes in database locks and resulting latency by splitting
  large transactions to use an asynchronous bottom half
  *- Oracle - SQS - Hibernate*
* Established testing strategies which ensured coverage while minimizing overhead
  *Spock - Spek - Brine - RSpec - JUnit - BATS - Dredd*
* Improved traceability and resiliency across threads/processes and services by
  establishing common patterns for integration points
  *- Java - Logback *

[Harvard Business Publishing](https://www.harvardbusiness.org)
---

2011-2015
: Senior Application Developer

Contributed to all aspects of the development of e-learning solutions:
establishing designs, technologies, and solutions throughout the stack.
*- Spring - Seam - AngularJS - Bootstrap - jQuery - Oracle*

> <mattwhipple@acm.org> • +1 (508) 317 7435 • Mattapoisett, MA, USA
