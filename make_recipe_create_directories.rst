#################################
Make Recipe: Creating Directories
#################################

**********
Background
**********

The need to create directories as part of a build process
is a common one, and there are several documented practices
to handle this. I've settled on one however, that seems to
work particularly well yet is not one of the established
approaches. I've not seen it seen it documented elsewhere
though I also haven't looked; and that could certainly also
be an indication that there is some major issue with this
approach.

*****************
Proximal Pitfalls
*****************

There are a couple pitfalls that can mess with
possible solutions to this task: as I write this I also
realize that this solution also has a related limitation,
which I'll document at the end. The first is that make
will remove trailing ``/``s from targets, so there is no
difference between targets such as ``out :`` and ``out/ :``.
So that effectively means `make` doesn't differentiate
from a file and directory target (this simple text slinging
model is often very useful). The second is that pattern
rules *seem* like they could be very useful to create
containing directories as needed, but the likelihood of
having a pattern that matches a shorter stem than that
which triggers directory creation is fairly high which
means that the required target won't be called and the
build will won't be able to proceed.

******
Recipe
******

Easy creation of directories can be done by collecting
all such directories into a variable, and an appropriate
recipe to create the directories for that variable's values.
This results in a pattern such as:

.. code-block:: make

   BUILD_DIR := build/
   OUT_DIRS  += ${BUILD_DIR}

   ${OUT_DIRS} : ; mkdir -p $@

The recipe and target only needs to be defined once, so
additional directories would simply need to be added to the
accumulating variable (``OUT_DIRS`` in the example above).
The recipe for the variable should be placed near the end of
the Makefile so that all values have been collected into the
variable and can be expanded into the target.

This approach provides a bit more more cohesion to the definition
of directory creation, and it also enables conventional cleaning
of any of the created directories. Making sure all directories
generated using the above are removed during a clean is as simple
as:

.. code-block:: make

   clean: ; rm -rf ${OUT_DIRS}

``rm -rf`` has its usual dangers: it should be verified that
``OUT_DIRS`` can only contain known, safe values. This is
particularly pertinent in cases where some of the values may
be dynamically generated.

********
Pitfalls
********

I've been using this approach across several projects without any
notable issues. The one snag that I have run into is that the
directory creation will do nothing if a file exists with the
same name as the directory that would be created. This is a
typical potential issue with make and there is nothing specific
about this recipe that leads to it, though this solution may
provide less useful feedback than alternatives such as marker
files; make does not differentiate between a file and directory
target and will normally consider the target realized if such
a file is present. Additional verification could be added to
the Makefile if this is a concern but if project directories
are kept clean and controlled then it shouldn't be a practical
concern (and the additional noise and complexity can be avoided).
In short: don't do that.
