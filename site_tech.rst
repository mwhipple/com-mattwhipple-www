##########
Technology
##########

***********
Static Site
***********

This site is a static Web site [StaticWebSiteWikipedia]_.
This is used as it
significantly simplifies hosting requirements and
fits naturally with the workflow and tooling that
I normally use for software development (using a
text editor and then building and publishing
artifacts).

*******
Hosting
*******

Being a static site, this site only depends on what any Web host
should be able to provide. I opt for a cloud provider because
it aligns with other work I do, a usage based pricing model is
attractive (especially with a free tier ;)), and of course it
will be able to handle the surge in traffic when I inevitably
get slashdotted.

I opted for Google Cloud Platform's (GCP) Google Cloud Storage
as a chance to tinker around with GCP. Google tends to
offer decently accessible tooling; providers like
AWS may have a little bit more assembly required which is not
an issue for any relatively complex solution but can add undue
overhead to trivial cases.

**************
Build & Deploy
**************

The site is built and deployed using Make, which calls
sphinx-build to generate the HTML for the site and uses
gsutil's rsync command to publish the site to GCP.

.. rubric:: References

.. bibliography:: /core.bib
   :Filter: topic %"SiteTech"
