---
title: Make Style
---

- [Make](./make.html)

---

I write enough Make that I've adopted some style preferences.

# Use tabs for indentation

just kidding (that's pretty much mandatory)

# Use braces to expand values and parentheses for complex expressions

Make doesn't differentiate between using `${..}` and `$(..)` for
interpolation of values and a common practice is to use `$(..)`
ubiquitously. Beyond that there's not also a particlar distinction
between whether an expansion corresponds to something relatively
simple like the value of an assigned variable or the result of
evaluating some arbitrary logic (akin to potentially impure
lambda calculus). In spite of this I tend to use `${..}` for
values which are more similar to variables (typically nullary
assignments regardless of when they are evaluated) and `$(..)`
for constructs which are more obvious evaluations (those with
parameters). This is analogous to Bash's distinction of brace
and command substitution.

# Use braces for single character identifiers.

As in many systems with similar interpolation syntaxes, the use
of braces or parentheses is sometimes optional. Make is more
limited than most such systems in that it is required whenever
the identifier is multiple characters, but I'll try to use
braces consistently even for single character identifiers.
This is particularly noticeable with some of the automatic
variables such as (with braces) `${*}`, `${@}`, `${<}`.
This is done for consistency and to hopefully provide more
of a hint as to what is going on for anyone not familiar
with Make in cases where there are practices like nested expansion
or recipes that may start to resemble line noise.

# Identify Directories With Trailing Slashes

I think there was originally some practical benefit for adopting
this practice, and there are certainly some pitfalls in using it
which will be documented next time I stumble into one.
When referencing directories I store a trailing slash as part
of the directory, e.g.:

~~~{.Makefile}
BUILD_DIR := build/
~~~

From a conceptual standpoint it offers the advantage that it
more clearly indicates that the value represents a directory
while also containing that knowledge such that it can provide
a generalized container where it is used. As the usages would
resemble `${CONTAINER}${ITEM}` anything that could act
as a prefix can be used to qualify the items rather than being
limited to directories. This matches a general practice where
the initial stringiness and delayed semantics of make are leveraged.
