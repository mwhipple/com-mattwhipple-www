---
title: Work Setup
---

- [Home](./index.html)

Here I'll catalog some of my setup for how I work.
This is going to largely reflect how I approach my
personal work rather than my professional work where
I'm likely to adopt approaches which are likely to be
more readily assimilated across a team.

This is related to some of the technology reboot efforts.

# "Office"

I've been largely working from home from several years
and so have had a home office in one form or another.
The COVID-19 pandemic shifted me to working remotely
nearly exclusively which also somewhat led to my rethinking
my home office setup. While as mentioned I do have an
office (though it is at the moment uninsulated), I'm
instead focusing on optimizing my ability to perform
mobile work. One of the potential great advantages to
working remotely is the ability to work anywhere, so
it seems foolish to squander that through becoming
overly reliant on the trappings of a particular
physical place.

My primary office is therefore a backpack in which I
keep most of my daily essentials and have one pocket
filled with the equipment needed for my work
(with my work laptop is in another pocket).
