---
title: My Resume
---

- [Career](./career.html)

Here I'll be working on capturing my resume.
For quite a while now my resume has been produced using a LaTeX
document which I'm now hoping to generate using Pandoc[@pandoc-users-guide]
which will enable the same markdown source to produce the canonical PDF
(potentially through the same general LaTeX template) and an HTML
presentation more suitable for the Web.
