---
title: Make - Collect Source Files
---

- [Make](./make.html)

One of the core purposes of Make is to only perform that work
which is needed based on what has changed, and to support that
the inputs/dependencies for a given output/target should be defined
(and normally composed into some DAGs).
Make's simple model leaves not only little in the way of implicit
input mapping but also very limited pre-packaged options to collect such
inputs.

The most standard means to collect sources is likely `wildcard`, but that has
the limitation in that it does not provide recursion. That restriction can
certainly be worked around but those solutions are likely to be heavy-handed
for simple cases. In such cases starting with a call out to some command
such as `find` or an incantation of `ls` may be more straightforward.

# In a Git Repository

In git repositories (which seem to be the de facto standard) `git ls-files`
[@man-git-ls-files]
can also be used which provides the benefit of correlating directly with
what git sees as sources which should be fairly canonical. This excludes
files that should be ignored and can be further tuned based on git status
if that makes sense.

An example collection which gets files in any state while honoring standard
exclusions looks like:

~~~{.Makefile}
SRCS := $(shell ${GIT} ls-files --cached --deleted --others --modified --exclude-standard .)
~~~

The `.` directory specifier at the end can be modified to target appropriate subtrees
such as `src` or `tests`. It does not _seem_ as though ls-files supports selection based
on patterns, but the functions that make provides or piping results through a filter such
as `grep` can provide that behavior.

Note that `:=` is used rather than `=` so that the value is only expanded once.
In most projects this is likely appropriate as this call should not fail and should
be relatively inexpensive: on a large project that expense may be become more significant.
There are numerous potential ways to address such costs but all of those fall outside
the scope of the `simple cases` which this addresses.
