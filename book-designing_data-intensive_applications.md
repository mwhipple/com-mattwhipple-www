---
title: Designing Data-intensive Applications
---

- [Books](./books.html)

---

I likely don't have much to say about Martin Kleppmann's
Designing Data-intensive Applications[@designing-data-intensive-applications]
that hasn't been said before. This is one of my go-to recommendations
and one of the few books I ordered a print copy for after reading an
electronic copy...that was likely pre-pandemic (before I went fully
remote) when I expected to be able to loan the copy to workers but
it's still worth having for it's referenced sources alone.

The book provides fantastic coverage of the concepts that underlie the
challenges facing many engineers currently, and of which anecdotally most
of those engineers seem to be blissfully unaware and therefore
mistakenly take things for granted.

I recently re-read it somewhat arbitrarily due to it being the most
popular book on O'Reilly learning online and it continues to prove
valuable to bolster some of the vocabulary around the concepts with
which I'm fairly familiar and also to pick up on some ideas that
didn't stick during previous reads.

I personally find the book to be a perfect mix of concepts which are
likely to remain somewhat evergreen and how they play out in
practice. I was somewhat surprised somewhat recently to hear a
(young) teammate refer to the book as being an academic reference; the
re-reading confirmed my previous thought that this is _not_ the case.
While it certainly does not provide any specific recipes it helps
explain the fundamentals which guide the which and why for those
recipes and many of the inescapable tradeoffs lurking among them.