\version "2.18.2"
\paper {
  #(set-paper-size "letter")
  top-margin   = 10
  left-margin  = 10
  right-margin = 10
}
\header {
  title    = "Minuet in G major"
  composer = "Christian Petzold"
}

\score {
  \new PianoStaff <<
    \new Staff = "RH" {
      \tempo "Allegretto"
      \time 3/4
      \key g \major
      \relative c'' {
        \repeat volta 2 {
          d4_5\p(
           g,8 a8 b8 c8   | d4-.) g,4-. g4-. |
          e'4_3( c8  d8 e8 fis8 | g4-.) g,4-. g4-. |
           c4_4( d8 c8 b8 a8    | b4 c8 b8 a8 g8   | fis4_2 g8_1 a8 b8 g8 | \acciaccatura b8 a2.) |
          d4_5(
           g,8 a8 b8 c8   | d4-.) g,4-. g4-. |
          e'4_3( c8  d8 e8 fis8 | g4-.) g,4-. g4-. |
           c4_4( d8 c8 b8 a8    | b4 c8 b8 a8 g8   |   a4     b8   a8 g8 fis8_2 | g2._1) |
        }
        \repeat volta 2 {
          b'4_5\mf( g8 a8 b8 g8 | a4_4 d,8 e8 fis8 d8 | g4_4 e8 fis8 g8 d8_1 | cis4_3 b8 cis8 a4) |
          a8(\< b8 cis8 d8_1 e8 fis8\! | g4-.) fis4-. e4-. | fis4-. a,-.\> cis4-. | d2. |
          d4\p\!( g,8 fis8_2 g4_1) | e'4( g,8 fis8 g4) | d'4_5( c4 b4 | a8 g8 fis8_2 g8_3 a4) |
          d,8(\< e8 fis8 g8_1 a8 b8\! | c4-.) b4-. a4-. | b8-.\>( d8-. g,4-._1) fis4-._2\! | <g d b>2. |
        }
      }
    }
    \new Staff = "LH" {
      \clef "bass"
      \key g \major
      \relative c' {
        \repeat volta 2 {
          <g b d>2 a4 | b2. | c2. | b2. |
          a2.         | g2. | d'4 b4_3 g4 | d'4 d,8_1 c'8 b8 a8 |
          b2 a4       | g4_4 b4 g4 | c2._1 | b4_2 c8 b8 a8 g8_4 |
          a2_1 fis4_3 | g2_2 b4_1  | c4_2 d4 d,4 | g2_1 g,4 |
        }
        \repeat volta 2 {
          g'2. | fis2. | e4 g4 e4 | a2 a,4 | a'2. | b4-._3  d-. cis4-. | d4-. fis,4-._4 a4-._2 |
          d4( d,4 c'!4_1) |
          <<
            { \voiceOne r4 d2 | r4 e2 }
          \new Voice
            { \voiceTwo b2_3 b4 | c2_2 c4 } |
          >> b4_3 a4 g4 | d'2 r4 |
          <<
            { \voiceOne r4 r4 fis,4_3 }
          \new Voice
            { \voiceTwo d2. }
          >> | e4-. g4-. fis4-. | g4_1( b,4-.) d4-. | g4-. d4-._2 g,4-. |
          
        }
      }
    }
  >>
}
