---
title: Washer/Dryer Replacement 2021
---

- [Home Ownership](./home_ownership.html)

# Conclusion

This page captures information from when I was exploring purchasing a
new washer/dryer, but the plan outlined here wasn't realized and will
be cleaned up to reflect reality. The Bosch listed here was ordered
but was never delivered and I ended up having to chase down a refund
from Home Depot. In the meantime the previous washer dryer continued
to hobble along for several more months and then it sounded as though
some bearings were shot and the dryer belt broke (again). The desired
Bosch model with the heat pump seems to have disappeared and so
thankfully I was able to consult this page and go with the Miele
option listed, which was purchased from a local appliance store (from
which I purchased a new microwave a few weeks prior as all of my
appliances seem to be reaching the ends of their lives).

The dimensions remained a major factor (and led to the need for
additional electrical work) but I also just had the appliances
installed now that the state of COVID allows _some_ level of comfort
with unfamiliar people in my house for a brief period.

# Background

The washer and dryer in my house as I write this are the ones
that were here when we moved in. It is a stacked set of Asko
units which I'd imagine were probably nice at the time they
were installed but have likely significantly surpassed the
typical modern lifespan of a washer and dryer. Of particular
note is that the dryer takes a _long_ time and they have a
small capacity which leads to the situation that the dryer
is running nearly constantly.

A couple of years ago the belt in the dryer broke which unfortunately
led to some clothes near the heat getting ruined. At the time
the age and poor performance of the dryer led me to look at
replacing it, but unfortunately while measuring the upstairs
laundry closet in which it resides I discovered that the
depth of the closet was shallow enough to apparently significantly
limit the options that would fit in the space. I didn't think paying
to have the dryer fixed professionally was worth it but being
fairly sure it was a broken belt I figured I could order a
replacement and atttempt to fix it and meanwhile we asked some
contractors doing other work to narrow the threshold which would
extend the depth a few inches. I managed to get the belt replaced
(which was a nuisance of contortion due to lack of access to the
motor but didn't take too long) and the contractors didn't perform
that bit of work (they were going to throw it for free while working
on other projects around the house), and so ultimately everything
was reset back to the initial state where the dryer was fixed for
something in the neighborhood of $15 and punishing my arms and hands
with getting pinched between some of the pieces that weren't
readily separable.

Now, however, the dryer has switched from being run nearly constantly
to being a runaway that no longer stops (which makes sense if we
anthropomorphize the dryer and subject it to such undending toil until
it finally cracks). It seems the time borrowed with the belt replacement has
run out, and all things considered this is a project that seems suitable
for me to take on myself.

# Dimensions

Knowing the dimensions of the space were an issue before that seemed
like a decent place to start this time. The troublesome depth
is 24" which is largely imposed by an aforementioned threshold;
some solution or other could extend the available depth to a far
more accommodating 28" which would roughly fill the area from the
door to the rear floorboard. The closet width is roughly 28" which
is likely relatively unimportant given that 24" seems to be a fairly
standard (European) width for smaller profile washers and dryers and the 24"
is also the width I've quickly checked for viability of navigating
the washer and dryer through the house to the laundry closet. The
available height is 73". All measurements are based on the doorway
as that acts as bottleneck for utilizing the inner space.

A particular note is that the 24" depth is the depth of the absolute
base whereas most listed depths are that to the closed door. In addition
to the prospect of expanding the available depth obtaining the more
detailed dimensions for a particular model may indicate that any such
adjustment is unnecessary.

# Vendor Preference

One of the first questions was where to buy the equipment. In this case
I have a sizeable nest egg of American Express rewards points which could
absorb a fair amount of personal costs and the exchange ratio of
points to value for Home Depot gift cards is one of the best. I'd typically
look for a more local company and I was led to question any
political implications of Home Depot at my wife's prodding. After
some cursory investigation into the political aspects it appears as
though the claims are fact-based but ultimately misleading
[@snopes-lowes-versus-home-depot-meme]; while Lowes
_may_ have more promising political leanings (which seems uncertain at best),
Home Depot seems to be fairly evenly mixed while also demonstrating a
reasonable reaction to the racial tensions of 2020 [@home-depot-racial-equality-justice-all]
(though that could certainly just be PR and the amount seems relatively trifling).
Ultimately there's not a clear enough case to outweigh the
financial benefit of redeeming gift cards using my rewards points.

# Model Selection

While I'm purchasing a washer/dryer combination there is far more
variability that I'm aware of for dryers when considering compact
models. Most of the decision will therefore be driven by properties
of the dryer.

## Vented vs. Ventless Dryer

A consideratin is whether to purchase a vented or a ventless dryer.
The dryer that is being replaced is a vented model and my uninformed
impression was that vented would be preferable given that the relevant
ducts already exist, but upon further discovery it seems as though
ventless may be an overall better fit. Ventless are apprarently generally
more efficient, and while the time per load may be higher than vented it
is still almost certainly less than that of our previous dryer.
It sounds as though an additional consideration is that vented dryers are
more likely to heat the ambient air whereas ventless are likely to
introduce humidity; this also seems preferable in my case as the
dryer is in a living area where the dryer may compete with air conditioning
in the summer and additional humidity in the colder seasons is welcome
to compensate for the dry heated air. Fortunately the initial cost difference
does not need to be a deciding factor and the prospect of lower utility
bills is promising[@16-ventless-vs-vented-dryer].

Out of the ventless options it appears as though a heat pump would
generally be preferable over a condenser. The former is more efficient
and able to operate more independently of environmental conditions.
This may be more of a requirement than a preference given that the
installed location is a closet.

## Candidate Collection

For high level guidance I started with the 2021 list provided by
Wirecutter[@wirecutter-compact-washer-dryer-2021].

### Bosch 300

Their top pick is a Bosch 300 model which coincidentally matches a
dishwasher I installed last year and which has been
working well. Installation of the dishwasher was delayed while
waiting for additional required parts; this is leading to
extra attention during this purchase to minimize any time spent
without a washer/dryer (which was thankfully less of an issue with
a dishwasher).

#### Vendor

The Bosch has the major advantage that it is
available from Home Depot (for the reasons above) but the
disadvantage that it is uses a condenser rather than a
heat pump. There are some follow up practical questions however.

#### Power

The Bosch requires a single 240-volt outlet so an initial question
is verifying what is currently present in the laundry closet. My
suspicion which is confirmed by looking at the installation guide
for the previous model is that the outlet is 240V, so that is
not a concern.

#### Drain

An additional consideration for this dryer is that it needs a drain.
While there is certainly a drain present in the closet that is used
by the washer, there's an open question of whether that would be
sufficient. I'd hope that the dryer can piggyback on the washer
drain as it does the power, but that needs verification.

#### Base Size

Consulting more precise dimensions for this washer/dryer reveals that it
should fit within the closet without modification.

#### Condenser in a Closet

The single biggest concern as referenced earlier is the use of a
compresser rather than a heat pump. While I think there should be
sufficient airflow to minimize concern and as previously mentioned
the additional humidity may actually be beneficial, I'm reluctant
to take the chance of installing a unit that may introduce
significant issues.

### Electrolux

The runner up choice is also a condensor model and has indicated reliability
issues, so this is quickly dismissed as an option.

### Miele

The Miele is a compelling alternative: some of the comments suggest that there
may be a slight learning curve associated but that those that conquer that
seem to be highly satisfied.

#### Vendor

Miele is not available from Home Depot which is likely to incur higher
out-of-pocket expense.

#### Power

This unit unfortunately runs on lower voltage than the outlet provided. I'd
think that I could pick up a converter to reduce the voltage until having
an electrician in but that seems subtoptimal, and swapping the voltage to
what seems less common for heavy applicances also gives me pause.
Upon paying closer attention, however, it is shown that Miele provides
an appropriate adapter to address this concern.

### Samsung

An option listed but which was too new to review was a Samsung heat pump model.
While I have the unfounded sense that some of the other companies have more
solid reputations for heavier appliances I've had a solid experience with
Samsung electronics which typically seem to be high value in particular,
so this option seems worth considering.

#### Vendor

While not linked from Wirecutter (and therefore regretably likely not generating
them a kickback), the Samsung models do seem available from Home Depot so
it starts on a good footing.

#### Depth

Unfortunately this dryer has a decent depth and it is uncertain whether the
dryer would fit with acceptable clearance within the space available between
the door of the closet and the rear wall.

### Other Options

Several other options were quickly scanned but most seemed to suffer from the
largely anticipated depth problem. A particularly promising option is the
newer Bosch 500 Heat Pump dryer. Unfortunately there are a mix of unresolved
reviews (largely from the Bosch site itself) concerning whether the dryer
dries sufficiently. Given the novelty of heat pumps within Bosch dryers
it seems unproven enough to give me some trepidation.

## Selection

After poking around for quite a while I'm largely back at an updated version of
where I started; the newer Miele W1/T1 is available from a local appliance vendor
whereas the Bosch 500 heat pump model could be ordered from Home Depot
[@bosch-500-dryer-home-depot].
After watching a video from Yale (another local appliance vendor) which speaks
to Bosch's higher simplicity and reliability I'm going to explore the installation
requirements for the newer Bosch. 

### Installation Preparation

The Home Depot includes some installation preparation guidance
[@home-depot-appliance-delivery-installation] but it unfortunately
seems too general to be useful. While some of the information
is universally applicable there also seems to be a fair amount
that may be misleading depending on the specifics of the unit or
site involved. It seems as though a fair amount of this could be
better integrated into the checkout process with benefit to
the purchaser in the form of more specific information and benefit for
the business through promoting purchase of more potentially required
supplies. Instead I'll need to largely rely on the installation guides
for the purchased units where such guides are on the product pages
(albeit buried a bit in the middle).

#### Size

The depth of the unit base is less that 24" based on a diagram in the
specifications[@bosch-wtw87nh1uc-specifications] which indicates the
depth of the top is $23\frac{3}{4}''$ which
includes a lip that extends past the base at the back (it looks to
be $\frac{5}{16}''$ but that measurement seems suspect).

#### Power

While the general instructions reference the need for a standard
3-proing 110V outlet, the dryer's manual[@bosch-wtw87nh1uc-manual]
indicates that it includes an adapter such that the washer and
dryer can share a 4-prong 240V (which is present in the laundry closet).
