---
title: Road to Rust
---

- [Home](./index.html)

A little while ago I took some time to look into Rust which felt a bit overdue.
It turned out to be a language that has tremendous immediate appeal to me, moreso
than I can remember any _language_ doing. There have certainly been mentalities and
technologies that have grabbed me and languages that have grown on me (often quickly),
but Rust's offerings promise to align very tidily with preferences and perspectives
I've acquired. I'm therefore an aspiring rustacean and have started my journey by
brushing up on C.

# The Scenic Route

While C and Rust are both suitable for systems programming the decision is certainly
non sequitur and the efficiency of adopting Rust by way of C is questionable at best.
This is largely a reflection of a pursuit which started years ago while I was on
paternity leave and is now resurrected through a confluence of factors including the
resets provided by the COVID-19 pandemic. Interest in Rust has renewed my general
interest in building and/or having deeper understanding of most of the software that
I use. This is part of larger thoughts that need more substantiation but largely
circle around the notion of the diminishing returns associated with how technology
is typically used. As C is the dominant language in a GNU/Linux system and specifically
the implementation language of the system libraries it seemed the best place to start.
I also somewhat stumbled back to C through quickly trying out the Ox editor which led
me to the snaptoken kilo tutorial[@snaptoken-kilo] which in turn led me to dust off
my pursuit of writing my own [Ekstra](./ekstra.html) text editor. Ekstra will be my
primary guinea pig for this journey and will therefore hopefully evolve fairly
signficantly.

## Dipping Down

Incidentally while working with C I also ended up getting pulled into some issues
around object file formats which has led me to poke around at assembly a bit more.
Previously I've spent just enough time with assembly to get a feel for the mechanics
but I don't know if I've previously actually built anything (though that was largely
a result of reading material for x86 while on an amd64 machine). It's unlikely I'll
have a reason to spend a significant amount of time with assembly this time around
either but I'll at least increase familiarity and maybe look at stating the track
on exercism.io.

## Going Wide

A fair amount of the software I use also makes heavy use of C++ so that has become my
next topic. C++ is a fairly large language; I wrote a bit of it many (many) years ago and
almost certainly very poorly. I'm fairly familiar with much of what it has to
offer on a conceptual level but currently highly unfamiliar with practical details
such that I can typically infer at a high level what C++ code is doing but am likely
to be lost in the trees. As will likely be documented elsewhere I am likely to write
C constructs that aim to recreate some of the structure and behavior of other langauges;
C++ promises a vehicle to deliver many such constructs in a more natural and idiomatic
package. The combination of these factors and the relative void that I have regarding
C++ experience compared to the relative demand are motivations to take a detour down
that path.

## Arriving

After C++ I'll get back to Rust with the intent of having all of the systems
play well together at the end and likely gradually porting the other two to Rust.
There may be an additional detour since I'm likely to introduce an extension language
and so I may spend some time with whatever that turns out to be (like Scheme through
GNU Guile since the split of front and back-ends appeals to me).
Throughout I'm likely to primarily focus on unidirectional calls such that with edges
of Rust -> C++ -> C each language involved would call code in languages to the direction
of the arrows but not the other way around.
