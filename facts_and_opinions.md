---
title: Facts and Opinions
---

- [Assorted Thoughts](./assorted_thoughts.html)

> Everyone is entitled to his own opinion, but not his own facts.
>  -- Daniel Patrick Moynihan[@dp_moynihan_wikiquote]

While I fully appreciate the sentiment of the above quote and its
attempts to cudgel ignorance, I also worry
that some of the message is insidiously counter-productive (more
provocatively it also raises the question of whether American
attitudes towards religious freedom rather than reason imply that the
opposite is actually true). Lurking within the quote is the suggestion
that statements are either facts or opinions and the objective
veracity of any statement is a property of that category. This tends
to produce perverse polarization of largely objective but
_somewhat_ unresolved issues such as climate change.

A closely related notion is that of "scientific fact". Within the
context of science "fact" does not carry the designation of immutable
truth present in common
interpretation[@ncse-definitions-fact-theory-law], but to more clearly
express the distinctions herein "fact" will be used for statements
which less ambiguously possess that attribute. While science is
certainly full of facts, the phrase "scientific fact" is often used
for assertions which transcend (but are likely supported by) facts.  A
less leading term for such assertions which are accepted by the
scientific community is _settled science_. The difference may seem
subtle but is significant. The message of "scientific fact" is one of
irrefutability. This is a stance of dogma which is diamterically
opposed to that of science. Science is perhaps unfortunately often
considered a source of _answers_ but probably more than anything else
science is a framework for questions and skepticism: it is not a
message of "listen to me" but rather "bring it!" It's so itching for a
fight that it goes on to say "listen...I may have a bunch of reasons
to believe this but if you can give me one, just _one_, measly
reproducible counterexample you can have the win."

Science therefore provides a fairly clear context for the large middle
ground between fact and opinion. This is the space in which most of
our knowledge lies: hypotheses and theories composed on top of
falsifiable empiricism. In this space you can collect facts and
conjecture based on those facts but the merit of those assertions is
contingent upon the evidence supporting them. Everyone is certainly
entitled to make claims, but they are only worth as much as they are
able to be substantiated. This is the foundation of rational beliefs
and discourse which fosters our collective understanding of the world.

Maybe that's just my opinion, but it's also a roadmap for how to
change it since I don't feel entitled to believing things that are
disproven.