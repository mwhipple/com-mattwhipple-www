---
title: JavaScript
---

[Software](./software.html)

Like the majority of modern programmers I've sent a fair
amount of time writing JavaScript. Currently this is the
primary language I use professionally.

My most recent use of JavaScript (and TypeScript) has rekindled
an appreciation for the language. My initial outlook in JavaScript
from many years ago was largely SICP inspired (as was apparently
Brendan Eich's[@eich-popularity]) and I find combining that
style with Promises to
be a very natural and powerful fit for the event loop backed
runtimes typically used for JavaScript. This can produce very
elegant solutions on top of simple machinery. I'll start
capturing some of those ideas here.

However I'm still highly wary of the use of these platform
for general use due to how prominently accidental
control[@Moseley2006OutOT] must be incorporated into designs
to accomodate that simplicity. This concern is very often
neglected and is unlikely to align with any business value
the vast majority of the time. Indeed this is an issue
which is typically justifiably underserved but poses
fundamental risks. Certainly in cases where technology
warrants deep attention the simplicity and associated
tradeoffs can be recognized and embraced, but in cases
where the technology primarily serves as a vehicle for
other value the inherent complexity of the additional
control concerns are likely solely cost. Far too often
JavaScript and TypeScript are adopted for their
accessibility rather than their suitability without
proper appreciation for the latter.

[Node.js Traps](./node_js_traps.html)
