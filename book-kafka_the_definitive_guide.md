---
title: "Kafka: The Definitive Guide"
---

- [Books](./books.html)

---

I'm current immersed fairly deeply into systems which make heavy use
of Kafka, and so I've started to read some appropriate reasources
starting with the second edition of Kafka: The Definitive
Guide[@kafka-the-definitive-guide]. I read the first edition of the
book several years ago and remember it being very solid, so I was very
pleased that a new edition was available which would allow me to both
refresh and update some foundational Kafka knowledge.

As alluded to the book provides very good background to Kafka,
specifically to the breadth of information relevant for Kafka in terms
of operating and making use of Kafka. The diversity of aspects may
suggest that the book is _not_ the best resource for anyone looking to
do something in particular with Kafka; it seems likely to leave a fair
gap between the information provdied and the practical application of
efficiently administering or leveraging Kafka to produce a
solution. That is in no way a derogatory assessment but rather an
acknowledgement that the book remains focused on Kafka itself which is
a powerful and complex enough technology that covering it
appropriately in isolation is a significant enough undertaking and one
which would likely be diluted through overextension. There is an
abundance of supplementary resources within the ecosystem that are
likely to be more tailored for more specific or more nebulous needs.

This book provides a very good mix of fundamental concepts and
guidance along with some reference style material, and will therefore
act as a preferred source when looking for Kafka specific information.