---
title: Scripting Language
---

- [Software](./software.html)

While trying to adopt technologies from the metal
up I fairly quickly found myself wanting to select
a scripting language. While that term is used very
widely my framing for it seems to match the standard
definition[@scripting-language-wikipedia]; I'm looking
for a language which enables logic to be embedded
within configuration. Given a system
that contains well-defined components the scripting
language enables more flexible configuration of
those components. An alternate perspective is that
scripting languages provides imperative configuration
which can more economically absorb a range of needs
than extending a declarative format (where the boundaries
between the two are likely to get blurry after
heavy extension). In this default use as an extension
language the scripting language provides the
more flexible but less vetted and less supported complement
to the built system.

While extension is the primary goal, the use of a
scripting language can also promise faster development.
Such languages will therefore also typically
be used where appropriate for simple functionality or
for protypical development of more advanced systems.

Given the technologies that I'm currently using there
are a handful of obvious choices for scripting languages.
I'll almost certainly be diving into many of these over
time but will be adopting them iteratively and the status
will be captured here.

JavaScript/ECMAScript is potentially the initial choice
since it's very popular and what I'm currently coding
professionally. This is a language I've found renewed
appreciation for, but primarily as it offers a more
palatable vehicle for concepts from other languages.
Integrating JS as an extension language doesn't lie
naturally down the track of solutions I'm currently
venturing, so this may be incidentally pulled in as
a front-end for GNU Guile but is unlikely to be a
first-class pursuit.

Scheme through GNU Guile is a very compelling alternative.
While support for more imperative extension can keep
options option, declarative solutions are typically
more readable and maintainable. The homoiconicity
along with macros of Lisp enable easy construction of
declarative elements while enabling imperative
deparatures where needed. Many other languages provide
comparable support for the creation of small embedded
DSLs but those are typically an imperative subset
whereas Lisp enables the data structure to be the
primary construct. Scheme itself is a very elegant
language and its use as an extension language would
likely be provided through GNU Guile which is a
compelling alternative with support for multiple
language frontends (though Scheme is the primary).

Lua is a _very_ popular choice as it is a small
codebase that was designed for the purpose of
being an extension language. I spent some time
learning Lua several years ago while exploring using
it within nginx, and it grew on me. The pattern
of using constructors to provide a data definition
is particularly appealing to me. There are potential
perceived drawbacks in that it does not seem
generally popular and I think I remember some
coworkers turning up their noses at the 1-based indexing.
This is in use in several of the projects I'm
planning on spending some quality time with, and is
therefore likely to be embraced in some capacity or other.

Python is arguably the most versatile higher level
language in use today; potentially to a fault as
it contains segmented ecosystems. Given Python's
prominent role in areas such as data science I'd
likely gravitate towards Python as my default choice,
as it can not only provide local extension but can
also act as vessel for such specialized functionality.
Python has declarative and functional leanings and
its use within fields such as machine learning
have inspired efforts to optimize Python for
more complex needs.

Bash is certainly one of the most used even if
not always recognized scripting and glue languages.
Bash provides a standard and key component in the
UNIX pipes and filters interface which enables
the filters to remain more focused software tools.
It has a fair amount of quirks many of which seem
attributable to the fact that it is designed to support
natural execution of other commands via the same
interface through which programming instructions are
passed (along with a fair amount of legacy cruft), but
once that hurdle is passed the underlying power can
be readily harnessed. Due to the integral role bash
plays in POSIX systems, it will be the scripting
language with which I start. While it seems unlikey
that bash will be used as an extension language inside
of other software, it will be used externally with
the operating system and itself providing assorted
means to deliver equivalent functionality.

