---
title: Make Recipe - Check Command Versions
---

- [Make](./make.html)

# Background

In addition to checking that required commands are installed,
it is also often useful to verify that a supported version
is being used. The most obvious scenario where this is relevant
is assertion that the version installed on a system is sufficient
for use, but the case which is likely to be far more frequent in
practice is around selection of one of many installed versions.

A common means to manage required versions is to use some form
of version management tool so that different projects can specify
versions which may vary from the system-wide installation, typically
provided by some tool with a technology specific prefix and ending
in something like "vm" to indicate version manager or "env" to
indicate managing a controlled environment. These tools are very
useful but similar to the need to have the underlying commands themselves
this often leaves a self-referential gap in that the tools need to do
their part prior to invoking the managed code.

Validating the versions in Make allows for more targeted feedback to
be conveyed by the project itself, without assuming conventions or
system configurations which may drift over time. This allows for
clear guidance on what steps should be taken to be sure that the build
proceeds as expected rather than potentially failing with arcane
failures or relying on past implicit knowledge.

Make can additionally invoke the version manager itself which may
present a simpler alternative, but I'd likely view that as more
of a convenience layered on top of this more essential assertion.

# Approach

While validating system-wide installation could likely be done one
time and recorded in a file, multiple versions should likely not
use such a shared resource as different processed may be running
different versions. Therefore a false positive could present
if a process using an invalid version stat's a file created by
another process with a valid version. It therefore seems preferable
to keep the results of such validation in memory.

# Recipe

The pattern of caching used in [required commands](./make_required_commands.html)
will be used for the efficiency outlined in that recipe
(extraction of that behavior will be considered later).
To faciliate this the first parameter will be the _name_ of
the command whose version is being tested which will be composed
with the _version_ (which will be the second parameter)
to produce the cache key.

The version check itself will be primarily handled by the
sort command[@sort-man]. The key features provided by sort are that it
supports `--version-sort` and that it can return a status based
on whether the input is properly sorted (`--check`, with `=quiet` to
only get the status).

Validating that a version is at least equal
to a desired version can therefore be done by piping the current
version and the desired version as inputs into sort which will validate
that they are in sorted order. This can be done using the shell
`printf` built-in.

While the exit status is great conceptually, the Make functions involved
are checking for whether there is content and therefore to satisfy
the control flow within Make this is going to negate the antecedent
of the silence is golden rule and attempt to only communicate success
by using the shell provided `&&` short-circuiting conjunction.
This seems less reliable since any other feedback could get in the
way so there may be some redirections needed, but it seems to work
in practice so those concerns can be addressed if and when they arise.

The command to retrieve the program version will be passed as the
third parameter. Any filtering or similar required to extract a
version number comparable to that which is passed as the second
parameter should be included in that last parameter.

~~~{.Makefile}
require-version-gte = $(or ${_${1}_gte_${2}},                      \
	$(eval _${1}_gte_${2}=$(shell printf "${2}\n%s\n" $$(${3}) \
	| sort --check=quiet --version-sort                        \
	&& echo 'Version validated')),                             \
	${_${1}_gte_${2}},                                         \
	$(error ${1} version must be at least ${2}))
~~~
