writing bash scripts. Bash is particularly susceptible to being
exploited due to the ways in which it interacts with its environment,
and therefore it is important to make sure such interactions are
done safely; there are many resources available for writing secure
shell scripts.

This is currently not a personal priority as I primarily use bash
with a tight scope on single user systems. In the case of any scripts
that are used on a multi-user system or for performing system adminstration
tasks, security should be given far more attention. My personal use will
likely evolve to include such practices as I need to worry about them
or as I absorb them from elsewhere and they become habit.

**********
Foundation
**********

As will be described elsewhere, I prefer to adopt established and
documented styles where appropriate. The basis for my bash style is
the Shell Style Guide from Google :cite:`BashGoogleStyleGuide`.

=========================
Use ``set -euo pipefail``
========================= 

set -e
------

``set -e`` is used by default, causing the script to fail
for any uncaught errors. This makes sure that any
unexpected failures result in obvious, proximal errors
rather than potentially leading to more mysterious downstream
errors. This also leads to clearer error handling in that,
due to the above, unexpected errors do *not* need to be
handled for the failure to be evident,
and therefore any error handling present reflects
expected scenarios. The price of having to handle
unimportant unsuccessful calls is minimal, as the command
can just be wrapped in some form of branch.

For example:

.. code-block:: bash

   # Should always succeed
   user=$(whoami)
   # Failure is okay...not sure if this is a valid command.
   $(fortune | mail -s 'MOTD' $user) || echo 'Cannot send mail!'

Not using ``set -e`` invites the possibility of moving in the
opposite direction where errors may be handled solely
for the purpose of surfacing them even if they are
exceptional while some expected failures may be buried
in implicit handling. The previous example could end up as:

.. code-block:: bash

   # Abort here to head off later issues.
   user=$(whoami) || { echo 'whoami failed somehow'; exit 1 }
   # This could fail, but the command hides that.
   fortune | mail -s 'MOTD' $user

set -uo pipefail
----------------

``set -u`` and ``set -o pipefail`` are both used in addition to 
``set -e`` which combine to form the invocation at the start of this
section. These are less likely to warrant less explanation beyond
their obvious behavior, however: largely acting to enforce additional
strictness. Working with ``set -u`` is also discussed in `bash_var_presence`.

=====================
Declare All Variables
=====================

Identifying ``local`` and ``readonly`` variables is a
common best practice, but bash scripts are also likely
to rely on globalish variables that either stem from
an external source (such as environment variables)
or are used to coordinate state (particularly due to
the limitations around function return values in bash).
All variables will be declared explicitly in one form
or another so that they can be appropriately documented
and identified. Often they act as coordinators and so
should not be incorrectly lost as subordinates; they
should consequently be declared and documented in an
appropriate scope (such as top-level)
rather than being incidentally
part of the documentation for the places of access
(such as across the documentation for specific functions).

This also provides a convenient location to specify any
default values for shared variables. Bash supports default values
through some of the forms of parameter expansion so a locally
used variable can easily be given a default by having an initial
expansion in the style of ``${VAR:=default}``, but for variables
which are non-local and therefore may not have a clear first access
this can result in that default value being buried or scattered.
An alternate common practice is to use such a construct as a
standalone line in a form such as:

.. code-block:: bash

   : ${VAR:=default}

where the ``:`` serves to allow the rest of the expression
to be valid without failing when trying to invoke a command.
This unfortunately trades away clarity for the sake of
brevity. The declaration of a variable provides a natural and
expressive location to define a default with a statement such as:

.. code-block:: bash

   declare VAR=${VAR:-default}

========
Be Quiet
========

As a standard practice in the software tools methodology
:cite:`BashClassicShellScripting`, scripts should not
be chatty when they are operating successfully. Often any
deeper visibility into a script's operation can be enabled
by a user through ``set -x``, but in cases
where more controlled output is added it should be
predicated by a flag (which defaults to off) and sent to
sterr.

.. _bash_var_presence:

=======================================================
Use ``-`` Parameter Expansion to Test Variable Presence
=======================================================

Whn using ``set -u`` then it becomes an
error to test against an undefined variable and
therefore a test such as ``[[ -n "${POSSIBLY_UNUSED}" ]]``
would fail if the variable has not been defined.
To avoid this possibility the test should use the
parameter expansion ``${POSSIBLY_UNUSED-}`` which will
expand to the empty string if the variable is unset
and therefore avoid the issue. This expansion seems
fairly under-documented, but expansions which omit the ``:``
differs from those that include it in that they
handle the unset, but not the null case [BashClassicShellScripting]_.
This practice was stolen from other shell scripts such
as git completion.

==================================
Prefer Successful Short-Circuiting
==================================

Short conditional expressions can be handled in bash through the use
of ``&&`` and ``||`` with an example being something like:

.. code-block:: bash

   [[ -n "${SOME_FLAG}" ]] && handle_flag

This form nicely expresses the intent that if the flag
is non-empty it should trigger some bit of code. There is
an issue, however, in that if the flag is _not_ set the
second clause is skipped because the ``&&`` short-circuits
with a false value which leads to that false value being
returned for the status of the entire expression. This
normally indicates some type of failure in bash, and if
``set -e`` is used this will cause the script to exit
unsuccessfully.

Where it is preferred to retain this shorter style the
expression should yield success, so the above should
instead be written as:

.. code-block:: bash

   [[ -z "${SOME_FLAG}" ]] || handle_flag

which successfully short-circuits. In case where
this hampers the readability of the code ``if`` should
be used instead (among other cases for which ``if`` may
be more readable/maintainable).

