---
title: Assorted Thoughts - Matt Whipple
---

- [Home](./index.html)

---

I tend to concoct and refine a lot of ideas about different things
and such thoughts will be captured here. Most of these should be either
supported or challenged (or destroyed...or something else) by external
sources of greater clout.

- [Design Principles are For Messes](./design_principles_are_for_messes.html)
- [Documentation is For Change](./documentation_is_for_change.html)
- [Facts and Opinions](./facts_and_opinions.html)
- [Immaterial Implications](./immaterial_implications.html)
- [Overengineering](./overengineering.html)
- [Three Types of Engineering Questions](./three_types_of_engineering_questions.html)