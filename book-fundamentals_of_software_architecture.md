---
title: Fundamentals of Software Architecture
---

- [Books](./books.html)

---

I'm currently working as a software architect on an organization that
has a distinct and healthy positioning of architects (compared to
other organizations where architect is effectively an engineer level
or others where architects define mandates). I figured I should level
set my take on the role with that of the wider industry and so I read
through Fundamentals of Software
Architecture[@fundamentals-of-software-architecture] to make sure I
was focusing on the right areas and to inform any possible
adjustments.

The book offers a good survey of concerns to watch as an architect,
including a solid definition at the often nebulous concept itself. I
have my own compatible take which I'll work on documenting here at
some point. Probably the most valuable immediate concepts is that of
validating architectural characteristics with fitness functions and
using quanta to keep the relevant number of such characteristics small
(I certainly found myself thinking I would make a prioritized list
before reading the advice that that idea was a trap). 

Likely the main takeway is the mentioned _survey_ in that I feel as
though this book provides a decent catlogging of materials which are
more comprehensively covered elsewhere. I certainly picked up some new
ideas that I was able to apply coincidentally readily apply and will
likely come back to this book for a refresher but will more likely use
it to guide me towards some of the referenced materials in search of
more depth. There were occasional editing/grammatical issues that were
mildly surprising (though I'm sure far less than this site suffers
from so I'm likely a pot to that kettle). I found the referencing of
several Clojure tools personally amusing as I recall some
conversations at an SACon several years ago where Neal was expressing
his fondness for Clojure.

Overall it largely affirmed and clarified many of my conceptions going
in, but armed me with additional context and vocabulary to tackle
defining my own role and making sure I continue to mature into it.