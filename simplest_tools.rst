##############
Simplest Tools
##############

A design idea that I've increasingly adopted is trying to use the
simplest tool for a job. This somewhat an extension to the KISS
principle and one I've somewhat embraced in life in general but
for now this will focus on how this idea can be applied to computer software.

A general challenge with this pursuit is that absolute simplicity must be
balanced against the relative simplicity given any surrounding context.
If more powerful yet more complex tools are being used for
proximal work then it is natural to use those tools for ancillary uses
beyond those for which they are primarily intended. Often this is a
sane approach, but (like anything) it can have unintended drawbacks.

One such drawback is that when the solution is isolated from the surrounding
context the more powerful tools and the associated complexity are no longer
justified. This is very often noticeable when the solution involved is supporting,
but is largely independent of, the core pieces of a project. Some examples include
things such as support utilities that are invoked outside of the main system or
concerns which ultimately lay entirely outside of runtime such as build tooling.
A concrete example of such an independent concern is the functionality provided
by the `brightcove/buildsrc <https://github.com/brightcove/buildsrc>`_ project
which is focused on behavior which largely target common utilities such as ``git``.
Such utilities are unlikely to be used within the core solution and conversely the
utilities themselves are unlikely to be concerned with the details of that solution,
therefore although the pieces may be proximal their roles are mostly disjoint.

Like so many design goals in software, the purpose of this principle is to
assist in acquiring understanding of the system for anyone that doesn't have it...
which is likely to be yourself after several months have passed. It can be
natural to solve these types of problems using the tools that are already in hand,
but I've certainly cursed my past self several times when I've returned to an
old project and have needed to invest time in refreshing knowledge about tools
that don't provide any notable value for the specific task I'm looking to perform;
they often introduce significant overhead and conventions for what is a
superficial alignment with the rest of the project. Further, they can also
increase maintenance costs (as libraries evolve) without any corresponding 
practical value.

By working towards using the simplest tools regardless of incidental context
the overhead for understanding a separable piece can be minimized, and that
can in turn yield easier maintenance and increased reusability for that piece.
