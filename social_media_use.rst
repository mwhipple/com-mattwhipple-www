
################
Social Media Use
################

This draws on several sources which I'll be collecting and referencing.

I've never been a heavy user of social media: in the past I've normally
engaged with it for short bursts which have then fizzled. This has likely
been because I primarily prefer to use the Internet as a resource which I
explicitly engage with for discrete purposes whereas much of social media
is more biased towards a continuous use which is often driven by the
platform engaging with the users. The mode and direction of the interaction
therefore often do not fit how I prefer to use the Internet.

.. todo: Reference Rushkopf, read biases

I've also decided to adopt a very intentional approach to interacting with
social media so that I may regain control over what information I am provided.
I look for the Internet to provide me a breadth of somewhat democratized
information, yet many of the systems which are normally used on the Internet
deliver streams of information which are likely to progresssively narrow.
To combat this tendency I look to use tools which don't do that, and to use
tools that do in ways that will not constrain the delivered information in a
way that effectively fits blinders onto the stream that is presented to me.

*************************
No Malice Nor Self-Import
*************************

One aspect which is worth calling out initially is that these perspectives
do not attempt to imply any malicious intent or any form of conspiracy.
If anything it is the opposite extreme where things are reduced to the point
of triviality; the potentially undesirable side effects are likely an indicidental
result of a resulting lack of significance rather than having any
conscious undermining of any values.

For example: much of the ideas expressed herein also align with privacy concerns,
which is something else that interests me and of which I support protection but for
which I don't have enough background knowledge to speak to properly. Anecdotal
common arguments used to dismiss such privacy concerns are that those concerned
should have nothing to hide or have inflated sense of self-importance. This crux
of the perspective on this page is a continuation of the latter sentiment: in
general not only am I (or any indivdiual likely to be unimportant but I am also
likely to be no more than some mostly indiscriminate data points captured in bits
somewhere. But...beyond that I'm likely to not even be a *unique* data point,
I'm likely to have some a couple stereotypical personas attached to me based on
some scores and I'm likely to be one of a swarm of such entities with the same
personas attached. So in other words (contrary to the desire to claim some kind
of human identity) I'm likely not only be unimportant but also non-unique and
uninteresting. I'm a parameter.

********************
Identify Motivations
********************

As previously outlined the asserted position is that some concerns may be drowned
out and lost in other pursuits, and so it is worth identifying what goals primarily
drive how things work. For any service this will be very closely tied with how they
make a profit, or at least offset costs. Many of the services on the Internet are
"free" to use, but anything at scale will have associated costs that need to be
paid in some way or another. If the service is a core offering from a profit-seeking
company then most likely they are monetizing your use in some way or another.

How use translates to revenue (or not, if the service is donation driven)
can normally be inferred through paying attention to things like EULAs and
Privacy Policies and doing some basic research. But more fundamentally if
use *does* translate to revenue then there is an immediate motivation to
increase use. Free services are therefore likely to have a primary motivation
of keeping users engaged and returning to the service which is likely to extend
above and beyond the benefit the service is providing. In some cases there may
be a natural conflict between those two forces, as if a service delivers utility
*too* efficiently then the time using the service itself is likely to be too low
to maximize the opportunity to monetize the engagement (though if the experience
is poor it will drive users away and therefore there is likely a pursued sweet spot).

An alternative perspective is to view this through a basic capitalistic lens.
Businesses can be reduced to consumers giving money to vendors,
but in the case of using free for-profit services it is worth thinking about your
role. If you are not giving money you are not the consumer and therefore someone
else is; if your use is still being monetized then it is ultimately a product which
is being sold to someone else.

***********
The Problem
***********

The specific concern I have with social media (and some other services) is that
the information presented to me is likely to be filtered through algorithms which
tailor the information for me. In the simplest case this amounts to decisions
being made for me and the likely scenario that I'll be increasingly confined
within an information silo. Rather than being presented a wide array of information
which I can refine as desired, filtering will be done beforehand and therefore I'm
likely to become increasingly unaware of the options that have been filtered out,
losing the knowledge of their existence let alone the capacity to consume them.
I strive to be rational but I'm also a human and so my interactions are likely
to be nudged by cognitive biases and emotions and are also those factors that
are most likely to feed the type of engagement desired for monetization
(rationality would lead to the pursuit of less profitable utility).
Over time this can lead to what amounts to self-censorship driven by algorithms
which reinforce a preferred view of the world (not to be confused with a utopian
view: many of the strongest and most emotional views will be negative).

.. todo: Reference the great hack

A more complex factor is that these platforms are likely to generate revenue
from targeted marketing. As covered in the removal of any self-importance, I'm
not likely to be more than some dervied sterotypical personality and therefore
marketing could be mapped fairly easily to that digital identity. Such marketing
could aim to manipulate those biases and emotions that can arrest rationality and
perversely attempt to tap into my sense of identity.

These issues are particularly pernicious on social media as information silos
are likely to be fortified through sharing amongst friends and family. This fits
neatly into motivations of the social media platform itself as it increases engagement,
but can devastate the promise of the Internet being able to democratize information.

******
My Use
******

The underlying issue with the concerns above is  there control over what
information is proivded and what decisons are made are incrementally ceded to the
technology used. The solution is therefore to be aware that this is a possibility
and make conscious decisions about what types of information should flow through which
channels. Any classes of information for which a breadth of balanced information is
desired (such as those for which any bias is likely to lead to one or more forms of
ignorance) should not be subjected to algorithmic curation.

The level of personalization that is introduced in tools like social networks can be
useful in fostering *subjective* connections, but the inherent bias introduced to
maximize engagement within those connections disrupts the ability to deliver
accurate external information through the same channels. Objective information should
be accessed or verified through channels which are either unbiased or which readily 
expose what alterations are being applied to the information provided so that they
do not represent lurking variables in your resulting world view (and could ideally be
modified or removed).

Acknowledging my limitations and concepts such as the third-person effect I also
don't consider myself immune to more targeted manipulation even if I work to
consciously ignore content that may appear dangerous in that light. Therefore rather
than attempting to safeguard against or correct for how the platform may learn from
actions I may take related to external data I'll abstain from interacting with such
content this.

The ultimate result of this approach is that my actions on any social media platform
will reflect a personality which has been intentionally adopted in light of these
concerns. Interactions are likely to remain superficial or scoped to a controlled
realm of information for which the algorithmic curation of data is expected and not
undesired.
