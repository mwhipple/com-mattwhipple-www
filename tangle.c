#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int has_file(char* line, char* name, size_t name_len);
int
main(int argc, char** argv) {
  if (argc != 2) return EXIT_FAILURE;
  char* filename = *(argv + 1);
  size_t file_attribute_len = strlen(filename) + 5;
  char* file_attribute = malloc(file_attribute_len + 1);
  sprintf(file_attribute, "file=%s", filename);

  int in_output_mode = 0;
  char* line = NULL;
  size_t len = 0;

  while (getline(&line, &len, stdin) != -1) {
    if (in_output_mode) {
      if (!strncmp(line, "~~~", 3)) in_output_mode = 0;
      else printf("%s", line);
      continue;
    }

    if (!strncmp(line, "~~~", 3) &&
        has_file(line, file_attribute, file_attribute_len)) in_output_mode = 1;
  }

  return EXIT_SUCCESS;
}
int
has_file(char* line, char* name, size_t name_len) {
  char* found = strstr(line, name);
  if (found == NULL) return 0;
  switch(*(found + name_len)) {
    case '\0':
    case ' ':
    case '}': return 1;
    default: return 0;
  } 
}
