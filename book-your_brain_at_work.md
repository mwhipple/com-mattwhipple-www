---
title: Your Brain at Work
---

- [Books](./books.html)

---

David Rock's Your Brain at Work[@your-brain-at-work] is an easy
book to recommend. I've spent a fair amount of time thinking about my
thinking (metacognating) but have not spent much time learning about
much of the science behind how it works. While I've maintained what is
likely a higher than average awareness of how limited and
untrustworthy my senses and many functions of my brain are I've
largely given my prefrontal cortex a pass from such scrutiny, likely
spurred on by my profiled strengths of a growth mindset and cognitive
agility.

From the perspective of being a software engineer one of the largest
takeaways from this book is how little can be easily actively
processed at a time. For several years now I've tried to actively
combat cognitive overhead at work; while often teams adopt
practices and designs that require a fair amount of investment to get
up to speed I've lost desire to dig deeply into non-portable
knoweldge and instead evangelize modularized simple concept along with
documentation. While I've been on the fence as to whether this
qualified as the good or the bad kind of being lazy, this book
substantiates that those approaches map more neatly onto how brains
work generally rather than just my own (using approaches like chunking
and explicit information to keep the consumed precious mental
registers at a minimum).

Beyond limitations and strategies for optimizing working within them,
another crucial area where I've potentially hoped for reason to be
able to triumph over biology is in relation to some of the chemical
reactions as touched on with SCARF model. The prospect of certainty
being a dopamine trigger is particularly interesting given that I
often find myself on the side of trying to prosthelytize critical
thought when it oten counters perceieved certainty.

Another thread which I find particularly relevant now is in terms of
influence and coaching which should involve a synthesis of all of the
above ideas along with other materials I've been reading. From a
personal perspective I should certainly actively bolster my
relatability since I've fallen into a mindset of being at
work to work and increasingly working remotely isn't likely to help
efforts to more quickly tip the scales from foe to friend. More
generally the information seems highly useful as I'm looking to
introduce more coaching rather than mentoring into my role and
continue to expand and focus my impact.