---
title: You Can't be Netural on a Moving Train
---

- [Books](./books.html)

Howard Zinn's "You Can't be Netural on a Moving Train"[@zinn2018you] came
on to my radar because of a Netflix documentary I watched about him several
years ago (of the same name?). The title phrase is one that I very much like
and is one I'd like to quote and so it seemed as though I should make sure I
had sufficient background to quote it with appropriate understanding.
The book itself was fairly inspiring and enlightening and
certainly makes me feel like I should be doing more with my life.

Zinn's voice as a historian seems to me to help him present himself in an
appropriate role with treading into great white hope territory during much
of the discussion of the civil rights movement (though as
a white man I'm not a suitable judge of how well that line was held). Also
supporting this healthy perspective is one of the core themes that change is
enacted through the collective actions of a multitude of individuals and is
often marked through a series of results that may seem to be failures. This
thesis is very compelling to me as for mysterious reasons there seems to be
a tendency to attach simplistic and often superhuman merit to a select few
even though empirical and recorded evidence indicates otherwise and most often
the realities may be less fantastic but seem to me to normally be far more
interesting. This viewpoint alone leaves my highly inclined to read more of
his work.

Most of the sentiments he expressed resonated with me and clarified many of
my own trains of thought. It also left me with the realization that I am
ideologically likely a radical rather than the previously perceived liberal.
Ideologically is regretably an operative word as I currently
do very little in the way of action. There is a preverse tendency for people
to believe that a system that has brought a particular state into being somehow
inherently offers the antidote for that state. This too often disregards the
often active role mechanisms of the system may have played in convergence
while also trusting that somehow those that receive or perceive benefit from
the status quo would not be driven by some combination of complacency, risk
aversion, and a desire to preserve such benefits. As such systems mature so
can the machinery of self-preservation within such systems, stymieing change
even in the face of egregious deficiency. It also seems essential to recognize
that such entrenchment may or may not be fortified through conscious thoughts
or attitudes but are likely to be a natural and often covert consequence of
assorted incentives.

The central message of optimism and empowerment is a very welcome one. It
certainly seems to be manifest in the longer term trends in spite of how
grotesque some specifics can be. As some of the factors that have kept me
somewhat of a homebody subside hopefully this can provide some fuel to
engage me in helping foster worthwhile changes.
