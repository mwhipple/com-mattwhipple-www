---
title: Linux Configuration
---

- [Software](./software.html)

---

Configuration of much of Linux is fairly straightforward nowadays but
seems worth documenting since (for me at least) it is not
implicit. While many other devices and their operating systems are
likely to be pre-configured or designed for automatic configuration my
use of Linux tends towards _some_ level of explicit configuration.
Ultimately this is something that I embrace as it builds understanding
and aids in any troubleshooting where any issues can be very
intentionally worked through rather than stumbling around somewhat
blindly due to incompatible magic.

- [Linux Audio](./linux_audio.html)
- [Linux Bluetooth](./linux_bluetooth.html)
- [Linux Printing](./linux_printing.html)
- [Linux Touchscreen Support](./linux_touchscreen_support.html)
- [Linux Webcam](./linux_webcam.html)