#+TITLE: Coding - Matt Whipple

#+INCLUDE: "./nav_main.org"

While "coding" could certainly apply to a range of activities within
and without computer software, here I'm using it to apply to the
specific task of applying and analyzing algorithms and data structures
to solve a particular problem. This is distinct from concerns such as
design which somewhat be reduced battling entropy, or more trivial
programming tasks which amount to selecting a relatively obvious
approach and implementing it using adopted technologies. While these
skills are warranted fairly infrequently in most devlopment jobs
keeping them sharp is a helpful way to exercise engineering muscles,
and can also be fun. Here I'll track some of my working on my coding
skills.

* Languages

As will be covered elsewhere, I'm fairly indifferent about languages
so I'm likely to bounce between different languages. I'll use coding
as a means to tinker with and brush up on different languages.

** Elisp
   
As I'm currently investing a bit in emacs, an appropriate language to
start coding with is emacs lisp. I'll first be working through the
track provided on Exercism.

* Practice Sites

There are several sites around which provide coding exercises along
with a platform on which solutions can be attempted.

** Exercism[cite:exercism]

Exercism is a site that is more oriented towards languages than
problems, and has support for a wide range of languages. It does
not presently provide interactive editing and execution which likely
lowers the overhead involved in supporting a given language.
I was first introduced to exercism by a coworker where we used the
teams edition during a session of a company "book club" when we took
a break from reading books and instead spent some time exploring a
language (Elixir) as a group. Compared to other coding sites,
Exercism is more focused on growth through collaboration and mentorship
as opposed to a more competitive/interview-y slant. The platform itself
is free and open-source.
   
* Sources

bibliography:refs.bib
