---
title: Conscious Consumerism
---

- [Home](./index.html)

I strive to be very intentional about the business that I
support and act as a conscious consumer[@nytimes-conscious-consumer].
While I do not delude myself as to the impact these choices
make I do think it's an essential but selectively ignored tenet of
a free enterprise system.  The potentially limited scope of impact
should also consider aspects such as economic activity
multipliers and an immediate doubling in cases where one merchant
is preferred over another (positively impacting one and withholding
that benefit from the other).

# Minimalism

I certainly have strong minimalistic tendencies: the only goods I
regularly purchase are those that are consumed (i.e. food and drink).
The total footprint of my _stuff_ is likely to shrink rather than grow.
Unfortunately the notion that reduce, reuse, and recycle are in priority
order[@waste-hierarchy-wikipedia] seems to often get buried in America;
recycling is typically the most promoted practice and occasionally "reduce"
seems to be given alternative interpretations that would be less likely
to stymie rampant consumerism. In addition to the potential ecological
advantages to purchasing less and minimizing the waste/packagaging associated
with that which is produced it also seems likely to encourage a healthier
outlook in terms of material satisfication.

As in virtually everything there is likely a large gray area that at least
needs further discovery. Some additional questions I need to probe more
deeply are the consequences of actively supporting sustainably sourced business
as opposed to lower overall consumption of products which may have less
eco-friendly manufacturing processes. The most pressing example in my mind
would be the decision between something like paper books vs. electronics.
Paper is tied to a renewable resource and supporting sustainable forestry
initiatives create direct economic incentive for forests to exist whereas
electronics typically rely on mining of rare metals for production and
introduce dealing with hazardous materials and costly complexity to
waste management[@10.1145/3398390]. The end-to-end story
of corporate incentives, manufacturing, and shipping considerations all
contribute to some answers being far from obvious, but often there are
fairly clear signals to follow.

# Locality

I strive to support local businesses. This fosters local economic activity
and combats issues around wealth consolidation. While such activity is unlikely
to directly impact my income it seems very likely to benefit the community
in which I live and am raising a family. This may have other benefits in terms
of environmental impact though in many cases that seems less clear.
One instance where that _does_ seem clear and also has implications in terms
of quality is around purchasing locally grown food: I am an aspiring locavore
and seek out locally sourced foods and those companies that make use of them.
