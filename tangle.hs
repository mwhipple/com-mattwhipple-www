{-# LANGUAGE OverloadedStrings #-}

import Data.Text (pack)
import Text.Pandoc.JSON

main :: IO ()
main = toJSONFilter tangle

tangle :: [String] -> Block -> Block

tangle args (CodeBlock att contents)
    | matchesFile args att = (CodeBlock att contents)
    | otherwise = Null

tangle _ _ = Null

matchesFile :: [String] -> Attr -> Bool

matchesFile []          _                = False
matchesFile (target:xs) (_, _, namevals) =
    case lookup "file" namevals of
        Just file  -> file == pack target
        Nothing    -> False
