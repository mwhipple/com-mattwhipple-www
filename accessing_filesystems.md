---
title: Accessing Filesystems
---

- [Software](./software.html)

---

Probably like most people nowadays I have devices containing bits
scattered around that I want to access at some point. While I
personally don't tend to _acquire_ devices frequently due to my
[minimalist tendencies](./consiscious_consumerism.html), I do have
devices that have slowly accumulated over the decades (and which are
largely just awaiting inspection and purging). What seems like a
long-time strength of the GNU/Linux ecosystem is its ability to
readily access a wide range of filesystem.

An implied overdue effort whith I'll be tackling using my new laptop
is that of organizing the data I have floating around (and getting rid
of the physical vessels).

## NTFS

Interacting with NTFS is typical if using a dual-boot system but
since I essentially stopped using Windows around the time Windows 7
came out I haven't interacted much with NTFS since. I'd somewhat
expected NTFS support to be fairly complete within the mainline Linux
kernel by now, but the only option that presented itself in the
configuration interface seemed somewhat limited. After consulting the
relevant Gentoo wiki page[@gentoo-ntfs], the FUSE based `ntfs-3g`
which I believe I used back in the day seemed like it still offered
the most complete solution (and while kernelspace solutions seem to be
maturing, the use of FUSE certainly explains the peristent externality).