---
title: Haskell on Gentoo
---

- [Software](./software.html)

Installing Haskell on Gentoo is fairly easy but not quite
straightforward. Several months ago I installed it on
another Gentoo box and ended up playing whack-a-mask to
enable each package to be installed on the target
amd64 architecture. When looking to install it on
the machine I'm using now I knew there needed to be
an easier way.

The solution was very easy to find once it was looked
for. The Haskell page on the Gentoo wiki[@haskell-gentoo-wiki]
has a strong offering of practical information including a
link to the key instructions I followed from the
Gentoo Haskell Project [@gentoo-haskell-project-github-readme].
Once I managed to successfully type the commands provided
there (using the `eselect-repository` method which
seems like the newer hotness) all the packages
were installed without a problem.
