---
title: The Field Guide to Understanding 'Human Error'
---

- [Books](./books.html)

---

I recently stumbled into reading The Field Guide to Understanding
'Human Error'[@the-field-guide-to-understanding-human-error] which
turned out to be fairly relevant read for my current
circumstances. While much of the book is focused on tragic accidents,
the ideologies and practices described are likely to materialize in
some form or another; particularly in fields where there is regulatory
requirements around safety or quality.

The book was very welcome in that the Old World view of Safety I is
not only likely to be generally deficient but is likely to be fairly
opposed to what seem to be the most proven practices for building out
robust and resilient software systems. I've spent time in the recent
past discussing how approaches such identifying root causes is by
itself a token for whack-a-mole, but not having background knowledge
left me suggesting fairly trivial adjustments whereas this book
exposes a tranche of established thought in that arena that could help
drive more substantive corrections.
