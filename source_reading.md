---
title: Source Reading
---

- [Software](./software.html)

One practice I picked up many years ago is regularly reading source code.
While much code is fairly uninteresting reading a variety can reveal
helpful practices and tricks, can enable deeper understanding of software
that is used, and perhaps most importantly I think it has helped me develop
a very strong sense of coding taste. More generally it seems widely
acknowledged that reading code is a skill most engineers should have: one that
may be more difficult than writing code and based on metrics around
development versus maintenance time one that is likely to be used more often.

Recently I've started to do this more systematically using a small set of
tools which I'll be making heavy use of. Those selected tools will be
catalogued here.

- [Brine](./source-brine.html)
- [Criterion](./source-criterion.html)
