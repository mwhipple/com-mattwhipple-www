---
title: Visualize lsmod dependencies
---

This likely duplicates information already available,
but it seemed simple enough that
(given sufficient familiarity with the tools involved)
reimplementing it would likely take less time that finding
an existing implementation.

``lsmod`` outputs the list of in-use modules including
a listing of which other modules are directly using
(and therefore dependent on) the current module.
As this output doesn't immediately convey transitive
dependencies, a visualization of the complete depenency
graph using ``graphviz`` can help provide insight into
all utilized modules.

This is straightforward using awk to generate the
associated dot output. The fourth field, if
present, contains a comma separated list of the
modules which depend on the first field	.

.. code-block:: awk

   BEGIN { print "digraph G {" }
   {
     # if a leaf just output
     if (NF == 3) print $1
     # otherwise output produce each edge
     else {
       split($4,mods,",")
       for (mod in mods)
         print mods[mod] " -> " $1
     }
   }
   END { print "}" }

Placing that in an executable file
(with a ``#!/usr/bin/awk -f`` shebang line)
and invoking a command such as::

  lsmod | ./lsmod2dot | dot -Tsvg > lsmod.svg

will produce a complete visualizaton of
the dependencies of the lsmod output.
