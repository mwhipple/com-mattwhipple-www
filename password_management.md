---
title: Password Management
---

- [Password Management](./password_management.html)

I've unfortunately largely practiced poor personal password hygiene
(professionally there are typically sanctioned tools to avoid this
issue). While I've used passwords that are unlikely to be guessed,
I've tended to reuse some number of passwords across sites which is
certainly asking from trouble on the modern Internet. I've typically
avoided password management built in to software such as browsers
since that _was_ of little help if using a one-off device. Many of
these concerns could be addressed by using something which is cloud
based (either a dedicated manager or by synching stores from other
software) but I've been reluctant to commit to handing over that data.

Like virtually everyone else on the Internet, at least some of my
credentials have been compromised in some breach or other and are now
floating around on the Dark Web, leading to occasional spam emails and
presumably attempts to apply those credentials to alternative sites,
so _not_ adopting a better practice at this point seems foolhardy.

I started looking around for something open source a while ago; at the
time I was leaning towards a solution that derived passwords based on
some combination of a salt and per-use parameters (and therefore the
passwords would not be saved anywhere) but searching around for
readily usable solutions led me to pass[@pass-home]. The software as a
whole is certainly aligned with my general preferences and so far it
seems to be working very well. Though I need to invest a bit more in
getting it to do everything that can streamline my authentication
needs, it provides a clear path for me to stop my terrible past
habits.

An additional practice I've picked up is to use distinct usernames and
emails for each site, typically by appending some form of suffix to
a base value (for emails appending `+<something>` to the username acts
as an alias for many mail servers). This is likely to be slightly more
secure in that the username _is_ half of the login credentials and so
not reusing usernames will prevent any script kiddie replaying of
credentials and at least present a slightly higher hurdle for anything
more active or sophisticated. Additionally as any credentials _do_ get
compromised I can more easily identify the source of the leak.
