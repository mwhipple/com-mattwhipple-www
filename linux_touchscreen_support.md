---
title: Linux Touchscreen Support
---

- [Computers](./computers.html)

When I purchased my [XPS 13](./xps_13.html) I inadvertently got a
model with touchscreen support. While it is certainly a fairly minor
feature, it is one I've grown to like and use while reading something
on my laptop (primarily a habit picked up from another
device).  While it worked without issue on the now removed Windows
installation it is unfortunately _not_ working in Linux at the moment
and so I'll be digging in a bit here and there to see if I can get
that straightened out.

It is highly plausible that my system is missing some drivers and I'll
need to get up to speed on the hardware and how it is connected. I
made the switch to Wayland[@gentoo-wayland] several years ago and so the first step
will be making sure that is able to handle and provide diagnostic
feedback for the events from the touchscreen.

## Remedy

Apparently the missing element was just something in the kernel which I had
previously configured but had not rebooted into the new kernel, while
rebooting for other purposes the touchscreen inadvertently started working.
