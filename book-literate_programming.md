---
title: Literate Programming
---

- [Books](./books.html)

Donald Knuth's _Literate Programming_[@knuth1992literate] has
a palpable influence on this site in general, and so
re-reading it fairly soon after the creation of this site
seemed in order. In particular as I explore richer options
for my literate programming tooling I'd prefer to stay
close to the source.

I ordered this book a couple years ago when my interest in
the practice of literate programming started, with the
thought that it would be wise to draw from the source.
It reinforced some of my ideas around literate programming
in general and also provided me a firmer framework and
structure with which to approach the practice.
At the time of writing this those practices are compromised
a bit due to tool simplicity, and one of the main
motivations for this reading was to guide adoption and
enhancement in a way that would converge with the canonical
advice.

In terms of the literate programming(LP) sections I think there's
a strong argument for the practice but also many questions
are raised. Likely the most obvious line spawns from the
Programming Pearls submission and criticism by Doug McIlroy.
A potential takeaway from this discourse is that literate
programming is too tied up to an overly monolithic programming
style. My initial draw to LP is from quite the opposite
direction and has been referenced elsewhere in terms of using
LP for things like IaaC; much code makes use of increasing
amounts of abstraction and potentially implicit knowledge
which can leave a chasm between what the code says and its
effects. McIlroy in no way seemed to challenge LP as a
practice in and of itself and his terse alternative to
Knuth's monolith included the disclaimer:

> If you are not a UNIX adept, you may need a little explanation

which feels like it could easily support the notion that
LP is _more_ valuable as reuse provides greater economy
and therefore there exists a larger volume of potentially
useful external information.

I think another challenge with LP is applying it to use in
teams and as projects evolve. This largely is likely to
come down to establishing buy-in, practices, and discipline, and
is presumably already getting attention in environments
such as data science where LP style tools such as
Jupyter notebooks are common.

Much of the rest of the book is also very interesting,
and while the specifics may have been obsoleted through
developments like optimizing compilers and newer programming
languages, the general patterns of problems still exist
in updated guises.

On my initial read I didn't quite make it through
_The Error Log of TeX_ but this time I slogged through it
and it proved somewhat interesting. Similar to above
many of the specifics are now less relevant
(thankfully personal computers spare most us from
night shifts on timesharing systems), but the fact
that a very solid piece of software written by a
highly respected programmer suffers from the same
categories of (often foolish) bugs as the rest of us is
heartening (especially as I fell deeply into one
category recently). The categorization itself also
seemed potentially worth building on (and probably
has been).

This is a book I'll likely steal a fair amount of
information from and will therefore collect a
decent number of citations.



