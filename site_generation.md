---
title: Site Generation - Matt Whipple
---

- [This Site](./this_site.html)

---

This site is generated with pandoc[@pandoc-users-guide].
Each html file can be produced by the corresponding
source markdown[@markdown-daring-fireball]. The source files
can be collected using a wildcard/glob which can then be used
to derive the output files. The pattern
itself can be used to call pandoc with the desired invocation.

## Run Within Container if Specified

I typically use one of two patterns to run things in containers,
either a prefixed target pattern (i.e. `docker-<target>`) or
accepting a flag which swaps out how things are done. Here I'll
use the latter where the passing of a defined `IN_DOCKER` flag
(often provided as an enviornment variable)
modifies targets to run within defined containers rather than
directly on the host.

If that `IN_DOCKER` flag is defined then pandoc should be run
in a container which works within a bind mount of the local directory.

TODO: Extract and use current user

~~~{.Makefile file=Makefile}
ifdef IN_DOCKER
  PANDOC = ${DOCKER} run --rm --volume $(abspath .):/host -w /host ${PANDOC_IMAGE}
else
  PANDOC = $(call required-command,pandoc)
endif
~~~

## Store Commands in File

While it seems somewhat heavy, this project will also support
a third means of execution. The pandoc image used above does
not include `make` and therefore cannot readily use the invocations
derived by the rules within this file. Since these steps can be
done independently I'd rather go that route than introduce a
customized image when wanting to use a pipeline of containers
(for CI). It seems likely I'll find a one-stop
image at some point which will render this unnecessary, but
for now this file will also support
generation of a shell script containing what it would otherwise
do.

Generation of such a file is fairly straightforward as it can
amount to overriding the already defined `PANDOC` variable to
echo to the specified file rather than call the produced command.
The final functionality is very simple but is built on top of a
fairly high level of comfort with the underlying tools.

~~~{.Makefile file=Makefile}
GEN_SCRIPT:= gen_site

clean-script: ; rm -f ${GEN_SCRIPT}
.PHONY: clean_script

${GEN_SCRIPT}: PANDOC = >> ${GEN_SCRIPT} echo pandoc
${GEN_SCRIPT}: clean-script site
	chmod +x ${@}
~~~

## Define Conversion Rule

With all of the building blocks defined, the rule to pass the sourcefiles
through pandoc to produce the HTML output can be defined.

~~~{.Makefile file=Makefile}
${OUT_DIR}%.html: %.md
	${PANDOC} --defaults pandoc.defaults -s -o ${@} ${<}
~~~

# Pandoc defaults file

Pandoc will be configured through the use of the defaults file
[@pandoc-users-guide sec. #default-files].
supports which is pointed to above.

## Define input and output formats

I'll be using Pandoc enhanced Markdown as input and generating modern
HTML. At some point in the future I may want to swap the output over to
something more component/React based but that's not particularly likely
to actually happen and if so not for a bit yet.

~~~{file=pandoc.defaults}
from: markdown
to: html5
~~~

## Configure Citations

I want to use citations with a global bibliography. The `link-citations` metadata
field [@pandoc-users-guide sec. #other-relevant-metadata-fields] is helpful
to provide appropriate hyperlinking. With the linking I also prefer more of a
footnote citation style so this uses the numeric ISO 690 CSL style retrieved
from the Zotero Style Repository[@zotero-style-repository].
Without an appropriate background, sticking to
an ISO standard seemed a safe choice.

~~~{file=pandoc.defaults}
citeproc: true
bibliography:
- sources.bib
metadata:
  citation-style: iso690-numeric-en.csl
  link-citations: true
~~~

## Configure Math Rendering

At some point I'll likely be incorporating some interesting path but in
the short term I've already run up against the limits of the raw rendering
so I'll start with what seems like the default better option of mathjax.

~~~{file=pandoc.default}
html-math-method:
  method: mathjax
~~~

