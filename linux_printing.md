---
title: Linux Printing
---

- [Linux Configuration](./linux_configuration.html)

---

## Printing

Printing is something that certainly tends to fall _very_ low on my
list of things to do. I struggle to think of the last time I needed to
print something for myself so typically getting a printer configured
is something I end up doing for some shared household purpose.

I pay a correspondingly small amount of attention to printers
themselves. This is aggravated by the notion that there's no
particular suitable place for a printer to live in my house. While I
currently have a network-ready Canon multi-function device gathering
dust I gravitate towards using a Brother HL-L2320D[@brother-hl-l2320d]
that I picked up several years ago to curb the likelihood that the ink
will dry up between usages, but that has the disadvantage that it
needs to be connected to a computer so I have plans to connect it to a
shared server or small device such as a router but haven't gotten
around to that yet (see low prioritization above). So at the moment I
plug set it up and plug it in as needed. Unfortunately the printer is
also not supported by ChromeOS which is what is installed on other
laptops in my house so without taking the time to expose it over the
network there's effectively a golden printing machine to which
documents are first relayed through email or similar.

I've probably spent the last several years just using my work Macs to
do printing but in the effort to preserve that boundary more soundly
and to inch towards configuring the referenced server I opted to
configure my laptop to be able to make use of that printer.

## Configuration

Thankfully CUPS now provides a very nicely polished interface to
printing configuration in Linux. I have vague memories of it being far
more painful when I configured my first printer in Linux...which was
back before OS X came out which likely gave CUPS a nice shot in the
arm (along with the large amount of time since then).

To start I quickly went through the documentation provided by
Gentoo[@gentoo-printing]. I was very intridued by the newer trend
toward driverless printing but unfortunately it's not quite magical
enough to work with my printer that wasn't made for it so I ended up
having to chase down the driver. I spent a little bit of time trying
to manually unpack the driver provided by Brother and adapt around the
log messages produced by CUPS and got to the point where the printer
_seemed_ to be close to working.

I'd ventured down the path of trying to place all of the files into
expected locations but got to the point of not wanting to reinvent the
wheel in case an ebuild or similar was already floating
around. Another search led me to an Arch Linux
page[@arch-cups-printer-problems] which indicated that the `brlaser`
package may work for my printer. Similar information is also buried in
the Gentoo wiki but at best isn't readily discoverable due to there
not being any apparent guidance for non-networked Brother printers. If
I were feeling particularly responsible I'd submit an update to the
wiki but it doesn't seem like the intersection of people using Gentoo
and looking to freshly configure that specific older printer is likely
to extend beyond myself. After installing brlaser the printer works
without issue.