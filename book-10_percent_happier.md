---
title: 10% Happier
---

- [Books](./books.html)

I've read 10% Happier[@10PercentHappier] by Dan Harris a couple
times (having primarily re-read it as part of cataloguing materials
for this site). I originally picked up a copy for my wife since she
was raised with some of the anxiety being the "price of security"
concepts which are an undercurrent in the book and which I heard
Dan Harris speak briefly about in an interview several years ago.
Much of the material also resonated with me, particularly in light
of some of the challenges of parenthood and a pandemic and the
interactions between those.

Based on earlier notes my first reading left me reflective about
the relationship between youthful detachment and mindfulness as
compared to more recent relatively impulsive reactionality. The
pactices. The promise of exercising the skills such that I may
be able to more tangibly realize progress in areas of
self-improvement that seem largely prone to staganation seemed
very enticing.

My reaction to notions of control seem worthwhile to initially
re-include verbatim.
One of the themes of the book that I'm particularly interested in
reconciling in my own life is around control. I've spent most of my
life feeling as though my choices were very limited and that I was
somewhat a passenger of circumstance (which may have been a
contributing factor to my previous inclination towards detachment).  I
now feel as though I have seized a fair amount of agency but am likely
ill-equipped to reconcile when seemingly tractable concerns resist or
evade control. I rationally embrace concept such as wabi-sabi and
acknowledge that "life is what happens to you while you're busing
making other plans" (-- John Lennon) and can aphoristically reduce
this particular problem to the notion that you can choose your path
but you can't choose where it leads, but my /reactions/ when faced with
unexpected and perceivedly unnecessary slippage are generally less than
accommodating. Practicing mindfulful nonattachment feels as though it
should allow me to keep focus where it may be valuable.

While the first reading understably seemed to evoke a fair amount
of relating my past with the material the subsequent reading remained
predictably more oriented toward the material itself. While I
started insight meditation after the first reading my practice
stalled and ultimately got derailed due to <insert excuse here>.
The second reading provided new receptors for relatability and
reinforced the notion that I should actively pursue some of the
suggestions, and further that I am currently very well positioned
to do so.