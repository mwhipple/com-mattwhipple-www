---
title: XPS 13
---

- [Computers](./computers.html)

---

Last month I went on a ski trip during which my backpack with laptops
(personal and work) got accidentally taken by housekeeping. The bag
was found about a week later, but it prompted me to acquire a decent
new personal laptop. I've tended to get fairly low end ones for quite
some time and while they could handle the majority of what I regularly
needed, I would also need to lean on work computers to occasionally
fill in the gaps.

The notion of getting a _nicer_ laptop after having presumably lost a
previous one does seem to be based on questionable wisdom. The age and
costs of the laptops that I use largely mitigate that concern, but
this also suggests that I should prefer more disposable devices in
less controlled situations. This is particularly tenable now that I
have retrieved that original laptop which can now fill that role.

## Hardware Selection

After some quick discovery I ended up going with a Dell XPS 13
(9310 P117G002 FX02). I was
originally drawn to a Purism Librem but the cost:risk ratio didn't
seem overly appealing even if I want to suppor their mission (though
they'd still be a strong contender for a phone). I was then also
looking at a Lenovo, but I wanted to beef up some of the specs which
seemed to amplify the price difference past the point where it seemed
worthwhile. As the XPS seems to be consistently listed as one of the
best laptops for Linux and seemed to offer the best bang for the buck,
I ordered one on the evening of the discovery of my missing laptop.

For some reason or other (the current chip shortage? standard
padding?) the estimate for delivery was about a month from the order
date, but that was thankfully abbreviated and I actually received the
new laptop in about a week.

## Operating System

As aforementioned, I ordered the XPS due to it being a consistently
recommended Linux laptop, but I was surprised to discover when
ordering that an installation of Ubuntu cost notably _more_ than an
installation of Windows. Naively it would seem that a free OS should
cost less than one that requires a license. Either Ubuntu has a
premium attached for reasons such as the consumer demographic or
supply and demand motivations, or Windows leads to some discount
spurred by some combination of incentives and subsidies. The latter
seems fairly likely given the included suite of trial and freemium
software. Given that Ubuntu is not my preferred Linux distribution and
I thought it might be nice to take a newer Windows out for a spin, I
opted for a Windows installation.

While I had no particular aversion to Windows 11 when first starting
to use it, I quickly shifted into old-man-yelling-at-cloud mood when
trying to use it to for anything remotely technical. While the Linux
subsystem (wsl) seemed promising I found myself needing to chase down
third party software for functionality which seemed not only
rudimentary, but which older versions of Windows seemed to support
more readily. It could certainly be that such functionality was never
available in the personal versions of Windows and I'd previously been
using the more advanced versions, but at this point in time they seem
commoditized enough to pass to the lowest tier. It could certainly
also just be the nature of the closed source ecosystem where Microsoft
is (understandably) focused on the OS itself and there is no provided
complementary system of core utilities. While I'd imagine that such
solutions do exist and are simply not readily provided, I don't have
enough of an interest to dig in to such options. So went the chance
for Windows to survive on the system (likely as a dual boot option).

## Booting into Linux

The laptop has a couple USB-C ports and a Micro-SD Card reader. Based
on the apparent options in the BIOS the SD card provided the means to
boot into and install Linux, so I proceeded to load a boot image onto
such a card. The card itself would boot, but unfortunately the
installed versions of Linux never would, failing on assorted
combinations of IO errors and inability to load required files. I
tried several different images but they all failed similarly. As this
was a transitive issue I was far more interesting in just getting past
it rather than understanding the details behind it, and so I landed on
a workaround of having the image on both the SD Card (for booting) and
a hard connected to one of the USB ports. The Gentoo LiveCD provided a
nice failure mode during which an alternative location to the system
root could be specified, and so at that time I could redirect the boot
to the USB hard drive and..._it worked_.

_But_...now that the USB connected hard drive was in the mix I also
discovered that it also appeared in the boot menu and so therefore the
SD card approach was just a wild goose chase and the USB drive could
simply be booted rather than the more convoluted workaround
above. This information is presumably in some manual that I could have
taken the time to read (and likely will do so in the future) but since
the information wasn't provided with the laptop and I was hoping to
get the laptop configured so it could act as my preferred device for
tracking and consuming such information I learned this bit the hard
way.

## Installing and Configuring Gentoo

[Gentoo Linux](./gentoo_linux.html) is my preferred Linux distribution
and so it is what I installed on the laptop. After the detour around
booting into Linux the installation was _mostly_ a straightforward
progression through the Gentoo Handbook[@gentoo-handbook]. There were
some configuration wrinkles that needed to be ironed out to provide a
bootable kernel (and as usual I have some pending hardware
configuration), but that was resolved in a fairly typical amount of
time and tweaks.

At the end I'm fairly excited as this is the first time I've installed
Gentoo on one of my primary computers in several years and am looking
forward to approaching configuration more systematically. In this
installation I've also opted for the default OpenRC over
systemd. While I adopted systemd several years ago for the sake of
benefits it offered for hosts intended to run containers, that is not
an immediate concern for me and the simpler parts of OpenRC are more
conducive to some of my current knowledge management approaches.