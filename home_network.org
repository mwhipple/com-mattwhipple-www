#+TITLE: Home Network - Matt Whipple

#+INCLUDE: "./nav_main.org"

My home network is primarily driven by Linksys routers in the WRT
family. At one point when I was doing assorted consulting I configured
a wireless mesh network with
OpenWrt[cite:openwrt-wikipedia][fn:OpenWrtHome] and since then I've
primarily purchased WRT routers so that I had a simple route to
installing OpenWrt if desired or needed. When compared to stock
firmware OpenWrt provides the potential benefits that it is
open-source, more flexible, and receives more ongoing attention.

With OpenWrt a router basically becomes a minimal Linux box, so I'd
optimistically hoped that newish Wi-Fi router firmware would be robust
enough that I'd ultimately only be motivated to swap out the stock
firmware when I wanted to use my router to provide services it
normally wouldn't support. Unfortunately my optimism didn't pan out
and the firmware continued to suffer from issues which end up being
resolved by the occasional reboot combined with a factory reset every
so often to flush out whatever state is apparently blocking things
up. There's the possibility that these issues could be resolved
through configuration tweaks, but once investing any notable amount of
time is a factor it seems prudent to invest that time in an open
platform that is is less likely to impose barriers and is portable
across devices. The OEM firmware has also not seen an update in a long
time and is therefore almost certainly plagued with neglected
vulnerabilities[fn:HomeRouterSecurityReport2020]. I've therefore been
impelled to replace the firmware (which is a trivial process on the
involved routers that uses the standard firmware update interface).

* TODO Tweak Config

bibliography:refs.bib

[fn:HomeRouterSecurityReport2020] https://www.fkie.fraunhofer.de/content/dam/fkie/de/documents/HomeRouter/HomeRouterSecurity_2020_Bericht.pdf
[fn:OpenWrtHome] http://www.openwrt.org
