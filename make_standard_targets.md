---
title: Make Standard Targets
---

- [Make](./make.html)

---

This page should be updated to reference GNU standard targets.

# Introduction

Make promises a universal entrpoint to the build concerns for
a project, and adoption of some conventional targets furthers
the benefits of such universality by readily delivering common
forms of behavior and information. Many of these are likely
to be existing standards (potentially transplanted).

# `help`

You have a project checked out...now what? `make help` should tell you.

Well documented projects are likely to contain a range of information
of which guidance around build concerns is only a portion; when
such information is desired it can be very nice to have it delivered
through the same channel as other build information. The help target
provides such information including a list of targets/commands that
are likely to be useful along with more interesting settings or
instructions to aid in working with the code.

While much of this information could likely be introspected
this is typically done through having the text written somewhere
convenient and then dumping it out on the screen when help is
requested.

# todo

A common meta-task when working on a project is finding out what
work should actually be done. This may include some combination of
a link to some to some issue tracker somewhere or information
extracted from sources files. In either case it can be nice
to be able to run `make todo` to find a task to grab.

# check

A widely used standard, `check` performs code verification.
If everything is configured properly a successful execution
of `check` should mean that everything is ready for use.
