---
title: SysML Distilled
---

- [Books](./books.html)

---

Like probably most people I tend to produce diagrams fairly informally
using whatever online tool is readily available and I'm least terrible
at, but largely as part of focusing on aspects of communication over
contribution I've started to revisit those practices and brush up on
some of the more formal approaches. While producing some such diagrams
I started to flip through SysML Distilled[@sysml-distilled].

The ideas behind model based system engineering held enormous appeal
for me many years ago but didn't seem to have practical application at
the time likely due to my less weathered self not being able to pierce
through the veil of hype to see the wheat from the chaff.

The book itself will act as a highly valuable reference for SysML and
I'll need to brush up on UML. The use of well established constructs
should significantly aid in communication but what I'm particularly
interested in at present is the relationship between the diagrams and
a corresponding pursuit to represent a potentially complex system
through a linked web of such diagrams. One of the common traps with
diagrams and modeling in general is presenting too much information
and therefore the expression of key concepts is buried in noise (and
any associated attempt at being comprehensive is accompanied by an
increased risk of being stale and/or misleading). While larger
diagrams may be more feasible during live presentations a particular
tactic that I'd like to explore is to have each such diagram
restricted to convey only the handful or so of concepts that can fit
in someone's head at a time and then use linking to enable exploration
across the associated chunks of information.
