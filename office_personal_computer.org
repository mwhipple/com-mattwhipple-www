#+TITLE: Office Personal Computer - Matt Whipple

#+INCLUDE: "./nav_main.org"

Here I'm using "personal" to contrast with "work", not in regards to
"personal computer" as a device category.

While in my home office I spend most of my time on a work computer.
However to keep the contents of my work computer solely work related
and not add personal information to a system owned by my employer I
have a second computer on the desk which I use for my non-work related
activities. 

This is currently a ~10 year old Acer laptop which is certainly showing
its age a bit, but is sufficient for the supporting role which is required.
It is running Gentoo Linux with an installation that goes back to when the
system was first acquired and therefore has accumulated a fair amount of
cruft over the years.

I'm currently in the process of updating and cleaning the system up a
bit having just resurrected it.  While I'm straightening the system
out and spending a lot of time watching things compile I'll follow a
cycle of tuning things which are working even though some of them I'll
be planning on replacing once some dust settles.

The system currently uses Wayland for its GUI. Weston is currently
installed and configured, though I'll be looking to replace it with
Sway. I think when I last configured this system Wayland support was
far sparser and more involved and at that time Sway didn't work
smoothly and after a quick attempt it looks as though I still need to
adjust something which will likely be done after a kernel upgrade.
I will therefore spend a small amount of time tweaking my
[[weston_config.org][Weston configuration]].
