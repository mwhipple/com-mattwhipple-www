---
title: Camping
---

- [Home](./index.html)

Recently, with unknown provenance, my young children latched on to a
strong desire to go camping. This is an activity I've not done since
I was fairly young and one which is not comfortably within my wife's
wheelhouse, but not only do we typically want to support the interests
of our children this is also an appealing activity for us due to
its simplicity and the additional opportunities that it may present.

Our first camping experiences will remain simple since the children
are still young enough that they consume significant constant
attention and periodic intervention of some form or other. Associated
equipment and activities will therefore slowly be expanded as time
passes and viabilities are proven out.

## Trial Run

Prior to actually traveling somewhere and be potentially confronted
with a sleepless night or a post bedtime drive home I got a basic
four person tent and spent the night with the kids in our yard.
This overall went well (aside from attempting to early of a bedtime
which indirectly led to me getting hit in the face with a book.
Time to get a bit more gear and book a trip...

## First Trip

The first trip is booked in Myles Standish State Forest[@camping-myles-standish]
which is a fairly short drive from our home and is in my hometown.
I've spent a fair amount of time there while younger and we grabbed
a site at a pond where my wife spent time with her family growing up.
It promises to be a memorable trip that opens up the door to a new
family activity.

## First Debrief

The first trip overall went very well; everyone had a good time and
I personally found myself very restored in the aftermath. Thankfully
the site was close to home because although we made a list of things
we needed we also distributed responsibility without having a
consolidated verification...and so some things were forgotten but
quickly retrieved. I believe this was my first experience camping at
a campground so my expectations are now a bit more calibrated.

In terms of improving as a camper
my biggest takeaway was that I'm not accustomed to having to put
out a campfire quickly as is likely to be required on a family
camping trip. For next time I'll need to make sure to have an
appropriate shovel and bucket full of water on hand
[@usda-how-to-put-out-a-campfire]. In terms of
general gear: bringing something to act as a clothesline if
camping at a pond would be helpful, and my wife turned out to be
_very_ uncomfortable through the night so something along the lines
of an air mattress is likely to be in order for the next trip.
