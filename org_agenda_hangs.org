** TODO Clean-up

* Initial Discovery

Some good first steps in any software project are to see if the issue
is already filed somewhere and potentially resolved in a newer or
alternative version. Org-mode uses a mailing list and IRC channel for
help[cite:emacs-org-mode-community] and the former didn't provide any
promising results from a cursory search. My usual MO in these
scenarios is to start digging a bit so that I can do some combination
of performing a better search, asking a more informed question, and
working towards a resolution. The next step is therefore to upgrade my
org-mode and specifically switch to a fully updated source checkout
rather than a released package. This also frees up to use any fun
new org features which aren't available in the version that is
packaged with emacs (since I've already incurred the cost of relying
on non-builtin packages). At the moment this bumps me all the way
up from =9.3= to =9.4=!.

* Checkout

org-mode nicely includes the path and command to clone its source
repository right on its home page[cite:emacs-org-mode-home] so
getting the source is simple (some other projects often require
one or two clicks to track down the url which can obviously be
exhausting). Even more helpful are the instructions in the
=Installation= node of the Org's info manual [cite:emacs-org-mode-info]
which also provides the right installation and activation invocation.
The checked out code is wired up in my [[./emacs_init.org::*org-mode source][emacs init file]].

* Resolution

Loading the functions from the latest source code appears to have
resolved the issue, so there's nothing particularly interesting to
cover. I ended up evaluating the buffers within the source checkout to
make sure to use those definitions (and chasing down and evaluating
buffers as needed to quiet errors).  The init file updates /should/
reload the latest source on the next startup.

The specific issue could be tracked down through some combination
of examining changelogs and diffing or bisecting the code, but
there's no motivation to dig any further into this issue now
that it's resolved.

* TODO New Case

After resolving the initial issue I once again suffered from the
agenda hanging after adding a TODO item to a page on this site.
Brief experimentation showed that the agenda hangs when the TODO
is attached to a headline at level four but not one at three.
The short term remediation is to superficially promote that headline
but the underlying issue and specific variations of when it manifests
should be given more attention.

Another TODO caused the same issue at level 3 but not at level 2: I'd
speculate that the issue is related to surrounding context such that
a TODO that has a sibling or parent block may suffer from issues which
is avoided if the TODO is elevated from that context.

* Sources

bibliography:refs.bib
