---
title: Software Reboot
---

- [Software](./software.html)

# Background

Periodically I've gone through periods of revaluating the
software and technologies that I use and tend to adopt
simpler alternatives, and I've now settled on a fairly
minimialistic perspective. This drives my
personal use of computer software and informs but does
not drive my professional use.

One of the ongoing motivations for these descents is
having a tinker's curiosity or potentially a desire
for too much control. The first such reassessments
were largely driven by perceived obstacles: many tools
are built upon many layers of other tools and each
such layer introduces an additional way for things to
go wrong. When such things _do_ go wrong those additional
layers may obfuscate the issue; in any case if
familiarity with the upper layers comes at the cost
of neglecting those that are lower then there ability
to work through such issues becomes severly hampered.
In cases where higher level tools provide
non-essential benefits and conveniences then the value
that they deliver may be uncertain and therefore
the introduced obfuscation may merge into counter-productivity.

As pertinent sensibilities matured and the world moved
forward I began to perceive
wider possible issue around the concept of diminishing
returns in the modern use of technology. Beyond the
relatively harmless signal-to-noise concerns this can
present more serious risks in terms of suboptimal and
occasionally pathological patterns of interaction.
Use of any software is the acceptance of choices that
others have made and not assessing those choices
can lead to not effectively reaping benefits, or worse
in the cases where such choices are in pursuit of
goals which conflict wth your own up to manipulation
of your behavior through dark patterns[@10.1145/3397884].
Many of these are
very complex topics which I'm likely to periodically
reference.

In my personal case many of these
issues are far simpler due to the fact that I'm very
comfortable working more directly with more primitive
interfaces that are closer to the metal. Another
consideration is the attitude that building out such
comfort is likely to be far more empowering in the
long run. Having an increased awareness of the
underlying functionalities rather than passing
through the many additional filters that are involved
in providing glossier packages of anticipated use
inherently enable wider possibilities and build
a toolbox that not only avoids imposed limitations
but readily enables discovery and construction of
novel solutions. A microcosm for this notion is that
of graphical user interfaces (GUIs) vs. command line
interfaces (CLIs). While GUIs may seem more intuitive
to a neophyte, a CLI is likely to be faster all around
for a compotent typist while also enabling vastly
more power that would be dramatically less convenient
or unfeasible through a GUI. Expecting to invest
in technology rather than simply having things work
can be hugely empowering and can carry ongoing growth
after initial overhead which seems only marginally
higher (but less marketed and less pretty) than
packaged solutions.

Most recently my transition to this path has been
somewhat complete. I have a fairly minimal set of
technology that satisfies my needs and while I
continue to have more advanced areas of interest
that I'll explore I do not feel compelled to
disrupt this journey. Perhaps most signficntly I've
also somewhat relaxed my perspective around software
reuse (which will be reflected in some of the code
that ends up here). I think the notion of not
_reinventing_ the wheel is very important but I think
it too often gets taken to mean that wheels should also
not be _created_. This is another topic I'll likely
dive into separately as I think some engineering
approaches may end up too sharply dividing solutions
which are perceived solved from those that are not
such that it creates a chasm across which proven practices
and wisdom are not applied to work perceived as novel.
In this context that mentality will be taken to a cost
ineffective extreme so that I may learn how to make better
wheels through creating them while studying
master wheel builders.
