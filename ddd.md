---
title: Domain Driven Design
---

- [Home](./index.html)

One of my favorite books about software engineering is
Domain Driven Design by Eric Evans. While I've typically
used and promoted many of the concepts and practices to
bring clarity to systems that I work on I've also recently
started to evangelize the ideas more widely throughout
teams, working towards a more strategic adoption of many
of the underlying principles.

The two major values that DDD seems to offer to many organizations
are around fostering containment and conveyance of knowledge and
facilitating decomposition of larger systems (monoliths).

A core thread (which seems to be often neglected in newer
buzzword-y DDD) is the recognition that any model is incomplete
and is likely to be biased towards particular uses, so an
attitude of perceiving any model or implementation
as being _correct_ is flawed, and the pursuit should therefore
be that of an ongoing assessment of those models in use
and the utility they provide for the specific needs.

Also worth identifying (which seems contrary to many of the
DDD-as-a-product style materials but in line with the
big blue book) is that my focus is on
carving out the model as a first class concept used to
facilitate communication. While the model and code should
co-evolve and remain closely coordinated the notion of having
that connection be _direct_ seems to invite some combination
of churn, overengineering, and effort which may not yield
value to the business.

## Taming Knowledge

A typical trap for a technology company is to get lost in
the way things work. _What_ a solution does and _how_ it
does it can become increasingly inextricable: buried
underneath layers of tribal knowledge to the point that
any intended proposed value of system is lost amidst how
it happens to behave.

Such systems become unsafe to evolve as an implicit definition
of desired state in terms of that which currently exists leads to any
modification of such state a violation of that specification.
A system that is defined in terms of  itself can never satisfy both
current and future specifications.

More directly the ability to express the behavior in such systems
is likely to become increasingly difficult. Key concepts
are likely to be implict within the model and rely on convoluted
attribute based descriptions and assumed shared knowledge which
is likely to not have equivalent understanding across stakeholders
and to drift over time. All of these issues are likely to lead
to significant confusion and information loss over time, and likely
manfiest very directly in a prolonged cognitive overhead of the onboarding
of new team members.
