---
title: Gentoo Linux
---

- [Software](./software.html)

Gentoo Linux[@gentoo-wikipedia] has been my OS/distribution of choice
for quite a while now.  Based on the history of the project I've
been using it roughly since its inception. Around that time I had
different installations of Linux distributions and BSDs installed on
various machines so I could get a feel for the alternatives.
Shortly thereafter I moved and only brought my main desktop system
with me which at the time was running RedHat. I think at the time I
was trying to embrace RedHat more as it seemed the most commercially
dominant but I could not bring myself to embrace `rpm` at the time without
yearning for some of the other package managers (most notably `apt`).
The relative dislike of rpm combined with a desire to have a deeper
understanding of internals (which periodically bubbled to the surface)
led me to rip out rpm and instead install everything from source.
Having a from scratch system was an enlightening but very time consuming
project that I kept with for quite a while (long enough to work through
enough updates of `gcc` and `glibc` to get the hang of it and not
want to continue doing it).

Some of my main motivations for going the from scratch route (I think)
were that there was always a subset of software I'd typically install
from source. Packages may not have existed, been stale, or I may have
wanted to specify the build configuration options. The Apache web
server (`httpd`) comes to mind as a prominent example but there were
several other services and applications which fell into that category.
Additionally in the case of an issue with a package the solution often
involved jumping over to installing from source, and so it seemed
appealing to pursue and grow very comfortable with a unified approach
to installation.

I think at that time I'd installed Gentoo on a couple boxes
but didn't necessarily have a full understanding of it.
My vague memory was that I held the perception
that it was that it was a more BSD-like distribution with optimization
benefits. When the time came for me to move away from the source
installs I went with Gentoo and at some point came to the realization
that it pretty directly addresses the benefits I saw in the source
installs: ebuilds effectively providing a fairly thin wrapper to make
source installs less cumbersome.

At this point I primarily use Gentoo out of familiarity. I certainly
work with other distributions regularly and would be tempted to try
an alternative like Arch if I had time to burn and a system to wipe,
but at this point it seems unlikely that I'd switch Linux distributions
without a compelling reason to do so (checking out something like
Harvey would be more likely). Gentoo is also a (meta-)distribution
for which there may be little reason to switch due to its inherent
flexibility. Several years ago one of my coworkers was discussing
changing distibutions which piqued my interest, but when he told me
the reason he was doing so it was just due to wanting to make use
of a different set of opinions/choices which are a core element in so
many flavors of distributions. That was a reminder that even my
perception of what a distribution is has changed over the years and
that I'd likely feel needlessly restrained without the flexibility
afforded by Gentoo.

# systemd

While OpenRC is the default init system for Gentoo I'm using systemd
on all of my current Gentoo installations. My specific reasons for
using systemd are that several years ago (at work) I was configuring
a containerized deployment environment and was exploring options for
Docker log drivers. At that time (I'm not sure what has has
changed) the default json-file and journald were the two alternatives
which provided the most durable logging for containers while preserving
the behavior of logging to stdout/sterr (specifically
in terms of being able to view logs for containers which end up
terminated nearly instantly after launch). I stumbled upon a reputable suggestion
that json-file had less than stellar performance (and the environment
was expected to inherit enough traffic that that would be cause for concern),
so I adopted journald/systemd (and CoreOS which I quickly became
a fan of). To foster familiarity with systemd I also decided to adopt
it in other sensible places such as my personal systems.

In light of reassessing some technology choices I was tempted to switch
to OpenRC since it is likely far more inline with *nix design principles
and more easily introspectible but the Gentoo Handbook seems to be
fairly ambivalent about this choice with the identified deciding factor
being simply that of intertia and presence of
documentation[@gentoo-handbook-stage] which doesn't seem substantial enough
to warrant a shift.


