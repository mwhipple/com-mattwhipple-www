---
title: C Naming Conventions
---

- [C](./c.html)

My default style for naming objects in C draws on conventions
picked up from elsewhere. From what I remember the majority of
it was borrowed from the GObject project from when I
was exploring adopting that library several years ago.

# Verbosity

A general common practice I adhere to is keeping the verbosity
of identifiers proportional to their scope and role. Functions
or identifiers which are part of an interface should strive to
clearly communicate their purpose to the calling code.
Additionally any internal identifiers which may be relatively
long-lived should have expressive names so that their meaning
doesn't need to squat in brain cells or require repeated
decoding as they are referenced over time. In cases where
the use is simple or contextualized enough that the meanings
should remain evident, short identifiers will likely be used. 

# snake_case Identifiers

Functions and variable names will typically use snake_case.
This draws on common styles across multiple languages, most
unambiguously Python. I've encountered assorted claims suggesting
that snake_case is more comprehensible than alternatives such
as mixedCase[@5521745] and not any empircal evidence to the
contrary.

# CamelCase Types

In a style similar to many OOP languages types
will be CamelCase with an initial capital. In some cases I may
use mixedCase without the capital for an instance of the type,
but typically I'll try to either use snake_case or leverage the
defined type itself to provide additional information and keep
the identifier very terse.

# Identifier Prefixing

C has no built in support for namespacing and so identifiers
which are intended for sharing will be given a prefix
to curb the risk of any naming collisions and also set a
clear indication of the source of the identifier.

# spinal-case Prefixed Files

Filenames will remain all lower cased to avoid any issues with
filesystem case sensitivity. Files that are intended to be
used elsewhere will include a prefix analogous to those used
for identifiers, which will be prepended to the file name in
the pattern `<prefix>-...`.
