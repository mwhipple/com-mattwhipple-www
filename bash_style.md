---
title: Bash Style
---

- [Bash](./bash.html)

---

My bash style draws on some of my standard styles, and borrows
conventions from the Google style guide for bash scripts.

## Strict Flags

I make use of the fairly typical combination of flags to make sure
that possibly unexpected things are explicitly dealt with.

```bash
set -euo pipefail
```
